//#include <stdio.h>
//#define MAX_N 100000
//
//int arr[MAX_N];
//int tmp[MAX_N];
//int N;
//
//void swap(int i, int j) {
//	int tmp = arr[i];
//	arr[i] = arr[j];
//	arr[j] = tmp;
//}
//
//void quickSort(int leftEnd, int rightEnd) {
//	int l = leftEnd;
//	int r = rightEnd;
//	int pivot = arr[(l + r) / 2];
//	while(l <= r) {
//		while (arr[l] < pivot) l++;
//		while (pivot < arr[r]) r--;
//		if (l <= r) {
//			swap(l, r);
//			l++;
//			r--;
//		}
//		if (leftEnd < r) quickSort(leftEnd, r);
//		if (l < rightEnd) quickSort(l, rightEnd);
//	}
//}
//
//void mergeSort(int start, int end) {
//	if (start < end) {
//		int mid = (start + end) / 2;
//		mergeSort(start, mid);
//		mergeSort(mid + 1, end);
//
//		int i = start;
//		int j = mid + 1;
//		int k = start;
//		while (i <= mid && j <= end) {
//			if (arr[i] <= arr[j])
//				tmp[k++] = arr[i++];
//			else
//				tmp[k++] = arr[j++];
//		}
//		while (i <= mid) tmp[k++] = arr[i++];
//		while (j <= end) tmp[k++] = arr[j++];
//
//		for (int l = start; l <= end; ++l)
//			arr[l] = tmp[l];
//	}
//}
//
//bool binarySearch(int val) {
//	int start = 0;
//	int end = N - 1;
//	int mid = (start + end) / 2;
//	while (start <= end) {
//		mid = (start + end) / 2;
//		if (val > arr[mid])
//			start = mid + 1;
//		else if (val < arr[mid])
//			end = mid - 1;
//		else // if (arr[mid] == val)
//			return true;
//	}
//	return false;
//}
//
//int main() {
//	scanf("%d", &N);
//	for (int i = 0; i < N; i++) {
//		scanf("%d", &arr[i]);
//	}
//
//	//quickSort(0, N - 1);
//	mergeSort(0, N - 1);
//
//	int M;
//	scanf("%d", &M);
//	int find;
//	for (int i = 0; i < M; i++) {
//		scanf("%d", &find);
//		printf("%d\n", binarySearch(find));
//	}
//}