//#include <stdio.h>
//#define MAX_TABLE 1001
//#define MAX_N 100001
//#define MAX_LEN 6
//
//typedef struct _node {
//	char name[MAX_LEN];
//	bool isEnter;
//	_node* next;
//} NODE;
//
//NODE hashTable[MAX_TABLE];
//
//NODE nodePool[MAX_N];
//int nodePoolIdx = 0;
//
//NODE* ret[MAX_N];
//int retIdx = 0;
//
//NODE* bufferNode[MAX_N];
//
//unsigned long getHkey(const char* str) {
//	unsigned long hash = 5381;
//	int c;
//	while (c = *str++) {
//		hash = ((hash << 5) + hash) + c;
//	}
//	return hash % MAX_TABLE;
//}
//
//void init() {
//	for (int i = 0; i < MAX_TABLE; i++) {
//		hashTable[i].next = 0;
//	}
//	nodePoolIdx = 0;
//	retIdx = 0;
//}
//
//void mstrcpy(char* dst, char* src) {
//	int idx = 0;
//	while (src[idx]) {
//		dst[idx] = src[idx];
//		idx++;
//	}
//	dst[idx] = 0;
//}
//
//int mstrcmp(char* a, char* b) {
//	int idx = 0;
//	while (a[idx]) {
//		if (a[idx] != b[idx]) {
//			return a[idx] - b[idx];
//		}
//		idx++;
//	}
//	return a[idx] - b[idx];
//}
//
//void insertIntoHashTable(char name[MAX_LEN]) {
//	NODE* newNode = &nodePool[nodePoolIdx++];
//	newNode->isEnter = true;
//	mstrcpy(newNode->name, name);
//	int hkey = getHkey(name);
//	newNode->next = hashTable[hkey].next;
//	hashTable[hkey].next = newNode;
//}
//
//void removeFromHashTable(char name[MAX_LEN]) {
//	int hkey = getHkey(name);
//	NODE* cur = hashTable[hkey].next;
//	while (cur) {
//		if (mstrcmp(cur->name, name) == 0) {
//			cur->isEnter = false;
//			return;
//		}
//		cur = cur->next;
//	}
//}
//
//void moveFromNodePoolToRet() {
//	for (int i = 0; i < nodePoolIdx; i++) {
//		if(nodePool[i].isEnter) {
//			ret[retIdx++] = &nodePool[i];
//		}
//	}
//}
//
//// 사전 순으로 정렬, 출력 시 거꾸로 출력할 것임
//void mergeSort(int start, int end) {
//	if (start < end) {
//		int mid = (start + end) / 2;
//		mergeSort(start, mid);
//		mergeSort(mid + 1, end);
//
//		int i = start;
//		int j = mid + 1;
//		int k = start;
//		while (i <= mid && j <= end) {
//			if (mstrcmp(ret[i]->name, ret[j]->name) <= 0) {
//				bufferNode[k++] = ret[i++];
//			}
//			else {
//				bufferNode[k++] = ret[j++];
//			}
//		}
//		while (i <= mid) bufferNode[k++] = ret[i++];
//		while (j <= end) bufferNode[k++] = ret[j++];
//
//		for (int l = start; l <= end; ++l) {
//			ret[l] = bufferNode[l];
//		}
//	}
//}
//
//void swapRet(int l, int r) {
//	NODE* tmp = ret[l];
//	ret[l] = ret[r];
//	ret[r] = tmp;
//}
//
//// 사전 순으로 정렬, 출력 시 거꾸로 출력할 것임
//// 퀵소트로 하면 시간초과 발생!!!
//void quickSort(int leftEnd, int rightEnd) {
//	int l = leftEnd;
//	int r = rightEnd;
//	// mstrcpy보다는 NODE 포인터로 swap 하는게 더 빠르고 편하다
//	NODE* pivot = ret[(l + r) / 2];
//
//	while (l <= r) {
//		while (mstrcmp(ret[l]->name, pivot->name) < 0) l++;
//		while (mstrcmp(pivot->name, ret[r]->name) < 0) r--;
//		if (l <= r) {
//			swapRet(l, r);
//			l++;
//			r--;
//		}
//		if (leftEnd < r) quickSort(leftEnd, r);
//		if (l < rightEnd) quickSort(l, rightEnd);
//	}
//}
//
//int main() {
//
//	//init();
//
//	int N;
//	scanf("%d", &N);
//	char name[MAX_LEN];
//	char enterLeave[6];
//	for (int n = 0; n < N; n++) {
//		scanf("%s %s", name, enterLeave);
//		if (enterLeave[0] == 'e') {
//			insertIntoHashTable(name);
//		}
//		else {
//			removeFromHashTable(name);
//		}
//	}
//	moveFromNodePoolToRet();
//	mergeSort(0, retIdx - 1);
//	for (int i = retIdx - 1; i >= 0; i--) {
//		printf("%s\n", ret[i]);
//	}
//}