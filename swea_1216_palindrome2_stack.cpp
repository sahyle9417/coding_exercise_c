//// 중간에 스택의 top을 0으로 초기화하는 작업을 깜빡했다가 삽질했음, 주의 요망
//
//#include <stdio.h>
//#define MAX 100
//
//char input[MAX][MAX];
//int stack[MAX / 2];
//
//int main() {
//	freopen("input.txt", "r", stdin);
//	for (int tc = 1; tc <= 10; tc++) {
//
//		int t;
//		scanf("%d ", &t);
//
//		for (int i = 0; i < MAX; i++) {
//			for (int j = 0; j < MAX; j++) {
//				scanf("%c ", &input[i][j]);
//			}
//		}
//
//		bool success = false;
//		int len = MAX;
//
//		while (!success && len > 1) {
//
//			int front_end = (len / 2) - 1;
//
//			int rear_start;
//			if (len % 2 == 0) rear_start = len / 2;
//			else rear_start = (len / 2) + 1;
//
//			int top;
//
//			// 가로방향
//			for (int i = 0; i < MAX; i++) {
//				for (int j = 0; j <= MAX - len; j++) {
//					// 스택의 top을 0으로 초기화하는 작업을 깜빡했다가 삽질했음, 주의 요망
//					top = 0;
//					for (int k = 0; k <= front_end; k++) {
//						stack[top++] = input[i][j + k];
//					}
//					success = true;
//					for (int k = rear_start; k < len; k++) {
//						if (stack[--top] != input[i][j + k]) {
//							success = false;
//							break;
//						}
//					}
//					if (success) break;
//				}
//				if (success) break;
//			}
//			if (success) break;
//
//			// 세로방향
//			for (int j = 0; j < MAX; j++) {
//				for (int i = 0; i <= MAX - len; i++) {
//					// 스택의 top을 0으로 초기화하는 작업을 깜빡했다가 삽질했음, 주의 요망
//					top = 0;
//					for (int k = 0; k <= front_end; k++) {
//						stack[top++] = input[i+k][j];
//					}
//					success = true;
//					for (int k = rear_start; k < len; k++) {
//						if (stack[--top] != input[i+k][j]) {
//							success = false;
//							break;
//						}
//					}
//					if (success) break;
//				}
//				if (success) break;
//			}
//			if (success) break;
//
//			else len--;
//		}
//		printf("#%d %d\n", tc, len);
//	}
//}