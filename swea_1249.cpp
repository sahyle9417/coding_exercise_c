//#include <stdio.h>
//#include <malloc.h>
//#define SIZE 102
//#define MAX 2000000000	// int 최대값은 20억
//int N;							// 맵 한변의 길이
//int input[SIZE][SIZE];		// 입력값 그대로 기록
//int dp[SIZE][SIZE];			// 시작점에서 해당 칸 까지의 최단거리
////bool in_q[SIZE][SIZE];		// 동일한 칸을 큐에 2번 이상 넣지 않기 위해 기록, 메모리 절약 효과가 없진 않지만 실효성이 낮은듯 
//char line[SIZE-1];
//
//typedef struct {
//	int i;
//	int j;
//} po;
//
//int di[4] = {1, -1, 0, 0};
//int dj[4] = {0, 0, 1, -1};
//
//void print() {
//	for (int i = 0; i <= N + 1; i++) {
//		for (int j = 0; j <= N + 1; j++) {
//			if (dp[i][j] == MAX) printf("  x ");
//			else printf("%3d ", dp[i][j]);
//		}
//		puts("");
//	}
//}
//
//int main() {
//
//	int max_q_size = 0;
//
//	freopen("input.txt", "r", stdin);
//	int TC;
//	scanf("%d", &TC);
//	for (int tc = 1; tc <= TC; tc++) {
//		for (int i = 0; i < 102; i++) {
//			for (int j = 0; j < 102; j++) {
//				input[i][j] = MAX;
//				dp[i][j] = MAX;
//				//in_q[i][j] = false;
//			}
//		}
//
//		scanf("%d", &N);
//		for (int i = 1; i <= N; i++) {
//			scanf("%s", line);
//			for (int j = 1; j <= N; j++) {
//				input[i][j] = line[j-1] - '0';
//			}
//		}
//		dp[1][1] = 0;
//
//		// 이부분을 define 구문 사용하려다가 이상한 결과가 나왔었다
//		// define 한 것을 토대로 다시 define하고 이를 통해 연산을 수행하면 이상한 결과가 나오는 것 같다.
//		// define 구문 사용은 최소화하고 이를 통해 새로운 변수 쓰고 싶다면 define 구문 없이 그냥 정의하면 될듯!
//		int Q_SIZE = (SIZE - 2)*(SIZE - 2);
//		po* bfs_q = (po*)malloc(sizeof(po)*Q_SIZE);
//		int front = 0;	// pop할 값보다 한칸 전의 위치 가리킴
//		int rear = 1;		// push할 위치 가리킴
//		bfs_q[rear] = { 1, 1 };
//		rear = (rear + 1) % Q_SIZE;
//		//in_q[1][1] = true;
//
//		while (rear != (front + 1) % Q_SIZE) {
//			front = (front+ 1) % Q_SIZE;
//			po from = bfs_q[front];
//			//in_q[from.i][from.j] = false;
//			for (int d = 0; d < 4; d++) {
//				int to_i = from.i + di[d];
//				int to_j = from.j + dj[d];
//				if (dp[from.i][from.j] + input[to_i][to_j] < dp[to_i][to_j]) {	// 갱신 성공, bound 검사 불필요?
//					dp[to_i][to_j] = dp[from.i][from.j] + input[to_i][to_j];
//					//if (tc == 6) { print(); }
//					//if (in_q[to_i][to_j]) continue;
//					bfs_q[rear] = {to_i, to_j};
//					rear = (rear + 1) % Q_SIZE;
//					if ((rear - front + Q_SIZE) % Q_SIZE > max_q_size) max_q_size = (rear - front + Q_SIZE) % Q_SIZE;
//					//in_q[to_i][to_j] = true;
//				}
//			}
//		}
//		//print();
//		//printf("max q size : %d\n", max_q_size);
//		printf("#%d %d\n", tc, dp[N][N]);
//	}
//}