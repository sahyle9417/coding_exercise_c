//#define MAX_STEP 100
//#define MAX_TOOL 50
//#define MAX_TIME 100000
//#define NULL 0
//
//
//// procTime, stepNo 배열의 i, j번째 원소를 swap
//void swap(int i, int j, int procTime[], int stepNo[]) {
//	int tmp = procTime[i];
//	procTime[i] = procTime[j];
//	procTime[j] = tmp;
//
//	tmp = stepNo[i];
//	stepNo[i] = stepNo[j];
//	stepNo[j] = tmp;
//}
//
//
//// procTime, stepNo 배열을 procTime 짧은 순으로 정렬
//void quickSort(int leftEnd, int rightEnd, int procTime[], int stepNo[]) {
//
//	int left = leftEnd;
//	int right = rightEnd;
//	int pivot = procTime[(leftEnd + rightEnd) / 2];
//
//	while (left <= right) {
//		while (procTime[left] < pivot) left++;
//		while (pivot < procTime[right]) right--;
//
//		if (left <= right) {
//			swap(left, right, procTime, stepNo);
//			left++;
//			right--;
//		}
//	}
//	if (leftEnd < right)
//		quickSort(leftEnd, right, procTime, stepNo);
//	if (left < rightEnd)
//		quickSort(left, rightEnd, procTime, stepNo);
//}
//
//
//struct TOOL {
//	int stepNo;
//	int procTime;
//	bool busy;
//	// 같은 STEP에 속하는 TOOL끼리 연결하는 연결리스트 구현을 위한 포인터
//	TOOL* next;
//};
//
//
//TOOL toolArr[MAX_STEP * MAX_TOOL];
//
//
//// 특정 time에 작업이 끝난 tool들의 리스트
//TOOL* completedToolAt[MAX_TIME + 1];
//
//
//struct STEP {
//	TOOL* toolList[MAX_TOOL];
//	int toolNum;
//	int waitingLotNum;
//};
//
//
//STEP stepArr[MAX_STEP];
//
//
//int STEP_NUM;
//int lastTime;
//
//
//int getLotNumAt(int time, STEP* step) {
//	int lotNum = step->waitingLotNum;
//
//	for (int toolIdx = 0; toolIdx < step->toolNum; toolIdx++) {
//		if (step->toolList[toolIdx]->busy) {
//			lotNum++;
//		}
//	}
//
//	return lotNum;
//}
//
//
//// waitingLot을 tool에 할당할 가능성을 가진 step들에 대해 실제 할당 수행
//void allocateWaitingLotAt(int time, STEP* step) {
//
//	if (step->waitingLotNum == 0) {
//		return;
//	}
//
//	// 해당 step에 소속된 tool을 procTime이 짧은 순서대로 상태(작업이 끝났는지) 확인
//	// 대기중이던 lot(waitingLot)들이 모두 할당되면 즉시 break;
//	for (int toolIdx = 0; toolIdx < step->toolNum; toolIdx++) {
//		TOOL* tool = step->toolList[toolIdx];
//
//		// 작업이 끝나지 않은 tool은 스킵, 다음으로 procTime 짧은 tool의 상태 확인할 것임
//		if (tool->busy) { 
//			continue;
//		}
//		
//		// waitingLot이 존재하는 상황에서 작업이 끝나지 않은 tool 발견
//		// waitingLotNum을 해당 tool에게 할당 (waitingLotNum 감소, tool 작업 중 상태로 변경)
//		step->waitingLotNum--;
//		tool->busy = true;
//
//		// completedToolAt 배열에서 작업 끝나는 시점의 연결리스트에 tool을 추가
//		int endTime = time + (tool->procTime);
//		tool->next = completedToolAt[endTime];
//		completedToolAt[endTime] = tool;
//
//		if (step->waitingLotNum == 0) {
//			return;
//		}
//	}
//}
//
//
//
//// 이전까지 수행했던 timestamp(lastTime) 이후로부터 현재 입력받은 timestamp(curTime)까지
//// 작업 수행(완료된 tool에게서 lot 빼기 + waitingLot 할당)
//// completedToolAt 배열 참조하여 작업이 끝난 tool에게서 lot을 빼내는 작업 먼저 수행 후
//// 해당 tool이 속한 step과 다음 step에 대해 waitingLot을 tool에 할당
//void processUntil(int curTime) {
//
//	for (int time = lastTime + 1; time <= curTime; time++) {
//
//		// waitingLot을 tool에 할당할 가능성을 가진 step들의 리스트
//		// 하나의 step에 가용한 tool이 추가되고 처리해야할 lot이 추가되었을 때 해당 step이 stepNeedProcess 배열에 2번 추가될 수 있음
//		int allocatableStep[MAX_STEP * 2];
//		int allocatableStepNum = 0;
//
//		// complete[time]은 해당 time에 작업이 끝나서 비어있게 된 tool들의 리스트
//		for (TOOL* tool = completedToolAt[time]; tool != NULL; tool = tool->next) {
//			
//			// 해당 tool은 더이상 작업 중 상태가 아님
//			tool->busy = false;
//
//			// 다음 step으로 lot 넘기기
//			int nextStepNo = tool->stepNo + 1;
//			stepArr[nextStepNo].waitingLotNum++;
//
//			// waitingLot을 tool에 할당할 가능성을 가진 step들의 리스트
//			// tool->stepNo는 가용한 tool이 하나 추가되었고
//			// tool->stepNo+1은 처리해야할 lot이 하나 추가되었기 때문에 둘다 할당이 필요할 수 있다
//			allocatableStep[allocatableStepNum++] = tool->stepNo;
//			allocatableStep[allocatableStepNum++] = tool->stepNo + 1;
//		}
//
//		// waitingLot을 tool에 할당할 가능성을 가진 step들에 대해 실제 할당 수행
//		for (int i = 0; i < allocatableStepNum; i++) {
//			allocateWaitingLotAt(time, &stepArr[allocatableStep[i]]);
//		}
//	}
//	lastTime = curTime;
//}
//
//
//void init(int N) {
//	STEP_NUM = N;
//	// 각 step 초기화
//	for (int step = 0; step <= STEP_NUM; step++) {
//		stepArr[step].toolNum = 0;
//		stepArr[step].waitingLotNum = 0;
//	}
//	// 각 time에 작업을 완료한 tool이 없는 것으로 초기화
//	for (int time = 0; time <= MAX_TIME; time++) {
//		completedToolAt[time] = NULL;
//	}
//	// timestamp 초기화 (0에서도 동작이 있으므로 -1로 초기화)
//	lastTime = -1;
//}
//
//
//// T : 설비의 수
//// stepNo : 각 설비의 공정번호
//// procTime : 각 설비의 가공시간
//void setupTool(int T, int stepNo[5000], int procTime[5000]) {
//
//	// procTime, stepNo 배열을 procTime 짧은 순으로 정렬
//	quickSort(0, T - 1, procTime, stepNo);
//	
//	// procTime이 짧은 순으로 정렬된 procTime, stepNo 배열을 참조하여
//	// procTime이 짧은 tool부터 toolArr에서 정적할당
//	// 즉, toolArr 배열은 procTime 짧은 순으로 정렬된 tool들이 기록된다
//	for (int toolIdx = 0; toolIdx < T; toolIdx++) {
//		TOOL* tool = &toolArr[toolIdx];
//		tool->stepNo = stepNo[toolIdx];
//		tool->procTime = procTime[toolIdx];
//		tool->busy = false;
//		tool->next = NULL;
//
//		// procTime이 짧은 tool부터 각 step에 할당된다
//		// 각 step의 toolList(큐)에는 해당 step에 소속된 tool들이 저장된다
//		STEP* step = &stepArr[stepNo[toolIdx]];
//		step->toolList[step->toolNum++] = tool;
//	}
//}
//
//
//// 현재시점(time)까지 작업 수행하고 0번 step에 lot 추가 후 0번 step만 process
//void addLot(int time, int number) {
//
//	// 이전까지 수행했던 timestamp(lastTime) 이후로부터 현재 입력받은 timestamp(curTime)까지
//	// 작업 수행(완료된 tool에게서 lot 빼기 + waitingLot 할당)
//	processUntil(time);
//
//	// 0번 step에 대해 waitingLot 할당
//	STEP* step0 = &stepArr[0];
//	step0->waitingLotNum += number;
//	allocateWaitingLotAt(time, step0);
//}
//
//
//int simulate(int time, int wip[MAX_STEP]) {
//
//	// 현재시점(time)까지 작업 수행
//	processUntil(time);
//
//	// 0~STEP_NUM-1까지의 공정에서 작업 중인 lot들의 개수를 wip에 기록
//	for (int step = 0; step < STEP_NUM; step++)
//		wip[step] = getLotNumAt(time, &stepArr[step]);
//
//	// 공정을 모두 마친 lot의 개수 반환
//	// 실제 공정은 0~STEP_NUM-1까지 존재하며 STEP_NUM에 존재하는 lot은 공정이 모두 끝난 것임
//	return getLotNumAt(time, &stepArr[STEP_NUM]);
//}