//#include <stdio.h>
//#include <malloc.h>
//#include <string.h>
//
//typedef struct node {
//	char data;
//	struct node* prev;
//	struct node* next;
//} Node;
//
//typedef struct list {
//	Node* head;
//	Node* tail;
//	Node* cur;
//} List;
//
//List* mylist;
//
//// cur로 지정된 노드 뒤로 새로운 노드 추가, 새로운 노드가 cur로 지정됨
//void append(char c) {
//	Node* newNode = (Node*)malloc(sizeof(Node));
//	newNode->data = c;
//	newNode->prev = mylist->cur;
//	newNode->next = mylist->cur->next;
//	mylist->cur->next->prev = newNode;
//	mylist->cur->next = newNode;
//	mylist->cur = newNode;
//}
//// cur로 지정된 노드 삭제, cur 앞에 있던 노드가 새로운 cur로 지정됨
//// 단 head가 cur로 지정되어있다면 head 다음 노드 삭제
//void remove() {
//	Node* delNode = mylist->cur;
//	delNode->prev->next = delNode->next;
//	delNode->next->prev = delNode->prev;
//	mylist->cur = delNode->prev;
//	free(delNode);
//}
//void print() {
//	//printf("print : ");
//	Node* tmp = mylist->head->next;
//	while (tmp != mylist->tail) {
//		printf("%c", tmp->data);
//		tmp = tmp->next;
//	}
//	puts("");
//}
//
//int main() {
//	mylist = (List*)malloc(sizeof(List));
//	mylist->head = (Node*)malloc(sizeof(Node));
//	mylist->tail = (Node*)malloc(sizeof(Node));
//	//printf("head : %p\n", mylist->head);
//
//	mylist->head->next = mylist->tail;
//	mylist->tail->prev = mylist->head;
//	mylist->cur = mylist->head;
//
//	char str[100001];
//	scanf("%s", str);
//	int i = 0;
//	while (str[i] != '\0') {
//		append(str[i++]);
//	}
//	//print();
//
//	int com_num;
//	scanf("%d ", &com_num);
//
//	for (int cn = 0; cn < com_num; cn++) {
//		char com;
//		// scanf의 whitespacing이 매우 헷갈렸다.
//		scanf(" %c ", &com);
//		if (com == 'L' && mylist->cur != mylist->head) {
//			mylist->cur = mylist->cur->prev;
//		}
//		else if (com == 'D' && mylist->cur != mylist->tail->prev) {
//			mylist->cur = mylist->cur->next;
//		}
//		else if (com == 'P') {
//			char newdata;
//			scanf(" %c", &newdata);
//			append(newdata);
//		}
//		else if (com == 'B' && mylist->cur != mylist->head) {
//			remove();
//		}
//		//print();
//	}
//	print();
//
//	return 0;
//}