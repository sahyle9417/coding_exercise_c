//#include <stdio.h>
//
//// stack의 최대 크기
//#define MAX_N 100
//
//// top은 stack의 인덱스면서 동시에 원소에 개수도 표현
//int top;
//
//// 배열로 stack 구현할 것임, stack의 최대 크기만큼 배열 할당해 초기화
//int stack[MAX_N];
//
//// top 초기화
//void stackInit(void) { top = 0; }
//
//// top 이용해 stack 비었는지 확인
//// top은 인덱스면서 동시에 원소의 개수도 표현 
//int stackIsEmpty(void) { return (top == 0); }
//
//// top 이용해 stack 꽉 찼는지 확인
//// top은 인덱스로 사용함과 동시에 원소의 개수도 표현 
//int stackIsFull(void) { return (top == MAX_N); }
//
//// stack에 삽입(push), 성공 시 1 반환, 실패 시 0 반환
//int stackPush(int value) {
//	if (stackIsFull()) {
//		printf("stack overflow!");
//		return 0;
//	}
//	stack[top] = value;
//	top++;
//	return 1;
//}
//
//// stack의 top에 위치한 값을 포인터 이용해 value에 저장, 성공 시 1 반환, 실패 시 0 반환
//int stackPop(int* value) {
//	if (top == 0) {
//		printf("stack is empty!");
//		return 0;
//	}
//	top--;
//	*value = stack[top];
//	return 1;
//}
//
//int main(int argc, char* argv[]) {
//	int T, N;
//	scanf("%d", &T);
//	for (int test_case = 1; test_case <= T; test_case++) {
//		scanf("%d", &N);
//
//		// top 초기화 (top = 0;)
//		stackInit();
//
//		// stack에 삽입(push)
//		for (int i = 0; i < N; i++) {
//			int value;
//			scanf("%d", &value);
//			stackPush(value);
//		}
//
//		printf("#%d ", test_case);
//
//		// stack에서 값 뽑아냄(pop)
//		while (!stackIsEmpty()) {
//			int value;
//			if (stackPop(&value) == 1) { printf("%d ", value); }
//		}
//		printf("\n");
//	}
//	return 0;
//}
