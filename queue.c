//#include <stdio.h>
//
//#define MAX_N 100
//
//int front;
//int rear;
//int queue[MAX_N];
//
//void queueInit(void) {
//
//	// front는 pop할 위치 가리키는 인덱스, pop하면 증가
//	// 항상 '0 <= front <= MAX_N' 조건 성립
//	front = 0;
//
//	// rear는 push할 위치 가리키는 인덱스, push하면 증가
//	// 항상 '0 <= rear <= MAX_N' 조건 성립
//	rear = 0;
//}
//
//// queue 비어있는지 여부 확인하는 함수, 비어있다면 1, 비어있지 않다면 0
//int queueIsEmpty(void) { return (front == rear); }
//
//// queue 가득 찼는지 여부 확인하는 함수, 가득차있다면 1, 가득차있지 않다면 0
//int queueIsFull(void) {
//	// rear 바로 다음칸이 front인 경우 queue 가득 찼음
//	// 삽입할 위치(rear+1)를 front가 가리키고 있다면 해당 위치에 이미 원소 존재하는 것
//	if ((rear + 1) % MAX_N == front) { return 1; }
//	else { return 0; }
//}
//
//// 매개변수로 전달받은 value를 queue의 rear 위치에 삽입
//// rear 1 증가, '0 <= rear <= MAX_N' 조건 성립하도록 조정
//// 삽입 성공 시 1 반환, 삽입 실패 시 0 반환
//int queueEnqueue(int value) {
//	if (queueIsFull()) {
//		printf("queue is full!");
//		return 0;
//	}
//	queue[rear] = value;
//	// 삽입 후 rear 1 증가
//	rear++;
//	// rear 1 증가 후 '0 <= rear <= MAX_N' 조건이 성립하도록 조정
//	if (rear == MAX_N) { rear = 0; }
//	return 1;
//}
//
//// queue의 front에 위치한 원소를 포인터 이용해 value에 저장
//// value에 출력한 뒤 front 값 증가
//// '0 <= front <= MAX_N' 조건이 성립하도록 front값 조정
//int queueDequeue(int* value) {
//	if (queueIsEmpty()) {
//		printf("queue is empty!");
//		return 0;
//	}
//	*value = queue[front];
//	front++;
//	if (front == MAX_N) { front = 0; }
//	return 1;
//}
//
//int main(int argc, char* argv[]) {
//	int T;
//	int N;
//	scanf("%d", &T);
//	for (int test_case = 1; test_case <= T; test_case++) {
//		scanf("%d", &N);
//
//		queueInit();
//		for (int i = 0; i < N; i++) {
//			int value;
//			scanf("%d", &value);
//			queueEnqueue(value);
//			printf("setValue");
//		}
//
//		printf("#%d ", test_case);
//
//		while (!queueIsEmpty()) {
//			int value;
//			if (queueDequeue(&value) == 1) { printf("%d ", value); }
//		}
//		printf("\n");
//	}
//	return 0;
//}
