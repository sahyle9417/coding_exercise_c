//#include <stdio.h>
//#define MAX 502
//
//char X[MAX];
//char Y[MAX];
//// dp[i][j] : X에서 0~i번째 인덱스와 Y의 0~j번째 인덱스의 범위에서 일치하는 문자열의 개수?
//int dp[MAX][MAX] = { 0 };
//int N, lcsLen;
//
//void print() {
//	for (int i = 1; i <= N; i++) {
//		for (int j = 1; j <= N; j++) {
//			printf("%3d ", dp[i][j]);
//		}
//		puts("");
//	}
//}
//
//int main() {
//	freopen("input.txt", "r", stdin);
//	int tc;
//	scanf("%d", &tc);
//	for (int testCase = 1; testCase <= tc; testCase++) {
//		scanf("%d%s%s", &N, X + 1, Y + 1);
//		for (int i = 1; i <= N; i++) {			// X 한글자씩 순회
//			for (int j = 1; j <= N; j++) {		// Y 한글자씩 순회
//				// 해당 위치의 글자가 일치하는 경우, 좌측 상단 값 + 1 기록
//				if (X[i] == Y[j]) { dp[i][j] = dp[i - 1][j - 1] + 1; }
//				// 해당 위치의 글자가 일치하지 않는 경우
//				// dp[i-1][j]과 dp[i][j-1] 중에 더 큰 것을 dp[i][j]에 기록
//				else if (dp[i - 1][j] > dp[i][j - 1]) { dp[i][j] = dp[i - 1][j]; }
//				else { dp[i][j] = dp[i][j - 1]; }
//			}
//		}
//
//		printf("#%d %.2lf\n", testCase, (double)dp[N][N] / N * 100);
//		print();
//	}
//
//	return 0;
//}