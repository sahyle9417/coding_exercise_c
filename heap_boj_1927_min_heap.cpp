//
//// 최소힙
//
//#include <stdio.h>
//#define MAX_SIZE 100001
//
//int heap[MAX_SIZE];
//int heapSize = 0;
//
//void swap(int i, int j) {
//	int tmp = heap[i];
//	heap[i] = heap[j];
//	heap[j] = tmp;
//}
//
//void push(int num) {
//	heap[++heapSize] = num;
//	int idx = heapSize;
//	while (idx > 1 && heap[idx / 2] > heap[idx]) {
//		swap(idx, idx / 2);
//		idx = idx / 2;
//	}
//}
//
//int pop() {
//	if(heapSize==0) return 0;
//	int ret = heap[1];
//	heap[1] = heap[heapSize--];
//	int parent = 1;
//	int child;
//	while (true) {
//		// 자식이 존재하는지 검사
//		child = parent * 2;
//		if (child > heapSize) 
//			break;
//		// 두명의 자식 중 더 작은 자식 선택
//		if (child + 1 <= heapSize && heap[child] > heap[child + 1]) 
//			child++;
//		// 둘 중 더 작은 자식보다 내가 더 작아서 더 이상 바꿀필요 없음
//		if (heap[parent] < heap[child])
//			break;
//		// 내가 더 작지 않아서(크거나 같아서) 바꿀 필요 있음
//		swap(parent, child);
//		// 이거 빼먹어서 틀렸었음
//		parent = child;
//	}
//	return ret;
//}
//
//int main() {
//	int N;
//	scanf("%d", &N);
//	while (N--) {
//		int num;
//		scanf("%d", &num);
//
//		if (num == 0) {
//			printf("%d\n", pop());
//		}
//		else {
//			push(num);
//		}
//	}
//}