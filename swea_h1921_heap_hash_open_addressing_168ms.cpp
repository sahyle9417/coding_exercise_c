//#define NULL 0
//#define MAX_USER 1000
//#define MAX_AREA 10
//#define MAX_COST 1000
//#define MAX_FRIEND 20
//#define MAX_HEAP 10500	// 하나의 지역에 생성될 수 있는 여행상품(PACK)의 개수에 따라 설정
//#define MAX_PACK 100000
//
//
//int USER_NUM, AREA_NUM;
//
//
//typedef long long LL;
//const LL MAX_PID = 1000000000;
//const LL MAX_PRIORITY = MAX_PID * MAX_PACK;
//
//
//typedef struct _user {
//	int uid;
//	int friendNum;
//	int myReserve[MAX_AREA + 1];
//	_user* friends[MAX_FRIEND];
//} USER;
//
//
//typedef struct _pack {
//	int pid;
//	int area;
//	bool reserved;
//	LL priority;	// 작은 값을 가질수록 우선순위 높음
//} PACK;
//
//
//typedef struct _area {
//	int packNum;
//
//	// 최소 힙 (최소값을 갖는 원소가 Root에 위치)
//	PACK* heap[MAX_HEAP];
//	int heapSize;
//
//	void push(PACK* newPack) {
//		packNum++;
//
//		heap[++heapSize] = newPack;
//		int index = heapSize;
//
//		while (index >= 2 && heap[index / 2]->priority > heap[index]->priority) {
//			PACK* tmp = heap[index / 2];
//			heap[index / 2] = heap[index];
//			heap[index] = tmp;
//
//			index = index / 2;
//		}
//	}
//
//	void pop() {
//		heap[1] = heap[heapSize--];
//		
//		int parent = 1;
//
//		while (true) {
//			int child = parent * 2;
//
//			if (child > heapSize) {
//				break;
//			}
//
//			// 둘중에 더 작은 자식을 선택
//			if (child + 1 <= heapSize && heap[child]->priority > heap[child + 1]->priority) {
//				child++;
//			}
//
//			// 둘중에 더 작은 자식보다 부모가 더 작다면 더 이상 swap 수행하지 않음
//			if (heap[parent]->priority < heap[child]->priority) {
//				break;
//			}
//
//			PACK* tmp = heap[child];
//			heap[child] = heap[parent];
//			heap[parent] = tmp;
//
//			parent = child;
//		}
//	}
//
//	// 예약되지 않은 여행상품(PACK) 중 우선순위 값이 가장 작은 루트노드 반환
//	PACK* top() {
//		// 이미 예약된 여행상품(PACK)은 버리기 (어차피 예약취소 기능 없기 때문)
//		while (heap[1]->reserved) {
//			pop();
//		}
//		// 예약되지 않은 여행상품(PACK) 발견 시 pop하지 않고 반환
//		return heap[1];
//	}
//} AREA;
//
//
//USER userArr[MAX_USER + 1];
//PACK packArr[MAX_PACK];
//AREA areaArr[MAX_AREA + 1];
//
//
//// open_addressing 방식의 해시테이블에 새로운 여행상품(PACK) 추가
//PACK* genPACK(int pid) {
//	int index = pid % MAX_PACK;
//	while (packArr[index].pid) {
//		index = (index + 1) % MAX_PACK;
//	}
//	return &packArr[index];
//}
//
//
//// open_addressing 방식의 해시테이블에서 주어진 pid를 갖는 여행상품(PACK) 반환
//PACK* getPACK(int pid) {
//	int index = pid % MAX_PACK;
//	while (packArr[index].pid != pid) {
//		index = (index + 1) % MAX_PACK;
//	}
//	return &packArr[index];
//}
//
//
//void init(int N, int M) {
//
//	USER_NUM = N;
//	AREA_NUM = M;
//
//	// 지역 초기화
//	for (int areaIdx = 1; areaIdx <= AREA_NUM; areaIdx++) {
//		areaArr[areaIdx].heapSize = 0;
//		areaArr[areaIdx].packNum = 0;
//	}
//
//	// 사용자 초기화
//	for (int userIdx = 1; userIdx <= USER_NUM; userIdx++) {
//		USER* user = &userArr[userIdx];
//		user->friendNum = 0;
//		// 각 사용자의 모든 지역에 대한 예약 정보 초기화
//		for (int areaIdx = 1; areaIdx <= AREA_NUM; areaIdx++) {
//			user->myReserve[areaIdx] = 0;
//		}
//	}
//
//	// 여행상품(PACK) 초기화
//	for (int packIdx = 0; packIdx < MAX_PACK; packIdx++) {
//		packArr[packIdx].pid = 0;
//	}
//}
//
//
//void befriend(int uid1, int uid2) {
//	USER* u1 = &userArr[uid1];
//	USER* u2 = &userArr[uid2];
//
//	u1->friends[u1->friendNum++] = u2;
//	u2->friends[u2->friendNum++] = u1;
//}
//
//
//// 새로운 여행상품(PACK) 추가
//void add(int pid, int area, int price) {
//	PACK* newPack = genPACK(pid);
//	newPack->pid = pid;
//	newPack->area = area;
//	newPack->reserved = false;
//	newPack->priority = price * MAX_PID + pid - 1;
//	// 해당 지역의 힙에 새로운 여행상품(PACK) push
//	areaArr[area].push(newPack);
//}
//
//
//void reserve(int uid, int pid) {
//	PACK* pack = getPACK(pid);
//	pack->reserved = true;
//	areaArr[pack->area].packNum--;
//
//	USER* user = &userArr[uid];
//	user->myReserve[pack->area]++;
//}
//
//
//int recommend(int uid) {
//
//	USER* user = &userArr[uid];
//
//	// 자신과 친구들이 각 지역에 대해 예약한 횟수
//	int reserve[MAX_AREA + 1];
//
//	// 나의 예약
//	for (int areaIdx = 1; areaIdx <= AREA_NUM; areaIdx++) {
//		reserve[areaIdx] = user->myReserve[areaIdx];
//	}
//
//	// 친구들의 예약
//	for (int friendIdx = 0; friendIdx < user->friendNum; friendIdx++) {
//		USER* f = user->friends[friendIdx];
//		for (int areaIdx = 1; areaIdx <= AREA_NUM; areaIdx++) {
//			reserve[areaIdx] += f->myReserve[areaIdx];
//		}
//	}
//
//	// 각 지역별 예약횟수를 참조하여 예약이 많이 발생한 지역 순으로 삽입정렬
//	int sortedArea[MAX_AREA + 1];
//
//	// 삽입정렬은 처음에 하나의 원소를 무조건 넣어놓고 시작
//	// 하나의 원소만 존재할 때는 정렬되어 있는 상태라고 할 수 있음
//	sortedArea[1] = 1;
//
//	// 정렬 안된 지역(areaIdx)을 하나씩 골라서 맞는 위치에 삽입
//	for (int areaIdx = 2; areaIdx <= AREA_NUM; areaIdx++) {
//
//		// sortedArea 배열에서 1부터 areaIdx - 1까지 이동하며 삽입할 위치 탐색
//		int insertIdx = 1;
//		for (; insertIdx < areaIdx; insertIdx++) {
//
//			// 삽입할 원소의 예약 수가 insertIdx 위치에 있던 원소의 예약 수보다 더 많다면
//			// 해당 위치에 삽입하면 되므로 해당 위치부터 오른쪽(뒤쪽)으로 한칸씩 밀기
//			if (reserve[areaIdx] > reserve[sortedArea[insertIdx]]) {
//				for (int tmp = areaIdx; tmp > insertIdx; tmp--) {
//					sortedArea[tmp] = sortedArea[tmp - 1];
//				}
//				// 밀고나서 생긴 위치에 실제로 원소를 삽입
//				sortedArea[insertIdx] = areaIdx;
//				break;
//			}
//		}
//
//		// sortedArea 배열에서 areaIdx - 1까지 이동했지만 아직 삽입할 위치 찾지 못함
//		// 삽입할 원소가 맨 뒤(areaIdx)에 위치해야 하는 경우이므로 별도의 로직으로 처리해줘야 함
//		// 중간(1 ~ areaIdx-1)에 넣는 것이 아니라 맨 뒤(areaIdx)에 삽입
//		if (insertIdx == areaIdx) {
//			sortedArea[areaIdx] = areaIdx;
//		}
//	}
//
//	// 예약 횟수가 가장 많으면서 같은 예약 횟수를 가지는 모든 지역들(공동 1위들)의 범위(startIdx <= 지역번호 < endIdx)를 구함
//	// 가장 많이 예약된 지역부터 여행상품(PACK)이 하나라도 존재(targetPackNum > 0)할 때까지 반복
//	int targetPackNum = areaArr[sortedArea[1]].packNum;
//	int startIdx = 1;
//	int endIdx = 2;
//	for (; endIdx <= AREA_NUM; endIdx++) {
//		// 예약 횟수가 가장 많으면서 같은 예약 횟수를 가지는 모든 지역들(공동 1위들)은 모두 고려 대상
//		// 모든 지역에 예약 횟수가 하나도 없는 경우 모든 지역이 고려 대상이 된다
//		if (reserve[sortedArea[endIdx]] == reserve[sortedArea[endIdx - 1]]) {
//			targetPackNum += areaArr[sortedArea[endIdx]].packNum;
//		}
//		// 예약 횟수가 다른 지역 발견, 고려 대상 여행상품(PACK)의 개수가 존재하는지 반드시 확인해야 함
//		// 남아있는 여행상품(PACK)이 하나도 없으면 그 다음으로 예약 횟수 많은 지역들 고려해야함
//		else if (targetPackNum > 0) {
//			break;
//		}
//	}
//
//	// 가장 작은 priority 값을 가지는 여행상품(PACK)의 pid(retPid)를 구할 것임
//	// minPriority에는 실시간으로 가장 작은 priority값 기록됨
//	// minPriority의 초기값은 priority가 가질 수 있는 가장 큰 값으로 설정
//	LL minPriority = MAX_PRIORITY;
//
//	int retPid = -1;
//
//	for (int i = startIdx; i < endIdx; i++) {
//		AREA* area = &areaArr[sortedArea[i]];
//
//		if (area->packNum > 0) {
//			PACK* pack = area->top();
//
//			if (pack->priority < minPriority) {
//				minPriority = pack->priority;
//				retPid = pack->pid;
//			}
//		}
//	}
//
//	return retPid;
//}