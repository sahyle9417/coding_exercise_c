//#include <stdio.h>
//
//// 생년월일 6자리를 초과한 입력을 무시하기 위해 개행 만나기 전까지 입력버퍼 읽고 무시
//// 버퍼를 제대로 비워주지 않는다면
//// 6자리를 초과해 입력하는 경우 당연히 의도대로 동작하지 않고
//// 6자리 맞춰서 입력하더라도 입력버퍼에 개행문자가 남아있어 이름 입력 시 개행문자만 입력된다.
//void clearbuffer(void) {
//	while (getchar() != '\n') {}
//}
//
//int main(void) {
//	char birth[7];		// 생년월일 6자리
//	char name[10];	// 이름
//	
//	fputs("생년월일 입력 : ", stdout);
//	fgets(birth, sizeof(birth), stdin);	// id를 채우기 위해 6자리 입력받음
//	clearbuffer();					// id를 채운 뒤 입력버퍼 비우는 함수 (개행까지 무시)
//
//	fputs("이름 입력 : ", stdout);
//	fgets(name, sizeof(name), stdin);	// 버퍼에 저장된 스트림을 name 배열에 입력
//
//	printf("생년월일 출력 : %s\n", birth);
//	//fputs("생년월일 출력 : ", stdout);
//	//fputs(birth, stdout);
//	//fputs("\n", stdout);
//
//	printf("이름 출력 : %s\n", name);
//	//fputs("이름 출력 : ", stdout);
//	//fputs(name, stdout);
//	//fputs("\n", stdout);
//}