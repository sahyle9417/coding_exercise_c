//#include <stdio.h>
//#define MAXR  100
//#define MAXC  26
//#define NUL          0
//#define QSIZE     1000
//#define NODEPOOL 10000 
//
//struct COORD {
//	int y, x;
//};
//
//// 좌표(COORD)들 담는 큐와 관련 연산(push, pop)을 포함하는 구조체
//struct QUEUE {
//
//	// 좌표(COORD)들 담는 큐
//	COORD queue[QSIZE];
//
//	int head = 0, tail = 0, size = 0;
//	void init() { head = tail = size = 0; }
//
//	// 주어진 좌표가 큐에 없다면 추가 후 true 반환
//	bool push_once(int y, int x) {
//		int j = head;
//		for (int i = 0; i < size; i++) {
//			if (queue[j].y == y && queue[j].x == x) { return false; }
//			if (++j == QSIZE) { j = 0; } // 원형 큐
//		}
//		push(y, x);
//		return true;
//	}
//
//	// 중복검사가 완료된, 큐에 없던 새로운 좌표를 큐에 추가
//	void push(int y, int x) {
//		size++;
//		queue[tail].y = y; queue[tail].x = x;
//		if (++tail == QSIZE) tail = 0;
//	}
//
//	void push(COORD item) {
//		size++;
//		queue[tail] = item;
//		if (++tail == QSIZE) tail = 0;
//	}
//
//	COORD pop() {
//		size--;
//		COORD res = queue[head];
//		if (++head == QSIZE) head = 0;
//		return res;
//	}
//};
//
//// 좌표(COORD)들 담는 큐와 관련 연산(push, pop)을 포함하는 구조체
//QUEUE wque;
//
//// 함수의 유형(type), 피연산자 고정여부(fixed), 피연산자 좌표(a,b) 저장
//struct FUNC {
//	int type;
//	bool fixed;
//	COORD a, b;
//};
//
//// 각 칸에 담긴 함수의 정보(FUNC 구조체) 정보 담음
//// FUNC 구조체 : 함수의 유형(type), 피연산자 고정여부(fixed), 피연산자 좌표(a,b) 저장
//// 처음에는 empty로 초기화 (type=VAL, fixed=true, a.y=a.x=b.y=b.x=0)
//FUNC fun[MAXR][MAXC];
//
//// 테이블의 최종값 기록할 배열
//int  val[MAXR][MAXC];
//
//// 현재 cell의 좌표와 해당 좌표를 참조하는 좌표를 가리키는 포인터를 담고 있는 노드
//// 단방향 연결리스트처럼 동작
//// 하나의 노드가 고정되지 않은 상태가 되면 해당 노드에서 next 방향으로 줄줄이 고정되지 않은 상태가 된다
//struct NODE { 
//	int y, x;
//	NODE* next;
//};
//
//NODE nodepool[10000];
//int np;
//
//// 각각의 좌표"를" 참조하는 좌표들을 단방향 연결리스트 형태로 기록
//NODE* ref[MAXR][MAXC];
//
//// 함수명의 3번째 글자만 사용
//enum { VAL = 0, ADD = 1, SUB = 2, MUL = 3, DIV = 4, MAX = 5, MIN = 6, SUM = 7 };
//char funchar[8] = { ' ','D','B','L','V','X','N','M' };
//
//int R, C;
//
//
//// ref 배열의 피연산자 칸(ry, rx)에 자신을 참조하는 칸들이 연결리스트 형태로 기록되어 있음
//// 즉, 피연산자 칸(ry, rx)이 변경되었을 때 연쇄적으로 변경되어야 할 칸들이 연결리스트로 기록되어 있는 것
//// 새로운 참조 관계를 ref 배열의 피연산자 칸(ry, rx)에 추가하기 위해
//// 연산 결과 칸(y, x)을 ref 배열의 피연산자 칸(ry, rx)의 연결리스트 맨 앞에 추가
//void setRef(int y, int x, int ry, int rx) {
//	NODE* newNode = &nodepool[np++];
//	newNode->y = y;
//	newNode->x = x;
//	newNode->next = ref[ry][rx];
//	ref[ry][rx] = newNode;
//}
//
//// 현재 좌표(y,x)가 참조하는 다른 좌표들(fun[y][x].a, fun[y][x].b)을 ref배열에 기록
//void setRef(int y, int x) {
//	if (fun[y][x].type < MAX) {
//		setRef(y, x, fun[y][x].a.y, fun[y][x].a.x);
//		setRef(y, x, fun[y][x].b.y, fun[y][x].b.x);
//	}
//	else {
//		for (register int ry = fun[y][x].a.y; ry <= fun[y][x].b.y; ry++) for (register int rx = fun[y][x].a.x; rx <= fun[y][x].b.x; rx++)
//			setRef(y, x, ry, rx);
//	}
//}
//
//bool isFixed(int type, int y1, int x1, int y2, int x2);
//int calcValue(int type, int y1, int x1, int y2, int x2);
//int calcValue(int y, int x);
//
//// 연산 결과 기록 칸(y, x)이 피연산자 칸(ry, rx)을 더이상 참조하지 않게 변경
//// 참조 관계 리스트(ref[ry][rx]) 순회하며 연산 결과 기록 칸(y,x) 검색
//void removeRef(int y, int x, int ry, int rx) {
//	NODE* pre = NUL;
//	for (NODE* cur = ref[ry][rx]; cur; cur = cur->next) {
//		// 참조 관계 리스트(ref[ry][rx])에서 연산 결과 기록 칸(y,x) 발견
//		// 삭제할 연산 결과 기록 칸(y, x)만 제거
//		if (ref[ry][rx]->y == y && ref[ry][rx]->x == x) {
//			if (pre == NUL)
//				ref[ry][rx] = cur->next;
//			else
//				pre->next = cur->next;
//			return;
//		}
//		pre = cur;
//	}
//}
//
//// 현재 좌표(y,x)가 다른 좌표(ry, rx)를 더이상 참조하지 않게 변경됨
//void removeRef(int y, int x) {
//	// 피연산자가 2개인 연산들
//	if (fun[y][x].type < MAX) {
//		removeRef(y, x, fun[y][x].a.y, fun[y][x].a.x);
//		removeRef(y, x, fun[y][x].b.y, fun[y][x].b.x);
//	}
//	// 피연산자가 2개 이상일 수 있는 연산들 (MAX, MIN, SUM)
//	else {
//		for (register int ry = fun[y][x].a.y; ry <= fun[y][x].b.y; ry++) for (register int rx = fun[y][x].a.x; rx <= fun[y][x].b.x; rx++)
//			removeRef(y, x, ry, rx);
//	}
//}
//
//// 주어진 좌표 y,x를 큐에 넣은 뒤 y,x를 unfixed로 설정
//void setUnfixed(int ry, int rx) {
//	int y, x;
//	for (NODE* cur = ref[ry][rx]; cur; cur = cur->next) {
//		y = cur->y;
//		x = cur->x;
//		fun[y][x].fixed = false;
//		if (wque.push_once(y, x)) { setUnfixed(y, x); }
//	}
//}
//
//void setValues() {
//	COORD cell;
//	int type, y1, x1, y2, x2;
//	// 연산 수행이 필요한 칸들의 큐(wque)를 비울때까지 연산 수행
//	// 함수 호출 순서가 문제일 뿐 언젠간 큐는 비게된다.
//	while (wque.size) {
//		cell = wque.pop();
//		type = fun[cell.y][cell.x].type;
//		// 함수가 숫자로 update된 경우
//		if (type == 0) {
//			fun[cell.y][cell.x].fixed = true;
//			continue;
//		}
//		y1 = fun[cell.y][cell.x].a.y;
//		x1 = fun[cell.y][cell.x].a.x;
//		y2 = fun[cell.y][cell.x].b.y;
//		x2 = fun[cell.y][cell.x].b.x;
//		// 모든 피연산자가 고정되면 연산 수행 후 val에 기록
//		if (isFixed(type, y1, x1, y2, x2)) {
//			val[cell.y][cell.x] = calcValue(type, y1, x1, y2, x2);
//			fun[cell.y][cell.x].fixed = true;
//		}
//		// 고정되지 않은 피연산자가 있다면 큐에 추가
//		else
//			wque.push_once(cell.y, cell.x);
//	}
//}
//
//void init(int c, int r) {
//	R = r;
//	C = c;
//	FUNC empty;
//	empty.type = VAL;
//	empty.fixed = true;
//	empty.a.y = empty.a.x = empty.b.y = empty.b.x = 0;
//
//	for (register int i = 0; i < R; i++) for (register int j = 0; j < C; j++) {
//		val[i][j] = 0;
//		fun[i][j] = empty;
//		ref[i][j] = NUL;
//	}
//	np = 0;
//}
//
//void set(int col, int row, char input[]) {
//	register int i, j;
//	int sign;
//	// col, row는 1부터 시작하므로 1빼고 시작함.
//	col--; row--;
//	// 상수가 입력되었으므로 해당 칸에서 비롯된 참조관계들을 삭제
//	if (fun[row][col].type != VAL) { removeRef(row, col); }
//	// 상수 입력
//	if (input[0] < 'A') {
//		//if (input[0] == '-' || ('0' <= input[0] && input[0] <= '9')) {
//		if (input[0] != '-') { sign = 1;   i = 0; }
//		else { sign = -1;  i = 1; }
//		val[row][col] = 0;
//		for (; input[i] != 0; i++) { val[row][col] = val[row][col] * 10 + (input[i] - '0'); }
//		val[row][col] *= sign;
//		fun[row][col].type = VAL;
//	}
//	// 함수 입력
//	else {
//		for (i = ADD; i <= SUM; i++) {
//			if (input[2] == funchar[i]) {
//				fun[row][col].type = i;
//
//				j = 4;
//				fun[row][col].a.x = (input[j++] - 'A');		// 1번째 피연산자의 x좌표
//				fun[row][col].a.y = 0;						// 1번째 피연산자의 y좌표
//				while ('0' <= input[j] && input[j] <= '9') {
//					fun[row][col].a.y = fun[row][col].a.y * 10 + (input[j++] - '0');
//				}
//				fun[row][col].a.y--;
//				j++;	// 쉼표(,) 건너뛰기
//				fun[row][col].b.x = (input[j++] - 'A');		//  2번째 피연산자의 x좌표
//				fun[row][col].b.y = 0;						//  2번째 피연산자의 y좌표
//				while ('0' <= input[j] && input[j] <= '9') {
//					fun[row][col].b.y = fun[row][col].b.y * 10 + (input[j++] - '0');
//				}
//				fun[row][col].b.y--;
//				// 실제 연산 수행
//				val[row][col] = calcValue(row, col);
//				setRef(row, col);
//				break;
//			}
//		}
//	}
//
//	fun[row][col].fixed = true;
//	setUnfixed(row, col);
//	setValues();
//}
//
//inline int Min(int a, int b) { return a < b ? a : b; }
//inline int Max(int a, int b) { return a > b ? a : b; }
//
//// 함수명과 2개의 피연산자가 주어질 때, 해당 함수가 실행 가능한지 확인
//bool isFixed(int type, int y1, int x1, int y2, int x2) {
//	// 함수가 좌표 2개만 사용
//	if (type < MAX) {
//		if (fun[y1][x1].fixed == false || fun[y2][x2].fixed == false) { return false; }
//	}
//	// 함수가 좌표 2개 이상 사용 가능 (MAX, MIN, SUM)
//	else {
//		for (register int y = y1; y <= y2; y++) for (register int x = x1; x <= x2; x++)
//			if (fun[y][x].fixed == false) { return false; }
//	}
//	return true;
//}
//
//// 실제 연산 수행 후 결과 반환
//int calcValue(int y, int x) {
//	return calcValue(fun[y][x].type, fun[y][x].a.y, fun[y][x].a.x, fun[y][x].b.y, fun[y][x].b.x);
//}
//
//// 실제 연산 수행 후 결과 반환
//int calcValue(int type, int y1, int x1, int y2, int x2) {
//	int res;
//	if (type == ADD) { res = val[y1][x1] + val[y2][x2]; }
//	else if (type == SUB) { res = val[y1][x1] - val[y2][x2]; }
//	else if (type == MUL) { res = val[y1][x1] * val[y2][x2]; }
//	else if (type == DIV) { res = val[y1][x1] / val[y2][x2]; }
//	else if (type == MAX) {
//		res = val[y1][x1];
//		for (register int y = y1; y <= y2; y++) for (register int x = x1; x <= x2; x++)
//			res = Max(res, val[y][x]);
//	}
//	else if (type == MIN) {
//		res = val[y1][x1];
//		for (register int y = y1; y <= y2; y++) for (register int x = x1; x <= x2; x++)
//			res = Min(res, val[y][x]);
//	}
//	else { // SUM
//		res = 0;
//		for (register int y = y1; y <= y2; y++) for (register int x = x1; x <= x2; x++)
//			res += val[y][x];
//	}
//	return res;
//}
//
//
//void update(int value[MAXR][MAXC]) {
//	// val 배열의 내용을  value로 옮긴다.
//	for (register int i = 0; i < R; i++) for (register int j = 0; j < C; j++)
//		value[i][j] = val[i][j];
//}
