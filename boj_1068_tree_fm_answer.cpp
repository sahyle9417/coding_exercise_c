//#include <stdio.h>
//#define MAX_N 51
//
//typedef struct _node {
//	_node* parent;
//	bool deleted;
//	int childNum;
//	_node* child[MAX_N];
//
//	bool isLeaf() {
//		if (childNum == 0 && !deleted) return true;
//		else return false;
//	}
//} NODE;
//
//NODE nodePool[MAX_N];
//
//// 재귀적으로 모든 후손(자식, 자식의 자식, ...)노드를 제거
//void deleteNode(NODE* node) {
//	node->deleted = true;
//	node->parent->childNum--;	// 이 부분 때문에 root노드가 삭제되는 경우는 따로 처리해줘야 함
//	while (node->childNum)		// 모든 자식노드에 대해 재귀호출
//		deleteNode(node->child[node->childNum - 1]);
//	/*
//	// 위의 while문과 동일한 역할 수행
//	int childNum = node->childNum;
//	for (int i = 0; i < childNum; i++) {
//		deleteNode(node->child[i]);
//	}*/
//}
//
//void init(int N) {
//	for (int i = 0; i < N; i++) {
//		nodePool[i].childNum = 0;
//		nodePool[i].parent = 0;
//		nodePool[i].deleted = false;
//	}
//}
//
//int main() {
//	
//	int N;
//	scanf("%d", &N);
//
//	init(N);
//
//	int parent;
//	int root = 0;
//	for (int i = 0; i < N; i++) {
//		scanf("%d", &parent);
//		if (parent != -1) {
//			nodePool[i].parent = &nodePool[parent];
//			nodePool[parent].child[nodePool[parent].childNum++] = &nodePool[i];
//		}
//		else {
//			root = i;
//		}
//	}
//	int delNode;
//	scanf("%d", &delNode);
//	if (delNode == root) {
//		printf("0\n");
//		return 0;
//	}
//
//	deleteNode(&nodePool[delNode]);
//
//	int count = 0;
//	for (int i = 0; i < N; i++) {
//		if (nodePool[i].isLeaf()) count++;
//	}
//
//	printf("%d\n", count);
//	return 0;
//}