//#include <stdio.h>
//#include <string.h>
//
//int main(void) {
//	char str[100];
//
//	// fputs, printf는 puts와 달리 맨 끝에 개행을 첨가하지 않는다.
//	fputs("문자열 입력 : ", stdout);
//	// fgets는 gets, scanf와 달리 맨끝 개행문자까지 입력받는다.
//	fgets(str, sizeof(str), stdin);
//
//	printf("맨끝 개행 포함 길이 : %d\n", strlen(str));
//	printf("맨끝 개행 포함 출력 : %s", str);
//	
//	// 맨끝에 위치한 개행문자를 널문자(아스키값:0)로 바꿔서 개행문자를 제거한다.
//	str[strlen(str) - 1] = 0;
//	printf("맨끝 개행 제외 길이 : %d\n", strlen(str));
//	printf("맨끝 개행 제외 출력 : %s", str);
//
//}