//#define NULL 0
//
//typedef long long int LL;
//
//const int MAX_N = 1000;
//const int MAX_AREA = 10;
//const int MAX_COST = 1000;
//const int MAX_FRIENDS = 20;
//const int MAX_HEAP = 10200;
//const int MAX_PACK = 123412;
//const LL MAX_PID = 1000000000;
//const LL INF = MAX_PID * MAX_PACK;
//
//int nUser, nAreas;
//
//struct User {
//	int uid;
//	int nfriends;
//	int cnt_area[MAX_AREA + 1];
//	User* friends[MAX_FRIENDS];
//};
//
//struct Pack {
//	int pid;
//	int area;
//	int reserved;
//	LL d;
//};
//
//struct Area {
//	//Pack* packs;
//	int num_area;
//
//	int hSize;
//	Pack* heap[MAX_HEAP];
//
//	void push(Pack* pack)
//	{
//		Pack* t;
//		register int index = hSize;
//		heap[hSize++] = pack;
//
//		num_area++;
//
//		while (index && heap[(index - 1) / 2]->d > heap[index]->d)
//		{
//			t = heap[(index - 1) / 2];
//			heap[(index - 1) / 2] = heap[index];
//			heap[index] = t;
//			index = (index - 1) / 2;
//		}
//	}
//
//	void pop()
//	{
//		register int cur = 0, l, r, next;
//
//		heap[0]->pid = heap[0]->reserved = 0;
//
//		heap[0] = heap[--hSize];
//
//		Pack* t;
//		while (true)
//		{
//			l = cur * 2 + 1;
//			r = cur * 2 + 2;
//
//			if (hSize <= l)
//			{
//				break;
//			}
//
//			next = cur;
//
//			if (heap[next]->d > heap[l]->d)
//			{
//				next = l;
//			}
//
//			if (r < hSize && heap[next]->d > heap[r]->d)
//			{
//				next = r;
//			}
//
//			if (next == cur)
//			{
//				break;
//			}
//
//			t = heap[next];
//			heap[next] = heap[cur];
//			heap[cur] = t;
//			cur = next;
//		}
//	}
//
//	// always call hSize > 0, num_area > 0
//	Pack* top()
//	{
//		while (heap[0]->reserved)
//		{
//			pop();
//		}
//
//		return heap[0];
//	}
//};
//
//User users[MAX_N + 1];
//Pack packs[MAX_PACK];
//Area areas[MAX_AREA + 1];
//
//int cnt_areas[MAX_AREA + 1];
//int index_areas[MAX_AREA + 1];
//int indexes[MAX_AREA + 1];
//
//Pack* genPack(int pid)
//{
//	register int index = pid % MAX_PACK;
//	while (packs[index].pid)
//	{
//		index = (index + 1) % MAX_PACK;
//	}
//	return &packs[index];
//}
//
//Pack* getPack(int pid)
//{
//	register int index = pid % MAX_PACK;
//	while (packs[index].pid != pid)
//	{
//		index = (index + 1) % MAX_PACK;
//	}
//	return &packs[index];
//}
//
//void init(int N, int M)
//{
//	register int n, m;
//	register User* user;
//
//	nUser = N;
//	nAreas = M;
//
//	for (n = 1; n <= M; n++)
//	{
//		areas[n].hSize = areas[n].num_area = 0;
//	}
//
//	for (n = 1; n <= N; n++)
//	{
//		user = &users[n];
//
//		for (m = 1; m <= M; m++)
//		{
//			user->cnt_area[m] = 0;
//		}
//
//		user->nfriends = packs[n].pid = 0;
//	}
//
//	for (; n <= MAX_PACK; n++)
//	{
//		packs[n].pid = 0;
//	}
//}
//
//void befriend(int uid1, int uid2)
//{
//	register User* u1 = &users[uid1];
//	register User* u2 = &users[uid2];
//
//	u1->friends[u1->nfriends++] = u2;
//	u2->friends[u2->nfriends++] = u1;
//}
//
//void add(int pid, int area, int price)
//{
//	register Pack* pack = genPack(pid);
//	pack->pid = pid;
//	pack->area = area;
//	pack->reserved = false;
//	pack->d = price * MAX_PID + pid - 1;
//
//	areas[area].push(pack);
//}
//
//void reserve(int uid, int pid)
//{
//	register User* user = &users[uid];
//	register Pack* pack = getPack(pid);
//
//	pack->reserved = true;
//	user->cnt_area[pack->area]++;
//	areas[pack->area].num_area--;
//}
//
//
//
//int recommend(int uid)
//{
//	register User* user = &users[uid];
//	register Pack* pack;
//	register Area* area;
//	register LL d = INF;
//	register int n, m, k;
//
//	for (n = 1; n <= nAreas; n++)
//	{
//		cnt_areas[n] = user->cnt_area[n];
//		index_areas[n] = n;
//	}
//
//	for (m = 0; m < user->nfriends; m++)
//	{
//		register User* f = user->friends[m];
//		for (n = 1; n <= nAreas; n++)
//		{
//			cnt_areas[n] += f->cnt_area[n];
//		}
//	}
//
//	indexes[1] = 1;
//	for (n = 2; n <= nAreas; n++)
//	{
//		for (m = 1; m < n; m++)
//		{
//			if (cnt_areas[n] > cnt_areas[indexes[m]])
//			{
//				for (k = n; k > m; k--)
//				{
//					indexes[k] = indexes[k - 1];
//				}
//
//				indexes[m] = n;
//				break;
//			}
//		}
//
//		if (m == n)
//		{
//			indexes[n] = n;
//		}
//	}
//
//	int size = areas[indexes[1]].num_area;
//	int start = 1;
//	for (n = 2; n <= nAreas; n++)
//	{
//		if (cnt_areas[indexes[n]] == cnt_areas[indexes[n - 1]])
//		{
//			size += areas[indexes[n]].num_area;
//		}
//		else
//		{
//			if (size)
//			{
//				break;
//			}
//			else
//			{
//				start = n;
//				size = areas[indexes[n]].num_area;
//			}
//		}
//	}
//
//
//	k = -1;
//
//	for (m = start; m < n; m++)
//	{
//		area = &areas[indexes[m]];
//
//		if (area->num_area)
//		{
//			pack = area->top();
//
//			if (pack->d < d)
//			{
//				d = pack->d;
//				k = pack->pid;
//			}
//		}
//	}
//
//	return k;
//}