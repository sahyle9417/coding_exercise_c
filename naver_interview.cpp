////배열에서 0을 모두 오른쪽으로 몰고싶습니다.
////0을 제외한 나머지 수의 순서는 유지되어야 합니다.
////ex. 1 5 0 1 0 3 -> 1 5 1 3 0 0
////단, 새로운 배열을 선언해서는 안됩니다.
//
//
//#include <stdio.h>
//#define LEN 11
//
//int main() {
//	int arr[LEN] = {0, 1, 5, 0, 1, 0, 3, 0, 0, 1, 2};
//
//
//	int endOfNonZero = 0;
//	int i = 0;
//	while (i < LEN && endOfNonZero < LEN) {
//		if (arr[i] != 0) {
//			arr[endOfNonZero] = arr[i];
//			endOfNonZero++;
//		}
//		i++;
//	}
//	while (endOfNonZero < LEN) {
//		arr[endOfNonZero++] = 0;
//	}
//	
//	for (int i = 0; i < LEN; i++) {
//		printf("%d ", arr[i]);
//	}
//}