//#include <stdio.h>
//#define MAX 100
//
//int map[MAX + 2][MAX + 2];		// 4로 맵 가장자리 둘러쌈, 그래야 인덱스 검사 4번을 값 검사 1번으로 대체 가능
//bool visit[MAX + 2][MAX + 2];
//int qx[MAX*MAX];
//int qy[MAX*MAX];
//
//int dx[] = {0, 0, -1, 1};
//int dy[] = {1, -1, 0, 0};
//
//int main() {
//	freopen("input.txt", "r", stdin);
//	for (int tc = 1; tc <= 10; tc++) {
//		int t;
//		scanf("%d ", &tc);
//
//		for (int i = 0; i <= MAX+1; i++) {
//			map[i][0] = 4;
//			map[i][MAX + 1] = 4;
//			map[0][i] = 4;
//			map[MAX + 1][i] = 4;
//		}
//
//		int src_x = 2;
//		int src_y = 2;
//		int dst_x = MAX;
//		int dst_y = MAX;
//
//		char str[MAX + 1];
//		for (int y = 1; y <= MAX; y++) {
//			scanf("%s", str);
//			for (int x = 1; x <= MAX; x++) {
//				int tmp = str[x - 1] - '0';
//				map[y][x] = tmp;
//				visit[y][x] = false;
//				if (tmp == 3) {
//					dst_x = x;
//					dst_y = y;
//				}
//			}
//		}
//
//		bool answer = false;
//
//		// front가 가리키는 영역은 절대로 값이 들어올 수 없다.
//		int front = 0;
//		int rear = 0;
//		rear = (rear + 1) % MAX;
//		qx[rear] = src_x;
//		qy[rear] = src_y;
//
//		map[src_x][src_y] = true;
//
//		while (front != rear) {
//			front = (front + 1) % MAX;
//			int x = qx[front];
//			int y = qy[front];
//			for (int d = 0; d < 4; d++) {
//				int next_x = x + dx[d];
//				int next_y = y + dy[d];
//
//				if (visit[next_x][next_y]) continue;	// 방문여부 검사
//				if (map[next_x][next_y] == 4) continue;	// 바운드 검사
//				if (map[next_x][next_y] == 3) {			// 끝났는지 검사
//					answer = true;
//					break;
//				}
//				if (map[next_x][next_y] == 0) {
//					map[next_x][next_y] = 2;
//					visit[next_x][next_y] = true;
//					rear = (rear + 1) % MAX;
//					qx[rear] = next_x;
//					qy[rear] = next_y;
//				}
//			}
//			if (answer) break;
//		}
//		if (answer) printf("#%d 1\n", tc);
//		else printf("#%d 0\n", tc);
//	}
//}