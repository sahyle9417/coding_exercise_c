//// https://huiyu.tistory.com/entry/DP-%EC%97%B0%EC%87%84%ED%96%89%EB%A0%AC-%EC%B5%9C%EC%86%8C%EA%B3%B1%EC%85%88-%EC%95%8C%EA%B3%A0%EB%A6%AC%EC%A6%98
//
//#include<iostream>
//#include<algorithm>
//#include<memory.h>
//
//#define MAX_N 102
//
//using namespace std;
//
//int mtx[MAX_N][2];
//int N, cnt;
//int board[MAX_N][MAX_N];
//bool used[MAX_N];
//int save[MAX_N];
//int dp[MAX_N][MAX_N];
//
//void print() {
//	printf("===========save===========\n");
//	for (int i = 0; i < N; i++) {
//		printf("%d ", save[i]);
//	}
//	printf("\n===========dp===========\n");
//	for (int i = 0; i < N; i++) {
//		for (int j = 0; j < N; j++) {
//			printf("%8d ", dp[i][j]);
//		}
//		printf("\n");
//	}
//}
//
//bool sortMatrix(int idx, int cnt) {
//	if (cnt == N)
//		return true;
//	used[idx] = true;
//	for (int i = 0; i < N; i++) {
//		if (!used[i] && mtx[idx][1] == mtx[i][0]) {
//			if (sortMatrix(i, cnt + 1)) {
//				save[cnt] = i;
//				return true;
//			}
//		}
//	}
//	used[idx] = false;
//	return false;
//}
//
//// 행렬의 크기(행과 열의 길이)를 mtx배열에 기록 ([행렬번호][행(0)or열(1)])
//void func(int y, int x)
//{
//	int ey = y, ex = x;
//
//	while (board[ey][ex + 1])
//		ex++;
//	while (board[ey + 1][ex])
//		ey++;
//
//	for (int i = y; i <= ey; i++)
//		for (int j = x; j <= ex; j++)
//			board[i][j] = 0;
//	mtx[cnt][0] = ey - y + 1;
//	mtx[cnt++][1] = ex - x + 1;
//}
//
//// l번째 행렬부터 r번째 행렬까지 곱할 때 발생하는 원소 곱셈 횟수 반환
//int mcm(int l, int r) {
//	if (l == r) { return 0; }
//	int &ret = dp[l][r];
//	if (ret) { return ret; }
//	ret = 99999999;
//	for (int i = l; i <= r; i++)
//		ret = min(ret, mcm(l, i) + mcm(i + 1, r) + mtx[save[l]][0] * mtx[save[i]][1] * mtx[save[r]][1]);
//	return ret;
//}
//
//int main() {
//	freopen("sample_input.txt", "r", stdin);
//	int T;
//	scanf("%d", &T);
//	for (int t = 1; t <= T; t++) {
//		memset(dp, 0, sizeof(dp));
//		memset(used, 0, sizeof(used));
//		cnt = 0;
//		scanf("%d", &N);
//		for (int i = 0; i < N; i++)
//			for (int j = 0; j < N; j++)
//				scanf("%d", &board[i][j]);
//		// board에서 행렬 크기 구하기
//		for (int i = 0; i < N; i++)
//			for (int j = 0; j < N; j++)
//				if (board[i][j])
//					func(i, j);
//		N = cnt;
//		// 행렬 순서 맞추기
//		for (int i = 0; i < N; i++) {
//			mtx[N][1] = mtx[i][0];
//			if (sortMatrix(N, 0))
//				break;
//		}
//		// 0번째 행렬부터 N-1번째 행렬까지의 최소 원소곱셈 횟수
//		printf("#%d %d\n", t, mcm(0, N - 1));
//		//print();
//	}
//	return 0;
//}
//
