//// 디렉토리 이름 최대 길이는 6
//#define NAME_MAXLEN 6
//// 디렉토리 경로 이름의 최대 길이는 1999 (깊이 아님)
//#define PATH_MAXLEN 1999
//// 각 TC마다 생성되는 최대 디렉토리 개수는 5만
//#define MAX_DIR_NUM 50005
//
//struct DIR {
//	int name;
//	DIR* parent;	// 부모
//	DIR* child;		// 자식
//	DIR* prev;		// 형
//	DIR* next;		// 동생
//};
//
//DIR dirPool[MAX_DIR_NUM];
//DIR dummy = { 0, };			// 이걸로 초기화하면 memset과 동일한 효과
//
//// DIR 배열에서 끌어다쓴 디렉토리 개수
//int dirPoolIdx = 0;
//
//void init(int n) {	// n 무시하면 됨, 어차피 이전 TC에서 사용한 부분만 dummy로 초기화하면 되기 때문
//	for (int i = 0; i < dirPoolIdx; i++)
//		dirPool[i] = dummy;
//	// 0번 자리는 루트 디렉토리
//	dirPoolIdx = 1;
//}
//
//DIR* getPath(char path[PATH_MAXLEN + 1]) {
//
//	// 0번째 글자는 무조건 '/'니까 제끼고 1번째 글자부터 읽기
//	int pos = 1;
//
//	// 루트 디렉토리부터 탐색
//	DIR* dir = &dirPool[0];
//
//	// path 문자열이 끝날때까지 반복
//	while (path[pos]) {
//
//		// 하나의 디렉토리명을 int형 변수 name으로 치환
//		// MAX_LEN이 6이므로, 최대값은 2^5^6 = 1073741824 이므로 int로 표현 가능
//		int nameInt = 0;
//		while (path[pos] != '/') {
//			nameInt = (nameInt << 5) + path[pos] - 'a' + 1;
//			pos++;
//		}
//
//		// 현재 디렉토리명과 일치하는 자식 검색
//		dir = dir->child;
//		while (dir) {
//			if (dir->name == nameInt) break;
//			dir = dir->next;
//		}
//
//		// 다음 디렉토리명 검색을 위해 '/' 건너뛰기
//		pos++;
//	}
//	return dir;
//}
//
//// newChild를 parent의 첫번째 자식으로 추가
//void connect(DIR* parent, DIR* newChild) {
//	// newChild의 부모는 parent
//	newChild->parent = parent;
//	// newChild는 parent의 첫번째 자식이므로 형이 존재하지 않음
//	newChild->prev = 0;
//	// parent의 기존 첫번째 자식을 newChild의 next로 지정 (동생으로 처리)
//	newChild->next = parent->child;
//	// parent의 기존 첫번째 자식의 형은 newChild
//	if (parent->child)   parent->child->prev = newChild;
//	// parent의 새로운 첫번째 자식은 newChild
//	parent->child = newChild;
//}
//
//void disconnect(DIR* delDir) {
//	// 형이 존재하면 동생을 형의 직속 동생으로 지정
//	if (delDir->prev)
//		delDir->prev->next = delDir->next;
//	// 형이 존재하지 않으면 동생을 부모의 첫번째 자식으로 지정
//	else
//		delDir->parent->child = delDir->next;
//	// 동생이 존재하면 동생의 직속 형을 내 직속형으로 지정
//	if (delDir->next) delDir->next->prev = delDir->prev;
//}
//
//void cmd_mkdir(char path[PATH_MAXLEN + 1], char name[NAME_MAXLEN + 1]) {
//	// 목표 지점 경로 획득
//	DIR* parent = getPath(path);
//
//	int pos = 0;
//	int nameInt = 0;
//	while (name[pos]) {
//		nameInt = (nameInt << 5) + name[pos] - 'a' + 1;	// 알파벳 1글자당 5자리 차지 (32진법)
//		pos++;											// 0은 무조건 루트 디렉토리니까 a는 1로 변환
//	}
//	DIR* newChild = &dirPool[dirPoolIdx++];
//	newChild->name = nameInt;
//	connect(parent, newChild);
//}
//
//void cmd_rm(char path[PATH_MAXLEN + 1]) {
//	DIR* dir = getPath(path);
//	disconnect(dir);
//}
//
//// src 이하 디렉토리 구조를 그대로 가져다가 dst에 생성(복붙)
//void copyDir(DIR* src, DIR* dst) {
//
//	// 복사해야하니 새로운 디렉토리 할당받고 이름 복붙
//	DIR* newDir = &dirPool[dirPoolIdx++];
//	newDir->name = src->name;
//
//	// 새로 만든 디렉토리를 dst의 자식으로 추가
//	connect(dst, newDir);
//
//	// src의 모든 자식들을 순회하며 newDir의 자식으로 복사
//	// 재귀호출되었으므로 자식의 자식들도 모두 복제됨
//	DIR* child = src->child;
//	while (child) {
//		copyDir(child, newDir);
//		child = child->next;
//	}
//}
//
//void cmd_cp(char srcPath[PATH_MAXLEN + 1], char dstPath[PATH_MAXLEN + 1]) {
//	DIR* src = getPath(srcPath);
//	DIR* dst = getPath(dstPath);
//	copyDir(src, dst);
//}
//
//void cmd_mv(char srcPath[PATH_MAXLEN + 1], char dstPath[PATH_MAXLEN + 1]) {
//	DIR* src = getPath(srcPath);
//	DIR* dst = getPath(dstPath);
//	disconnect(src);
//	connect(dst, src);
//}
//
//int find(DIR* parent) {
//	int count = 0;
//	// while문 돌며 모든 자식을 순회하고 각 자식마다 자식 수 찾는 함수(find) 재귀호출
//	DIR* child = parent->child;
//	while (child) {
//		// 자기자신도 입력된 디렉토리의 하위 디렉토리니까 1도 추가로 더해줌
//		count += find(child) + 1;
//		child = child->next;
//	}
//	return count;
//}
//
//int cmd_find(char path[PATH_MAXLEN + 1]) {
//	DIR* dir = getPath(path);
//	return find(dir);
//}