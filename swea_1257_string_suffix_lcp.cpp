////https://blog.naver.com/PostView.nhn?blogId=1ilsang&logNo=221475165220&categoryNo=0&parentCategoryNo=0&viewDate=&currentPage=1&postListTopCurrentPage=1&from=postView
//// trie로도 풀수 있는 문제, 시간 나면 해보기?
//
//#include<stdio.h>
//#include <time.h>
//#define MAX_CHAR 401
//
//char str[MAX_CHAR];
//int k;
//int strLen;
//int suffix[MAX_CHAR];
//int lcp[MAX_CHAR];
//int cnt[MAX_CHAR];
//
//int my_strcmp(char* s1, char* s2) {
//	int i = 0;
//	while (s1[i] != 0) {
//		if (s1[i] != s2[i])
//			break;
//		i++;
//	}
//	return (s1[i] - s2[i]);
//}
//
//// 두 문자열에서 앞쪽 몇글자까지 동일한지 반환
//int getLCP(char* s1, char *s2) {
//	int len = 0;
//	while (*s1++ == *s2++) {
//		len++;
//	}
//	return len;
//}
//
//void quickSort(int s, int e) {
//	if (s < e) {
//		int pivot = s;
//		int left = s;
//		int right = e;
//		while (left < right) {
//			while (my_strcmp(str + suffix[left], str + suffix[pivot]) <= 0 && left < e)
//				left++;
//			while (my_strcmp(str + suffix[right], str + suffix[pivot]) > 0)
//				right--;
//
//			if (left < right) {
//				int t = suffix[left];
//				suffix[left] = suffix[right];
//				suffix[right] = t;
//			}
//		}
//
//		int t = suffix[right];
//		suffix[right] = suffix[pivot];
//		suffix[pivot] = t;
//
//		quickSort(s, right - 1);
//		quickSort(right + 1, e);
//	}
//}
//
//void solve(int testCase) {
//	int temp;
//	int sum = 0;
//	scanf("%d", &k);
//	// tfbpqyuekmsonzgdlvjhcawxr
//	scanf("%s", str);
//	strLen = 0;
//	while (str[strLen]) { strLen++; }
//	// 접미사 배열(suffix)의 길이는 원본 문자열의 길이와 같다.
//	// 각각의 글자가 모두 시작 인덱스로 한번씩 지정되기 때문
//	// suffix 배열에는 시작 인덱스 번호만 저장한다
//	// tfbpq... -> tfbpq...(0), fbpq...(1), bpq...(2), pq...(3)
//	for (int i = 0; i < strLen; i++) { suffix[i] = i; }
//	// 접미사 배열(suffix)을 사전순으로 정렬
//	quickSort(0, strLen - 1);
//	// [0] awxr, [1]bpqyuekmsonzgdlvjhcawxr, [2] cawxr, [3] dlvjhcawxr, [4] ekmsonzgdlvjhcawxr, ...
//
//	// getLCP : 두 문자열에서 앞쪽 몇글자까지 동일한지 반환
//	// LCP (최장 공통 부분 문자열) : 중복된 부분 문자열의 개수를 기록한다
//	// LCP로 중첩되는 부분 문자열만큼 건너뛰어서 중복 제거된 K번째 부분 문자열 찾는다
//	// TC 7에서 str+suffix[0]:acfcyheaglzgun, str+suffix[1]:aglzgun -> lcp[1] = 1
//	for (int i = 1; i < strLen; i++) {
//		lcp[i] = getLCP(str + suffix[i], str + suffix[i - 1]);
//	}
//	// cnt에는 중복을 제거한 부분 문자열의 개수가 저장된다.
//	cnt[0] = strLen - suffix[0];
//	for (int i = 1; i < strLen; i++) {
//		cnt[i] = strLen - suffix[i] - lcp[i] + cnt[i - 1];
//	}
//
//	if (k > cnt[strLen - 1]) {
//		printf("#%d none\n", testCase);
//	}
//	else {
//		int i;
//		for (i = 0; i < strLen; i++) {
//			if (k <= cnt[i]) { break; }
//		}
//		int start = suffix[i];
//		int end = -1;
//		if (i == 0) {
//			end = suffix[i] + k;
//		}
//		else {
//			end = suffix[i] + k - cnt[i - 1] + lcp[i];
//		}
//		str[end] = 0;
//
//		/*puts("\nsuffix"); 
//		for (int i = 0; i < strLen; i++) {
//			printf("%d ", suffix[i]);
//		}
//		puts("\nlcp");
//		for (int i = 0; i < strLen; i++) {
//			printf("%d ", lcp[i]);
//		}
//		puts("\ncnt");
//		for (int i = 0; i < strLen; i++) {
//			printf("%d ", cnt[i]);
//		}
//		puts("\n=========================\n");*/
//
//		// 한글자씩 출력하는 것(%c)보다 한번에 string으로 출력(%s)하는게 10배이상 빠르다
//		printf("#%d ", testCase); puts(str + start);
//		/*printf("#%d ", testCase);
//		for (i = start; i < end; i++) {
//			printf("%c", str[i]);
//		}
//		puts("");*/
//	}
//}
//
//int main() {
//	freopen("input.txt", "r", stdin);
//	int tc;
//	scanf("%d", &tc);
//
//	for (int testCase = 1; testCase <= tc; testCase++) {
//		solve(testCase);
//	}
//}
