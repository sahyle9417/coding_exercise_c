//#include <malloc.h>
//#include <stdio.h>
//#define MAX_N 1000
//#define MAX_M 20
//#define MAX_STR 20000	// 별자리 지도 전체에 포함될 수 있는 최대 별 개수
//#define MAX_CON 20		// 하나의 별자리에 포함될 수 있는 최대 별 개수
//
//int n, m;
//int** map;
//
//struct loc {
//	int y, x;
//};
//
//loc str_loc[MAX_STR];
//int str_num;
//
//// 별자리 모양 기록
//// 첫번째 인덱스는 회전방향 (0도, 90도, 180도, 270도)
//// 두번째 인덱스는 별 번호, 기준별은 항상 1번 인덱스에 위치
//loc con_loc[4][MAX_CON];
//int con_num;
//
//struct Result {
//	int y, x;
//};
//
//// 좌표 90도 회전
//loc rotate(loc input) {
//	loc output;
//	output.y = input.x;
//	output.x = m - 1 - input.y;
//	return output;
//}
//
//void print() {
//	printf("======================\n");
//	for (int i = 0; i < n; i++) {
//		//printf("i:%d\n", i);
//		for (int j = 0; j < n; j++) {
//			printf("%d ", map[i][j]);
//			//printf("i:%d, j:%d, n:%d\n, map[i][j]:%d", i, j, n, map[i][j]);
//		}
//		puts("");
//	}
//	printf("======================\n");
//}
//
//void init(int N, int M, int Map[MAX_N][MAX_N])
//{
//	
//	n = N;
//	m = M; 
//	
//	/*printf("m:%d\n", m);
//	loc input = { 0, 1 };
//	for (int i = 0; i < 4; i++) {
//		printf("%d %d -> ", input.y, input.x);
//		input = rotate(input);
//	}
//	puts("");*/
//	str_num = 0;
//	map = (int**)malloc(sizeof(int*)*n);
//	for (int y = 0; y < n; y++) {
//		map[y] = (int*)malloc(sizeof(int)*n);
//		for (int x = 0; x < n; x++) {
//			map[y][x] = Map[y][x];
//			if (map[y][x]) {
//				str_loc[str_num++] = { y, x };
//			}
//		}
//	}
//}
//
//Result findConstellation(int stars[MAX_M][MAX_M])
//{
//	con_num = 1;
//
//	for (int y = 0; y < m; y++) {
//		for (int x = 0; x < m; x++) {
//			if (stars[y][x] == 9) {
//				con_loc[0][0] = { y, x };
//			}
//			else if (stars[y][x] == 1) {
//				con_loc[0][con_num++] = { y, x };
//			}
//		}
//	}
//
//	for (int dir = 1; dir <= 3; dir++) {
//		for (int sn = 0; sn < con_num; sn++) {
//			con_loc[dir][sn] = rotate(con_loc[dir - 1][sn]);
//		}
//	}
//
//	/*printf("M:%d\n", m);
//	for (int sn = 0; sn < star_num; sn++) {
//		for (int dir = 0; dir <= 3; dir++) {
//			printf("%d %d -> ", star_loc[dir][sn].y, star_loc[dir][sn].x);
//		}
//		puts("");
//	}*/
//
//	for (int sn = 0; sn < str_num; sn++) {
//		int y = str_loc[sn].y;
//		int x = str_loc[sn].x;
//		for (int dir = 0; dir < 4; dir++) {
//			// bound 검사
//			int center_y = con_loc[dir][0].y;
//			int center_x = con_loc[dir][0].x;
//			if (y - center_y < 0 || x - center_x < 0 || y + (m - 1 - center_y) >= n || x + (m - 1 - center_x) >= n) {
//				continue;
//			}
//
//			bool match = true;
//			for (int sn = 1; sn < con_num; sn++) {
//				int star_y = con_loc[dir][sn].y;
//				int star_x = con_loc[dir][sn].x;
//				if (!map[y - center_y + star_y][x - center_x + star_x]) {
//					match = false;
//					break;
//				}
//			}
//			if (match) {
//				for (int sn = 0; sn < con_num; sn++) {
//					int star_y = con_loc[dir][sn].y;
//					int star_x = con_loc[dir][sn].x;
//					map[y - center_y + star_y][x - center_x + star_x] = 0;
//				}
//				Result res;
//				res.y = y;
//				res.x = x;
//				return res;
//			}
//		}
//	}
//}