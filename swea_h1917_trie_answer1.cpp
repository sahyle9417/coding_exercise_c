﻿//#define MAX_USER 1000
//#define MAX_TAG 5000
//#define MAX_MSG 500000
//#define MAX_MSG_PER_USER 100
//#define MAX_TRIE 50001
//#define CHILD_NUM 26
//
//
//typedef struct _msg {	// msg의 실제 내용을 담고 있는 구조체
//	int id;
//	int priority;
//} MSG;
//
//
//typedef struct _user {
//	int msgNum;
//	MSG msgList[MAX_MSG_PER_USER];
//	int followNum;
//	_user* followList[MAX_USER];
//} USER;
//
//
//typedef struct _msg_node {	// 같은 tag를 가지는 msg들의 리스트를 구현하기 위한 노드
//	MSG* msg;				// id, priority
//	_msg_node* next;
//} MSG_NODE;
//
//typedef struct _trie_node {
//	_trie_node* child[CHILD_NUM];	// 알파벳 소문자는 26종류
//	MSG_NODE msgListHead;			// 해당 trie node에 해당하는 tag 갖는 메시지 리스트의 맨앞 (우선순위대로 연결되어있음)
//} TRIE_NODE;						// msgListHead를 포인터가 아닌 실제 값으로 가지면 공간 할당 안해도 되는 장점이 있다. (매우 중요!!!!)
//									// 어차피 첫 노드는 dummy node니까 값을 넣지 않을 것임
//
//USER userPool[MAX_USER];
//
//MSG_NODE msgNodePool[MAX_MSG];
//int msgNodeIdx;
//
//TRIE_NODE trieNodePool[MAX_TRIE];
//int trieNodeIdx;
//
//
//// HH:MM:SS를 초단위 숫자로 변환
//int parseTime(char* data) {
//	int time = (data[0] - '0') * 36000;	// 시 10자리
//	time += (data[1] - '0') * 3600;		// 시  1자리
//	time += (data[3] - '0') * 600;		// 분 10자리
//	time += (data[4] - '0') * 60;		// 분  1자리
//	time += (data[6] - '0') * 10;		// 초 10자리
//	time += (data[7] - '0');			// 초  1자리
//	return time;
//}
//
//
//// 문자열의 주소 입력받아서 해당 문자열에 해당하는 trie 노드 반환
//TRIE_NODE* getTrieNode(char** TagStr) {
//
//	// trie의 최상단 root의 주소는 0번 인덱스
//	TRIE_NODE* trieNode = &trieNodePool[0];
//
//	// 문자열에서 한글자씩 보면서 문자열의 끝까지 순회 (공백 또는 널 만나면 stop)
//	// 문자열의 주소니까 한글자(char)만 보려면 *가 2개 있어야 함 (문자열주소->문자열->문자)
//	while (**TagStr >= 'a') {	// == 'while (**TagStr != ' ' && **TagStr != 0) {'
//
//		// 알파벳을 인덱스로 이용, a=0, b=1, ...
//		int childIdx = **TagStr - 'a';
//
//		// 해당 위치에 trie 노드가 생성된적이 없었다면 trie nodes배열에서 한칸 떼다가 새로 생성
//		if (!trieNode->child[childIdx])
//			trieNode->child[childIdx] = &trieNodePool[trieNodeIdx++];
//
//		// 다음 글자로 넘어감
//		trieNode = trieNode->child[childIdx];
//		(*TagStr)++;
//	}
//	return trieNode;
//}
//
//
//// 입력받은 msg를 입력받은 TagStr에 해당하는 trie 노드의 리스트 내 우선순위에 맞는 위치에 추가
//// 문자열 매개변수를 넘겨 받기 위해 char 이중 포인터(문자열의 주솟값) 사용
//void addMsgToTag(MSG* newMsg, char** TagStr) {
//
//	// 새로운 msg 노드 공간 할당 후 msg 노드에 입력받은 msg 기록
//	MSG_NODE* newMsgNode = &msgNodePool[msgNodeIdx++];
//	newMsgNode->msg = newMsg;
//
//	// trie 노드 획득
//	TRIE_NODE* trieNode = getTrieNode(TagStr);
//
//	// 현재 msg를 해당 msg의 우선순위에 맞는 위치(prev와 next 사이)에 추가
//	// 내 priority(msg->priority)가 next의 priority보다 작으면 prev와 next 모두 한칸뒤(next)로 이동
//	MSG_NODE* prev = &trieNode->msgListHead;
//	MSG_NODE* next = trieNode->msgListHead.next;
//	while (next && next->msg->priority > newMsg->priority) {
//		prev = next;
//		next = next->next;
//	}
//	newMsgNode->next = next;
//	prev->next = newMsgNode;
//}
//
//
//// 입력받은 msg를 입력받은 TagStr에 해당하는 trie 노드의 리스트에 추가
//void addMsgToAllTags(MSG* msg, char* AllTagsStr) {
//	char* tagStr = AllTagsStr;
//	while (*tagStr) {				// * 있음에 유의, 널포인터 여부가 아니라 내용이 널값일때까지 반복
//		tagStr += 2;				// 맨앞에 띄어쓰기와 #기호 제끼기
//		addMsgToTag(msg, &tagStr);	// msg를 입력받은 tagStr에 해당하는 trie 노드의 리스트 내 우선순위에 맞는 위치에 추가
//	}								// tagStr(문자열)을 addMsgToTag 함수 내에서 수정해야하므로 주소값(call by reference로) 넘기기
//}
//
//void init() {
//	// 모든 사용자에 대해 메시지, 팔로우 초기화
//	for (int i = 0; i < MAX_USER; i++) {
//		userPool[i].msgNum = 0;		// 메세지 0개
//		userPool[i].followNum = 1;	// 팔로우 1명(자기자신)
//		userPool[i].followList[0] = &userPool[i];
//	}
//	// 이전 TC에서 사용한 trie_node들 초기화
//	for (int i = 0; i < trieNodeIdx; i++) {
//		trieNodePool[i].msgListHead.next = 0;
//
//		for (int j = 0; j < CHILD_NUM; j++)
//			trieNodePool[i].child[j] = 0;
//	}
//	msgNodeIdx = 0;
//	trieNodeIdx = 1;
//}
//
//
//void createMessage(int msgID, int userID, char msgData[]) {
//
//	// userID 구조체 획득 (이미 할당되어있었을 수도)
//	USER* user = &userPool[userID];
//
//	// 해당 user 구조체 내 msg_list 배열(해당 user 소유 msg 저장)에서 공간과 id 할당
//	MSG* newMsg = &user->msgList[user->msgNum++];
//	newMsg->id = msgID;
//
//	// 메시지의 우선순위는 1순위:시간높은것, 2순위:사용자ID낮은것
//	// 사용자ID는 1,000까지니까 초단위 시간을 1024 곱하고(<<10) 사용자ID를 빼면 시간 먼저 보고 사용자ID보게되는 것임
//	newMsg->priority = (parseTime(msgData) << 10) - userID;
//
//	// msg 객체와 tag 정보를 넘기면, tag에 해당하는 trie 노드의 리스트에서 우선순위에 맞게 msg를 위치시킴
//	// msgData는 'HH:MM:SS #tag1 #tag2 ...'의 형태를 띄므로 시간정보 8칸 제끼고 그 뒤로는 2칸씩 건너뛰며 tag 획득
//	addMsgToAllTags(newMsg, msgData + 8);
//}
//
//
//void followUser(int userID1, int userID2) {
//	// 해당 userID1 구조체 내 follow_count 값 1 증가
//	// 해당 userID1 구조체 내 follow_list 배열에 userID2 구조체를 추가
//	USER* user = &userPool[userID1];
//	user->followList[user->followNum++] = &userPool[userID2];
//}
//
//
//int searchByHashtag(char tagName[], int retIDs[]) {
//	char* _tagName = tagName + 1;					// 맨앞 #기호 제끼기
//	TRIE_NODE* trieNode = getTrieNode(&_tagName);	// trie 노드 획득
//	MSG_NODE* msgNode = trieNode->msgListHead.next;	// 해당 trie 노드에 달려있는 첫번째 msg 노드
//
//	int count = 0;
//	while (msgNode && count < 5) {
//		retIDs[count++] = msgNode->msg->id;
//		msgNode = msgNode->next;
//	}
//
//	return count;
//}
//
//// userID 사용자와 userID 사용자가 팔로우하는 사용자들의 메시지들 중 우선순위 상위 5개 출력
//int getMessages(int userID, int retIDs[]) {
//	
//	USER* user = &userPool[userID];
//
//	// 아직 ret에 추가되지 않은 msg들만 보기 위해 lastAddedMsgPriority보다 우선순위가 낮은 msg만 볼 것임
//	// 초기값 = 2^29-1 (23:59:59를 초기준 변환(86,399) 후 1,024 곱하고 1000 더한 값(88,473,576‬)보다 크면 됨)
//	int lastAddedMsgPriority = 0xFFFFFFF;
//
//	int retNum = 0;
//	while (retNum < 5) {
//
//		// 아직 ret에 추가하지 않은 msg 중 가장 우선순위 높은 msg를 msgRet에 기록할 것임
//		// 아직 ret에 추가되지 않은 msg들만 보기 위해 lastAddedMsgPriority보다 우선순위가 낮은 msg만 볼 것임
//		// 즉, lastAddedMsgPriority보다 낮은 우선순위 가지는 msg들 중 가장 큰 우선순위 가지는 msg를 retIDs배열에 추가할 것
//		// msg를 추가한 뒤에는 lastAddedMsgPriority를 새로 추가한 msg의 우선순위 값으로 갱신
//		MSG* msgRet = 0;
//
//		// 사용자 한명씩 선택
//		for (int i = 0; i < user->followNum; i++) {
//			USER* follower = user->followList[i];
//
//			MSG* msg = 0;
//
//			// 선택된 사용자의 메시지들 중 나중에 생성된 것(우선순위 높은 것)부터 하나씩 선택
//			for (int j = follower->msgNum - 1; j >= 0; j--) {
//				msg = &follower->msgList[j];
//
//				// 기준 우선순위(priorityThreshold)보다 우선순위 작은 msg 처음으로 발견, ret에 추가할지 여부는 다른 사용자들의 msg까지 다 보고 결정
//				// 기준 우선순위(priorityThreshold)보다 우선순위 큰 msg는 이미 ret에 추가되어있으므로 스킵
//				if (msg->priority < lastAddedMsgPriority)
//					break;
//			}
//
//			// 아예 msg가 하나도 없거나
//			// 이미 ret에 추가된(lastAddedMsgPriority보다 우선순위가 높은) msg들 뿐이라면 해당 userID 스킵
//			if (!msg || msg->priority >= lastAddedMsgPriority) 
//				continue;
//
//			// priorityThreshold보다 우선순위가 작은 msg들 중에서 우선순위 가장 높은 것을 msgRet에 기록
//			// msgRet가 비어있다면 해당 msg가 처음으로 발견된 priorityThreshold보다 우선순위가 작은 msg니까 msgRet 갱신
//			// msgRet가 비어있지 않다면 기존에 기록된 msgRet와 이번에 발견한 msg와 우선순위 비교 후 더 우선순위 높은 것을 msgRet로 설정
//			if (!msgRet || msgRet->priority < msg->priority)
//				msgRet = msg;
//		}
//
//		// 모든 사용자들의 모든 msg 다 훑어도 기준 우선순위(lastAddedMsgPriority) 이하의 msg 존재X
//		// 탐색 대상 msg 개수가 5개보다 작아서 ret 배열이 가득 차지 않았는데(<5) 더 이상 추가할 msg가 존재X
//		if (!msgRet)
//			return retNum;
//		
//		// lastAddedMsgPriority보다 우선순위가 작은 msg들 중에서 우선순위 가장 높은 msg(msgRet)를 ret 배열에 추가
//		retIDs[retNum++] = msgRet->id;
//
//		// lastAddedMsgPriority를 이번에 새롭게 ret에 추가된 msg(msgRet)의 우선순위 값으로 갱신
//		// lastAddedMsgPriority보다 우선순위가 낮은 msg(아직 ret에 추가되지 않은 msg)만 탐색하기 위함임
//		lastAddedMsgPriority = msgRet->priority;
//	}
//
//	return retNum;
//}