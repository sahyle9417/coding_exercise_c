//#include <stdio.h>
//
//#define UNKNOWN     -1
//#define MISS        0
////#define CARRIER     1
////#define BATTLESHIP  2
////#define CRUISER     3
////#define SUBMARINE   4
////#define DESTROYER   5
//#define MAP_SIZE	10
//#define MAX_HIT		17
//#define TYPE_NUM	6
//
//extern int fire(int r, int c);
//
//int prob[MAP_SIZE][MAP_SIZE];
//int map[MAP_SIZE][MAP_SIZE];
//int len[TYPE_NUM] = { 0, 5, 4, 3, 3, 2 };
//int found[TYPE_NUM];
//
//int dr[4] = { 1, 0, -1, 0 };
//int dc[4] = { 0, 1, 0, -1 };
//
//void init(int limit) {
//
//}
//
//// 주어진 방향으로 주어진 type의 길이만큼 나아가면서 각 칸의 확률값 업데이트
//void updateProb(int startR, int startC, int type, int dir) {
//	int r = startR, c = startC;
//	int cnt = 0;
//
//	// 주어진 방향으로 주어진 type의 길이만큼 나아가면서 칸의 일부가 밝혀진 type이 존재하는지 검사
//	for (int i = 0; i < len[type]; i++, r += dr[dir], c += dc[dir]) {
//
//		// 바운드 검사
//		if (r >= MAP_SIZE || r < 0 || c >= MAP_SIZE || c < 0) return;
//		
//		// 주어진 type이 아닌 것으로 확정된 칸은 확률값 업데이트하지 않음
//		if (map[r][c] != UNKNOWN && map[r][c] != type) return;
//
//		// 주어진 type이 맞으며 칸의 일부가 밝혀져 있고 나머지 일부는 밝혀지지 않은 상태
//		if (map[r][c] == type)
//			cnt++;
//	}
//
//	// 확률값 변화량을 주어진 type의 길이로 지정
//	// 주어진 type에 대해 칸의 일부가 밝혀진 경우, 해당 type이 위치할 수 있는 칸의 확률값 변화량을 큰값으로 설정(type길이 * 100)
//	int probDiff = len[type];
//	if (cnt > 0 && found[type] == cnt) {
//		probDiff *= 1000;
//	}
//
//	// 위에서 계산한 확률값 변화량을 토대로 각 칸의 확률값을 실제로 업데이트
//	r = startR; c = startC;
//	for (int i = 0; i < len[type]; i++, r += dr[dir], c += dc[dir]) {
//		prob[r][c] += probDiff;
//	}
//}
//
//void play() {
//	// 17번 hit 발생하면 끝
//	int hitCount = 0;
//
//	// 지도 UNKNOWN(-1)으로 초기화
//	for (int r = 0; r < MAP_SIZE; r++) {
//		for (int c = 0; c < MAP_SIZE; c++) {
//			map[r][c] = UNKNOWN;
//		}
//	}
//	// 각 TYPE별 찾은 칸의 개수(found) 초기화
//	for (int i = 0; i < TYPE_NUM; i++) {
//		found[i] = 0;
//	}
//
//	// 17번 hit 발생하면 끝
//	while (hitCount < 17) {
//
//		// 각 칸의 확률 초기화
//		for (int r = 0; r < MAP_SIZE; r++) {
//			for (int c = 0; c < MAP_SIZE; c++) {
//				prob[r][c] = 0;
//			}
//		}
//
//		// 모든 칸에서 모든 type, 모든 방향을 고려하여 각 칸의 확률값 업데이트
//		for (int r = 0; r < MAP_SIZE; r++) {
//			for (int c = 0; c < MAP_SIZE; c++) {
//				for (int type = 1; type < TYPE_NUM; type++) {
//					// 이미 모두 찾은 type에 대해서는 더 이상 찾을 필요 없으니 continue
//					if (found[type] == len[type])
//						continue;
//					for (int dir = 0; dir < 4; dir++) {
//						updateProb(r, c, type, dir);
//					}
//				}
//			}
//		}
//
//		// 확률이 가장 높은 칸 선택
//		int maxProb = 0;
//		int fireR = 0;
//		int fireC = 0;
//		for (int r = 0; r < MAP_SIZE; r++) {
//			for (int c = 0; c < MAP_SIZE; c++) {
//				if (map[r][c] == UNKNOWN && maxProb < prob[r][c]) {
//					maxProb = prob[r][c];
//					fireR = r;
//					fireC = c;
//				}
//			}
//		}
//
//		// 확률이 가장 높은 칸에 대해 fire함수 호출
//		map[fireR][fireC] = fire(fireR, fireC);
//
//		// HIT 발생 시 found 배열, hitCount 값 갱신
//		if (map[fireR][fireC] != MISS) {
//			found[map[fireR][fireC]]++;
//			hitCount++;
//		}
//	}
//}
