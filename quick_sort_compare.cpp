//#include <stdio.h>
//#include <time.h>
//
//#define MAX 3500
//
//int input[MAX];
//int num;
//clock_t start, end;
//float duration;
//
//void swap(int a, int b) {
//	int tmp = input[a];
//	input[a] = input[b];
//	input[b] = tmp;
//}
//
//// 매우 느리다, 쓰지말것
//void quickSortSlow(int first, int last) {
//	int pivot, i, j, temp;
//
//	if (first < last) {
//		pivot = first;
//		i = first;
//		j = last;
//
//		while (i < j) {
//			while (input[i] <= input[pivot] && i < last) i++;
//			while (input[j] > input[pivot]) j--;
//			if (i < j) swap(i, j);
//		}
//
//		swap(pivot, j);
//
//		quickSortSlow(first, j - 1);
//		quickSortSlow(j + 1, last);
//	}
//}
//
//void quickSort(int l, int r) {
//	int L = l;
//	int R = r;
//	int pivot = (l + r) / 2;
//
//	if (input[L] > input[pivot]) swap(L, pivot);
//	if (input[L] > input[R]) swap(L, R);
//	if (input[pivot] > input[R]) swap(pivot, R);
//
//	while (L <= R) {
//		while (input[L] < input[pivot]) L++;
//		while (input[pivot] < input[R]) R--;
//		if (L <= R) {
//			swap(L, R);
//			L++;
//			R--;
//		}
//		if (l < R) quickSort(l, R);
//		if (L < r) quickSort(L, r);
//	}
//}
//
//void printResult(void){
//	for (int i = 0; i < MAX; i++) printf("%d ", input[i]);
//	printf("\n");
//}
//
//void initialize() {
//	for (int i = 0; i < MAX; i++) {
//		input[i] = MAX - i;
//	}
//}
//
//int main(void){
//
//	initialize();
//	start = clock();
//	quickSort(0, MAX - 1);
//	end = clock();
//	duration = (float)(end - start) / CLOCKS_PER_SEC;
//	printResult();
//	printf("%f\n", duration);
//
//	initialize();
//	start = clock();
//	quickSortSlow(0, MAX - 1);
//	end = clock();
//	duration = (float)(end - start) / CLOCKS_PER_SEC;
//	printResult();
//	printf("%f\n", duration);
//
//}
