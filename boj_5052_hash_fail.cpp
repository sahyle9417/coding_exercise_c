//// 이 문제는 해시로 풀어선 안된다, 트라이가 더 좋은 선택
//// 더 긴 숫자가 먼저 들어가 있고, 더 짧은 숫자가 나중에 들어오면 일관성이 없지만 있다고 나온다.
//// 반례 : 12345, 123 입력 시 일관성 없지만 일관성 있다고 나옴
//
//#include <stdio.h>
//#define MAX_TABLE 100000
//#define MAX_NODE 10001
//
//typedef struct _node {
//	char num[11];
//	_node* next;
//} NODE;
//
//NODE hashTable[MAX_TABLE];
//NODE nodePool[MAX_NODE];
//int nodePoolIdx = 0;
//
//void init() {
//	nodePoolIdx = 0;
//	for (int i = 0; i < MAX_TABLE; i++) {
//		hashTable[i].next = 0;
//	}
//}
//
//int mstrcmp(char* a, char* b) {
//	int idx = 0;
//	while (a[idx]) {
//		if (a[idx] != b[idx]) return a[idx] - b[idx];
//		idx++;
//	}
//	return a[idx] - b[idx];
//}
//
//void mstrcpy(char* dst, char* src) {
//	int idx = 0;
//	while (src[idx]) {
//		dst[idx] = src[idx];
//		idx++;
//	}
//	dst[idx] = 0;
//}
//
//unsigned long getHkey(const char* str) {
//	unsigned long hash = 5381;
//	int c;
//	while (c = *str++) {
//		hash = ((hash << 5) + hash) + c;
//	}
//	return hash % MAX_TABLE;
//}
//
//bool detectCollision(char num[11]) {
//	int idx = 0;
//	while (num[idx]) {
//		char tmp = num[idx + 1];
//		num[idx + 1] = 0;
//		int hkey = getHkey(num);
//		NODE* cur = hashTable[hkey].next;
//		while (cur) {
//			// 충돌 발견
//			if (!mstrcmp(num, cur->num)) { return true; }
//			cur = cur->next;
//		}
//		num[idx + 1] = tmp;
//		idx++;
//	}
//	return false;
//}
//
//void insertNode(char num[11]) {
//	int hkey = getHkey(num);
//	NODE* newNode = &nodePool[nodePoolIdx++];
//	mstrcpy(newNode->num, num);
//	newNode->next = hashTable[hkey].next;
//	hashTable[hkey].next = newNode;
//}
//
//int main() {
//	int TC;
//	scanf("%d", &TC);
//	for (int tc = 0; tc < TC; tc++) {
//		init();
//		bool valid = true;
//
//		int N;
//		scanf("%d", &N);
//		char num[11];
//		for (int n = 0; n < N; n++) {
//			scanf("%s", num);
//			if (!valid) continue;
//			if (detectCollision(num)) {
//				valid = false;
//			}
//			else { insertNode(num); }
//		}
//		if (valid) printf("YES\n");
//		else printf("NO\n");
//	}
//}