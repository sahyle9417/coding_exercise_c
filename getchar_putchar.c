//#include <stdio.h>
//
//int main(void) {
//
//	int c1, c2;
//	c1 = getchar();		// 표준입력으로 char 변수 입력
//	c2 = fgetc(stdin);	// 스트림 지정해 char 변수 입력 (stdin : 표준입력 스트림)
//
//	putchar(c1);		// 표준출력으로 char 변수 출력
//	fputc(c2, stdout);	// 스트림 지정해 char 변수 출력 (stdout : 표준출력 스트림)
//
//	// scanf, printf보다 속도 빠르고 메모리 적게 사용
//	// 단순히 문자 하나씩 입출력할때는 getchar() putchar() 사용하기
//}