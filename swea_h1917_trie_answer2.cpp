//typedef long long int LL;
//
//#define MAX_USER		1000
//#define MAX_TAG			5000
//#define NULL			0
//#define MAX_CALL		50000
//#define MAX_ORDER		7
//#define SHIFT_UNIT		5
//#define MAX_BUCKET		9999
//#define MAX_RET			5
//
//struct MSG {
//	int userID;
//	int msgID;
//	int time;
//	MSG* next;
//};
//
//struct TAG {
//	LL tagId;
//	int msgNum;
//	MSG* msgList[MAX_ORDER + 1];
//};
//
//struct USER {
//	int num_follow;
//	USER* follow[MAX_USER];
//	MSG* valid;
//};
//
//// TAG들의 해시테이블
//TAG tagArr[MAX_BUCKET][MAX_TAG / 1000];
//MSG msgArr[MAX_CALL];
//USER userArr[MAX_USER];
//
//int msgNums;
//int tagNumInBucket[MAX_BUCKET];
//
//MSG* retMsg[MAX_RET + 1];
//
//bool compare(MSG* m1, MSG* m2)
//{
//	if (m1->time == m2->time)
//	{
//		return m1->userID < m2->userID;
//	}
//
//	return m1->time > m2->time;
//}
//
//void init() {
//	msgNums = 0;
//
//	for (int i = 0; i < MAX_BUCKET; i++) {
//		tagNumInBucket[i] = 0;
//	}
//
//	for (int i = 0; i < MAX_USER; i++) {
//		userArr[i].num_follow = 0;
//		userArr[i].valid = NULL;
//	}
//}
//
//
//void createMessage(int msgID, int userID, char msgData[]) {
//	MSG* newMsg = &msgArr[msgNums++];
//	int i, j;
//
//	newMsg->userID = userID;
//	newMsg->msgID = msgID;
//	newMsg->next = userArr[userID].valid;
//	userArr[userID].valid = newMsg;
//	newMsg->time =
//		(msgData[0] - '0') * 100000	+ (msgData[1] - '0') * 10000 +
//		(msgData[3] - '0') * 1000	+ (msgData[4] - '0') * 100 +
//		(msgData[6] - '0') * 10		+ msgData[7] - '0';
//
//	int msgDataIdx = 8;
//	while (msgData[msgDataIdx]) {
//		msgDataIdx += 2;
//
//		// 비트마스킹 이용하여 한 글자당 5비트씩 사용 (a:0, z:25니까 5비트로 표현 가능)
//		LL tagId = 0LL;
//		for (; 'a' <= msgData[msgDataIdx] && msgData[msgDataIdx] <= 'z'; msgDataIdx++) {
//			tagId <<= SHIFT_UNIT;
//			tagId |= msgData[msgDataIdx] - 'a';
//		}
//
//		// 해시테이블 순회하며 tag 탐색
//		int bucketIdx = tagId % MAX_BUCKET;
//		for (i = 0; i < tagNumInBucket[bucketIdx]; i++) {
//			if (tagArr[bucketIdx][i].tagId == tagId) {
//				break;
//			}
//		}
//
//		TAG* tag;
//
//		// 해시테이블 순회했지만 같은 값 찾지 못했다면 새로운 tag 발견한 것임
//		if (i == tagNumInBucket[bucketIdx]) {
//			tag = &tagArr[bucketIdx][tagNumInBucket[bucketIdx]++];
//			for (j = 1; j < MAX_ORDER + 1; j++) {
//				tag->msgList[j] = NULL;
//			}
//
//			tag->tagId = tagId;
//			tag->msgNum = 0;
//		}
//		// 해시테이블에서 같은 값 찾았다면 기존에 존재하던 tag
//		else {
//			tag = &tagArr[bucketIdx][i];
//		}
//
//		// 해당 tag를 포함하는 msg 객체들의 리스트에 newMsg 추가
//		tag->msgList[tag->msgNum++] = newMsg;
//		tag->msgNum %= MAX_ORDER;
//	}
//}
//
//void followUser(int userID1, int userID2) {
//	userArr[userID1].follow[userArr[userID1].num_follow++] = &userArr[userID2];
//}
//
//int searchByHashtag(char tagName[], int retIDs[]) {
//	int i, j, k;
//	LL tagId = 0LL;
//
//	// 비트마스킹 이용하여 한 글자당 5비트씩 사용 (a:0, z:25니까 5비트로 표현 가능)
//	for (i = 1; tagName[i]; i++) {
//		tagId <<= SHIFT_UNIT;
//		tagId |= tagName[i] - 'a';
//	}
//
//	// 해시테이블 순회하며 tag 탐색
//	int bucketIdx = tagId % MAX_BUCKET;
//	for (i = 0; i < tagNumInBucket[bucketIdx]; i++) {
//		if (tagArr[bucketIdx][i].tagId == tagId) {
//			break;
//		}
//	}
//
//	// 해시테이블 순회했지만 tag 발견 못함
//	if (i == tagNumInBucket[bucketIdx]) {
//		return 0;
//	}
//
//	TAG* tag = &tagArr[bucketIdx][i];
//
//	for (i = 0; i < MAX_RET; i++) {
//		retMsg[i] = NULL;
//	}
//
//	for (i = 0; tag->msgList[i]; i++) {
//		for (j = 0; j < MAX_RET; j++) {
//			if (retMsg[j]) {
//				if (compare(tag->msgList[i], retMsg[j])) {
//					for (k = MAX_RET - 2; k >= j; k--) {
//						retMsg[k + 1] = retMsg[k];
//					}
//					retMsg[j] = tag->msgList[i];
//					break;
//				}
//			}
//			else {
//				retMsg[j] = tag->msgList[i];
//				break;
//			}
//		}
//	}
//
//	for (i = 0; retMsg[i]; i++) {
//		retIDs[i] = retMsg[i]->msgID;
//	}
//
//	return i;
//}
//
//void processUser(USER* user)
//{
//	int count = 0;
//	for (MSG* msg = user->valid; msg; msg = msg->next)
//	{
//		for (int j = 0; j < MAX_RET; j++)
//		{
//			if (retMsg[j])
//			{
//				if (compare(msg, retMsg[j]))
//				{
//					for (int k = MAX_RET - 2; k >= j; k--)
//					{
//						retMsg[k + 1] = retMsg[k];
//					}
//					retMsg[j] = msg;
//					break;
//				}
//			}
//			else
//			{
//				retMsg[j] = msg;
//				break;
//			}
//		}
//
//		if (++count == 5)
//		{
//			msg->next = NULL;
//			break;
//		}
//	}
//}
//
//int getMessages(int userID, int retIDs[])
//{
//	int i;
//	USER* user = &userArr[userID];
//
//	for (i = 0; i < MAX_RET; i++)
//	{
//		retMsg[i] = NULL;
//	}
//
//	processUser(user);
//	for (i = 0; i < user->num_follow; i++)
//	{
//		processUser(user->follow[i]);
//	}
//
//	for (i = 0; retMsg[i]; i++)
//	{
//		retIDs[i] = retMsg[i]->msgID;
//	}
//
//	return i;
//}