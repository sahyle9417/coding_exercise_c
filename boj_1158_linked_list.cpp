//// head와 tail에도 data 존재하면 head, tail에 담긴 데이터를 추가, 삭제할 때
//// head, tail이 아닌 노드들에 담긴 데이터 추가, 삭제와 달리 별도의 처리 필요
//// 여기서는 head와 tail을 Dummy Node로 사용하기 위해 실제 data는 둘 사이의 노드에만 저장
//// head의 prev와 tail의 next는 NULL, 즉 원형의 성질을 가지지 않는다.
//// cur을 이동시키는 함수(ListForward, ListBackward)에서 적절히 처리해주면 원형처럼 동작도 가능
//
//#include <stdio.h>
//#include <malloc.h>
//
//// struct 내에서는 별칭(Node) 사용 불가, 더 아래에서 선언되기 때문
//typedef struct node {
//	struct node* prev;
//	struct node* next;
//	int data;
//} Node;
//
//typedef struct {
//	Node* head;
//	Node* tail;
//	Node* cur;
//	int num;
//} List;
//
//void ListInit(List* list) {
//
//	list->head = (Node*)malloc(sizeof(Node));
//	list->tail = (Node*)malloc(sizeof(Node));
//
//	list->head->prev = NULL;
//	list->head->next = list->tail;
//
//	list->tail->prev = list->head;
//	list->tail->next = NULL;
//
//	list->cur = list->head;
//	list->num = 0;
//}
//
//// 끝에 노드 추가, 새로운 노드가 cur로 지정됨
//void ListAppend(List* list, int newData) {
//	Node* newNode = (Node*)malloc(sizeof(Node));
//	newNode->data = newData;
//
//	newNode->prev = list->tail->prev;
//	newNode->next = list->tail;
//	list->tail->prev->next = newNode;
//	list->tail->prev = newNode;
//
//	list->cur = newNode;
//	list->num++;
//}
//
//// cur노드 뒤에 노드 추가, 새로운 노드가 cur로 지정됨
//void ListInsert(List* list, int data) {
//	Node * newNode = (Node*)malloc(sizeof(Node));
//	newNode->data = data;
//
//	newNode->prev = list->cur;
//	newNode->next = list->cur->next;
//	list->cur->next->prev = newNode;
//	list->cur->next = newNode;
//
//	list->cur = newNode;
//	list->num++;
//}
//
//// 첫번째 노드가 존재한다면 cur로 지정하고 rdata에 기록 후 true 반환
//// 첫번째 노드가 존재하지 않으면(노드가 하나도 없다면) false 반환
//bool ListFirst(List* list, int* rdata) {
//	if (list->num == 0) return false;
//	list->cur = list->head->next;
//	*rdata = list->cur->data;
//	return true;
//}
//
//// 마지막 노드가 존재한다면 cur로 지정하고 rdata에 기록 후 true 반환
//// 마지막 노드가 존재하지 않으면(노드가 하나도 없다면) false 반환
//bool ListLast(List* list, int* rdata) {
//	if (list->num == 0) return false;
//	list->cur = list->tail->prev;
//	*rdata = list->cur->data;
//	return true;
//}
//
//// 다음 노드에 값이 있다면 다음 노드를 cur로 지정하고 rdata에 기록 후 true 반환
//// 다음 노드가 tail이면 false 반환
//bool ListForward(List* list, int* rdata) {
//	if (list->cur->next == list->tail) return false;
//	list->cur = list->cur->next;
//	*rdata = list->cur->data;
//	return true;
//}
//
//// 이전 노드에 값이 있다면 cur로 지정하고 rdata에 기록 후 true 반환
//// 이전 노드가 head면 false 반환
//bool ListBackward(List* list, int* rdata) {
//	if (list->cur->prev == list->head) return false;
//	list->cur = list->cur->prev;
//	*rdata = list->cur->data;
//	return true;
//}
//
//// 현재 cur이 가리키는 노드 삭제
//int ListRemove(List* list) {
//	Node* rnode = list->cur;
//	int rdata = rnode->data;
//	rnode->prev->next = rnode->next;
//	rnode->next->prev = rnode->prev;
//	list->cur = rnode->prev;
//	free(rnode);
//	list->num--;
//	return rdata;
//}
//
//int main() {
//	int N, K;
//	scanf("%d %d", &N, &K);
//
//	// malloc을 함수 안에서 실행했다가 한참 삽질했다
//	// malloc은 메인에서 실행해서 유효한 포인터를 함수에게 넘겨주자
//	List* mylist = (List*)malloc(sizeof(List));
//	ListInit(mylist);
//
//	for (int i = 1; i <= N; i++) {
//		ListAppend(mylist, i);
//	}
//
//	/*printf("<");
//	int data;
//	if (ListFirst(mylist, &data)) {
//		printf("%d", data);
//		while (ListForward(mylist, &data)) {
//			printf(", %d", data);
//		}
//	}
//	printf(">\n");*/
//
//	int tmp;
//	// 마지막 노드로 커서 이동
//	// 마지막 노드에서 K칸씩 점프할 것임
//	ListLast(mylist, &tmp);
//
//	printf("<");
//	while (mylist->num > 1) {
//		// 1번째 노드부터 K번째 노드까지 한칸씩 이동
//		for (int i = 1; i <= K; i++) {
//			if (!ListForward(mylist, &tmp)) {
//				ListFirst(mylist, &tmp);
//			}
//		}
//		//printf("\nremove %d\n", mylist->cur->data);
//		//ListRemove(mylist);
//		printf("%d, ", ListRemove(mylist));
//	}
//	ListFirst(mylist, &tmp);
//	printf("%d>", tmp);
//}
