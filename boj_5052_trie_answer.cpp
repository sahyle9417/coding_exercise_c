//#include <stdio.h>
//#define CHILD_NUM 10
//#define MAX_LEN 11
//#define MAX_NODE 100000
//
//typedef struct _trie_node {
//	_trie_node* child[CHILD_NUM];	// 자식 노드의 개수 10 (숫자 0~9)
//	bool endNode;					// 구조체의 bool타입 멤버변수는 false로 자동 초기화됨 (구조체 밖에선 자동초기화 안됨)
//} TRIE_NODE;						// 구조체 정의부에서 멤버변수를 임의로 초기화하는 건 불가능 (메모리 공간이 잡혀있지 않기 때문)
//
//TRIE_NODE trieNodePool[MAX_NODE];
//int trieNodeIdx;
//
//char str[MAX_LEN];	// 전화번호 최대길이 10
//
//bool find() {
//	TRIE_NODE* node = &trieNodePool[0];	// 0번은 루트 노드
//
//	for (int strIdx = 0; str[strIdx]; strIdx++) {
//		int childIdx = str[strIdx] - '0';
//		// 노드가 존재하지 않으면 새로운 노드 생성
//		if (node->child[childIdx] == 0) {
//			node->child[childIdx] = &trieNodePool[trieNodeIdx++];
//		}
//		node = node->child[childIdx];
//		// 충돌 발생 케이스 1 (나보다 짧으면서 충돌 일으키는 전화번호 발견)
//		// 해당 노드로 끝나는 전화번호가 있다면 충돌 발생
//		// 현재 입력받은 전화번호가 해당 노드로 끝나는 전화번호를 포함하고 있음
//		if (node->endNode) return true;
//	}
//	// 전화번호 입력 끝남
//
//	// 충돌 발생 케이스 2 (나보다 길면서 충돌 일으키는 전화번호 발견)
//	// 현재 입력된 전화번호를 포함하며 길이가 더 긴 전화번호가 존재하면 충돌 발생
//	for (int i = 0; i < CHILD_NUM; i++) {
//		if (node->child[i]) return true;
//	}
//	// 위에서 다룬 2가지 충돌 케이스 모두 해당하지 않으므로 충돌 발생X
//	// 현재 입력받은 전화번호의 끝에 위치한 노드에 endNode 표시
//	node->endNode = true;
//	return false;
//}
//
//void init() {
//	for (int i = 0; i < trieNodeIdx; i++) {
//		// 모든 노드들의 endNode값은 false로 초기화, c++에서 bool은 false로 초기화됨
//		trieNodePool[i].endNode = false;
//		for (int j = 0; j < CHILD_NUM; j++) {
//			trieNodePool[i].child[j] = 0;
//		}
//	}
//	// 0번은 루트 노드, 매 TC마다 이거 지정해줘야함, 이거 안해줘서 오답 나왔던 것 같음
//	trieNodeIdx = 1;
//}
//
//int main() {
//	int TC;
//	scanf("%d", &TC);
//
//	while (TC--) {
//		init();
//
//		int N;
//		scanf("%d", &N);
//
//		bool ret = true;
//		while (N--) {
//			scanf("%s", str);
//			if (find()) ret = false;
//		}
//
//		printf("%s\n", ret ? "YES" : "NO");
//	}
//}