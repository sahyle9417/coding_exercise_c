//#include <stdio.h>
//int input[100][100];
//
//// 좌우상
//int dx[3] = {-1, 1, 0};
//int dy[3] = {0, 0, -1};
//
//int main() {
//	freopen("input.txt", "r", stdin);
//	for (int tc = 1; tc <= 10; tc++) {
//		int t;
//		scanf("%d ", &t);
//
//		for (int y = 0; y < 100; y++) {
//			for (int x = 0; x < 100; x++) {
//				scanf("%d ", &input[y][x]);
//			}
//		}
//
//		bool visit[100][100];
//
//		int min_distance = 9999;
//		int answer = -1;
//		int dst_x = 0;
//		for (; dst_x < 100; dst_x++) {
//
//			// 도착지 발견, 시작지점으로 설정
//			if (input[99][dst_x] == 1) {
//				int x = dst_x;
//				int y = 99;
//
//				// visit 초기화 후 시작지점 방문 처리
//				for (int y = 0; y < 100; y++) {
//					for (int x = 0; x < 100; x++) {
//						visit[y][x] = false;
//					}
//				}
//				visit[y][x] = true;
//
//				// 이동거리 기록할 예정
//				int distance = 0;
//
//				//printf("[%d][%d]\n", y, x);
//				while (y > 0) {
//					for (int d = 0; d <= 2; d++) {
//						int next_x = x + dx[d];
//						int next_y = y + dy[d];
//						if (next_x < 0 || next_x>99) continue;
//						/*printf("[%d][%d] : %d ", next_y, next_x, input[next_y][next_x]);
//						if (visit[next_y][next_x]) printf("t\n");
//						else printf("t\n");*/
//						if (!visit[next_y][next_x] && input[next_y][next_x] == 1) {
//							x = next_x;
//							y = next_y;
//							visit[y][x] = true;
//							distance++;
//							break;
//						}
//					}
//				}
//
//				//printf("x:%d, d:%d\n", x, distance);
//
//				if (distance <= min_distance) {
//					min_distance = distance;
//					answer = x;
//				}
//			}
//		}
//		printf("#%d %d\n", tc, answer);
//		//printf("=====================================\n\n\n");
//	}
//}