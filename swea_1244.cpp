//#include <stdio.h>
//#include <malloc.h>
//#define MAX 6
//
//void quicksort(int* arr, int l, int r, int* index) {
//	int L = l;
//	int R = r;
//	int pivot = arr[(l + r) / 2];
//
//	while (L <= R) {
//		while (pivot < arr[L]) L++;
//		while (arr[R] < pivot) R--;
//		if (L <= R) {
//			// 값을 담고 있는 arr 배열 뿐만 아니라 인덱스를 담고 있는 index 배열도 값 기준 내림차순 정렬
//			int tmp = arr[L];
//			arr[L] = arr[R];
//			arr[R] = tmp;
//			tmp = index[L];
//			index[L] = index[R];
//			index[R] = tmp;
//			L++;
//			R--;
//		}
//	}
//	if (l < R) quicksort(arr, l, R, index);
//	if (L < r) quicksort(arr, L, r, index);
//}
//
//int main() {
//	freopen("input.txt", "r", stdin);
//	int TC;
//	scanf("%d", &TC);
//
//	for (int tc = 1; tc <= TC; tc++) {
//		char str[MAX+1];
//		scanf("%s", str);
//		int input[MAX];
//		int sorted[MAX];
//		int index[MAX];
//
//		int len = 0;
//		while (str[len] != '\0') {
//			int tmp = str[len] - '0';
//			input[len] = tmp;
//			sorted[len] = tmp;
//			index[len] = len;
//			len++;
//		}
//		quicksort(sorted, 0, len-1, index);
//		
//		int swap_num;
//		scanf("%d", &swap_num);
//
//		/*for (int ii = 0; ii < len; ii++) printf("%d ", index[ii]);
//		puts("");*/
//
//		int target = 0;
//		bool duplicate = false;
//
//		while (swap_num > 0 && target < len-1) {
//			for (int i = target+1; i < len; i++) {
//				if (index[i] == target) {
//					index[i] = index[target];
//					index[target] = target;
//					/*for (int ii = 0; ii < len; ii++) printf("%d ", index[ii]);
//					puts("");*/
//					if (sorted[index[i]] == sorted[index[target]]) duplicate = true;
//					else swap_num--;
//					break;
//				}
//			}
//			target++;
//		}
//		//if (!duplicate && swap_num % 2 == 1) {
//		if (!duplicate && swap_num & 1 == 1) {
//			int tmp = index[len - 1];
//			index[len - 1] = index[len - 2];
//			index[len - 2] = tmp;
//		}
//
//		int* output = (int*)malloc(sizeof(int)*len);
//		for (int i = 0; i < len; i++) {
//			output[index[i]] = sorted[i];
//		}
//		printf("#%d ", tc);
//		for (int i = 0; i < len; i++) printf("%d", output[i]);
//		puts("");
//	}
//}