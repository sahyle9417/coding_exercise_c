//#include <iostream>
//#include <string>
//#include <vector>
//#include <algorithm>
//#include <math.h>
//
//using namespace std;
//
//vector<int> inputVector;
//vector<long long> outputVector;
//
//void swap(int i, int j) {
//	if (i == j) return;
//	int temp = inputVector[i];
//	inputVector[i] = inputVector[j];
//	inputVector[j] = temp;
//	return;
//}
//
//void addToOutputVector() {
//	long long num = inputVector[0];
//	for (int i = 1; i < inputVector.size(); i++) {
//		num *= 10;
//		num += inputVector[i];
//	}
//	outputVector.push_back(num);
//}
//
//void Permutation(int TotalN, int N) {
//	if (N == 1) {
//		addToOutputVector();
//		return;
//	}
//	for (int i = 0; i < N; i++) {
//		swap(i, N - 1);
//		Permutation(TotalN, N - 1);
//		swap(i, N - 1);
//	}
//}
//
//void getInput() {
//	string strInput;		// 띄어쓰기 기준으로 N개의 수를 입력받을 String
//	getline(cin, strInput);	// 숫자 입력 받기
//	string strNum = "";		// 각각의 숫자를 저장할 임시 String
//	for (int i = 0; i < strInput.length(); i++) {
//		if (strInput.at(i) == ' ') {
//			// 현재까지 저장한 문자(숫자)들을 Vector에 추가 후 String 초기화
//			inputVector.push_back(atoi(strNum.c_str()));
//			strNum = "";
//		}
//		else {
//			// 띄어쓰기가 나올 때까지 문자 더함
//			strNum += strInput.at(i);
//			continue;
//		}
//	}
//	inputVector.push_back(atoi(strNum.c_str()));	// 마지막 숫자도 벡터에 추가
//}
//
//void main() {
//	getInput();
//
//	int N = inputVector.size();
//	Permutation(N, N);
//
//	sort(outputVector.begin(), outputVector.end());
//
//	// 테스트용
//	/*for (int i = 0; i < outputVector.size(); i++) {
//		if (outputVector[i] < pow(10, inputVector.size() - 1))
//			printf("0");
//		printf("%ld\n", outputVector[i]);
//	}*/
//
//	int K;
//	scanf("%d", &K);
//	long long output = outputVector[K - 1];
//	long long zeroPadThreshold = pow(10, inputVector.size() - 1);
//	if (output < zeroPadThreshold)
//		printf("0");
//	printf("%lld", output);
//}
//
