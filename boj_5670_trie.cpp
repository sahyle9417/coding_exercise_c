//#include <stdio.h>
//
//#define MAX_LEN 81			// 단어 하나의 최대 길이
//#define MAX_N 100005		// 단어의 최대개수
//#define MAX_NODE 500006		// 트라이 노드의 최대개수, 이걸 100000으로 해서 런타임 오류 계속 발생
//#define CHILD_NUM 26		// 알파벳 가짓수 (a~z)
//
//typedef struct _node {
//	bool isEnd;
//	int childNum;
//	_node* child[CHILD_NUM];
//} NODE;
//
//NODE nodePool[MAX_NODE];
//int nodePoolIdx = 1;
//
//char dict[MAX_N][MAX_LEN];
//int dictIdx = 0;
//
//void init() {
//	for (int i = 0; i < nodePoolIdx; i++) {
//		nodePool[i].isEnd = false;
//		nodePool[i].childNum = 0;
//		for (int j = 0; j < CHILD_NUM; j++) {
//			nodePool[i].child[j] = 0;
//		}
//	}
//	nodePoolIdx = 1;
//	dictIdx = 0;
//}
//
//void insertTrie(char str[MAX_LEN]) {
//	NODE* node = &nodePool[0];
//	for (int i = 0; str[i]; i++) {
//		// 여기서 node->isEnd를 false로 설정했다가 한참 삽질함
//		// 기존에 true로 적혀있던 것을 날려버렸었음
//		// isEnd를 false로 설정하는 것은 TC 초기화할 때만 수행
//		int childIdx = str[i] - 'a';
//		if (!node->child[childIdx]) {
//			node->childNum++;
//			node->child[childIdx] = &nodePool[nodePoolIdx++];
//		}
//		node = node->child[childIdx];
//	}
//	node->isEnd = true;
//}
//
//void insertDict(char str[MAX_LEN]) {
//	int i = 0;
//	while (str[i]) {
//		dict[dictIdx][i] = str[i];
//		i++;
//	}
//	dict[dictIdx][i] = 0;
//	dictIdx++;
//}
//
//int getCnt(char str[MAX_LEN]) {
//	int cnt = 1;	// 무조건 1번은 눌러야함
//	NODE* node = nodePool[0].child[str[0] - 'a'];
//	for (int i = 1; str[i]; i++) {
//		// 자식이 여러개거나, 내가 아닌 어떤 문자열이 현재 위치에서 끝나는 경우, 입력 필요
//		if (node->childNum > 1 || node->isEnd) {
//			cnt++;
//		}
//		node = node->child[str[i] - 'a'];
//	}
//	return cnt;
//}
//
//int main() {
//	int N;
//	char str[MAX_LEN];
//
//	while (~scanf("%d", &N)) {
//		init();
//		
//		for (int n = 0; n < N; n++) {
//			scanf("%s", str);
//			insertTrie(str);
//			insertDict(str);
//		}
//
//		int sum = 0;
//		for (int di = 0; di < dictIdx; di++) {
//			//printf("%-10s : %d\n", dict[di], getCnt(dict[di]));
//			sum += getCnt(dict[di]);
//		}
//		printf("%.2lf\n", ((double)sum / dictIdx));
//	}
//	return 0;
//}