//#define MAXR        99
//#define MAXC        26
//
//#define MAX_VAL -1000000001
//#define MIN_VAL 1000000001
//#define MAX(a, b) ((a)>(b)?(a):(b))
//#define MIN(a, b) ((a)<(b)?(a):(b))
//#define NULL 0
//
//int cmax, rmax;
//
//struct POS { int r, c; };
//
//int parse_val(char* str) {
//	int sign = 1;
//	if (*str == '-') {
//		sign = -1;
//		str++;
//	}
//	int ret = 0;
//	char ch;
//	int digit = 0;
//	while (ch = *str++) {
//		if (ch < '0' || ch > '9') break;
//		digit = ch - '0';
//		ret = (ret * 10) + digit;
//	}
//	return ret * sign;
//}
//
//// k:시작 인덱스, input:입력 문자열
//POS parse_pos(int k, char input[]) {
//	POS ret;
//	ret.c = (int)(input[k++] - 'A');
//	ret.r = (int)(input[k++] - '0');
//	// 행 번호가 2자리 숫자인 경우
//	if (input[k] >= '0' && input[k] <= '9') {
//		ret.r *= 10;
//		ret.r += (int)(input[k] - '0');
//	}
//	// 입력받은 행번호(r)은 1부터 시작, 0부터 시작하도록 변환
//	ret.r--;
//	return ret;
//}
//
//struct REF {
//	int r;
//	int c;
//	REF* next;
//};
//
//// REF를 동적할당하지 않고 하나씩 떼다씀
//REF refPool[20000];
//int refPoolIdx = 0;
//
//REF* q[1000];
//int front, rear;
//
//struct NODE {
//	int cmd, val;
//	POS from1, from2;
//	REF head;
//};
//
//NODE map[MAXR][MAXC];
//
//// from은 피연산자 노드, toR, toC는 연산 결과 칸
//void add_list(NODE* from, int toR, int toC) {
//	// 연산 결과 기록할 칸(ref) 새로 할당
//	REF* to = &refPool[refPoolIdx++];
//	to->r = toR;
//	to->c = toC;
//	// ref(연산 결과 칸)의 참조 대상 리스트에 피연산자 노드 t가 포함된 리스트의 맨앞(head의 next)을 추가
//	// ref(연산 결과 칸)가 피연산자가 참조하는 것들까지 연쇄적으로 참조하는 것임
//	// 반대로 피연산자 입장에서는 ref(연산 결과 칸)가 참조리스트의 맨앞(head.next)에 위치하는 것임
//	to->next = from->head.next;
//	from->head.next = to;
//}
//
//// 연산결과 기록될 칸(map[toR][toC])이 가지는 참조관계 추가
//// 즉, 연산결과 기록될 칸(map[toR][toC])의 피연산자 칸들(from1, from2)에 대해 add_list 함수 호출
//void add_ref(int toR, int toC) {
//	// 함수가 없는 칸은 참조관계 추가하지 않음
//	int toCmd = map[toR][toC].cmd;
//	if (toCmd == 0) return;
//	// p1, p2 : 피연산자들
//	POS from1 = map[toR][toC].from1;
//	POS from2 = map[toR][toC].from2;
//	switch (toCmd) {
//	case 1:
//	case 2:
//	case 3:
//	case 4:
//		// ADD, SUB, MUL, DIV : 2개의 칸에 대해서만 add_list 수행
//		add_list(&map[from1.r][from1.c], toR, toC);
//		add_list(&map[from2.r][from2.c], toR, toC);
//		break;
//	case 5:
//	case 6:
//	case 7:
//		// MAX, MIN, SUM : 2개의 칸 사이의 모든 칸들에 대해 add_list 수행
//		if (from1.c == from2.c) {
//			for (int i = from1.r; i <= from2.r; i++) {
//				add_list(&map[i][from1.c], toR, toC);
//			}
//		}
//		else { // if (from1.r == from2.r)
//			for (int i = from1.c; i <= from2.c; i++) {
//				add_list(&map[from1.r][i], toR, toC);
//			}
//		}
//		break;
//	}
//}
//
//// 피연산자 노드 from이 영향을 끼치는 칸들의 리스트에서 연산결과 칸(toR, toC) 삭제 (건너뛰도록 설정)
//void remove_list(NODE* from, int toR, int toC) {
//	REF* cur = &(from->head);
//	while (cur->next) {
//		if (cur->next->r == toR && cur->next->c == toC) {
//			cur->next = cur->next->next;
//			return;
//		}
//		cur = cur->next;
//	}
//}
//
//// 연산결과 기록될 칸(map[toR][toC])이 가지는 참조관계 삭제
//// 즉, 연산결과 기록될 칸(map[toR][toC])의 피연산자 칸들(from1, from2)에 대해 remove_list 함수 호출
//void remove_ref(int toR, int toC) {
//	// 함수가 아닌 값이 적힌 칸은 삭제할 참조관계가 존재하지 않음
//	if (map[toR][toC].cmd == 0) return;
//	// p1, p2 : 피연산자
//	POS from1 = map[toR][toC].from1;
//	POS from2 = map[toR][toC].from2;
//	switch (map[toR][toC].cmd) {
//	case 1:
//	case 2:
//	case 3:
//	case 4:
//		// ADD, SUB, MUL, DIV : 2개의 칸에 대해서만 remove_list 수행
//		remove_list(&map[from1.r][from1.c], toR, toC);
//		remove_list(&map[from2.r][from2.c], toR, toC);
//		break;
//	case 5:
//	case 6:
//	case 7:
//		// MAX, MIN, SUM : 2개의 칸 사이의 모든 칸들에 대해 add_list 수행
//		// 같은 열 내에서 행 방향으로 연산
//		if (from1.c == from2.c) {
//			for (int i = from1.r; i <= from2.r; i++) {
//				remove_list(&map[i][from1.c], toR, toC);
//			}
//		}
//		// 같은 행 내에서 열 방향으로 연산
//		else if (from1.r == from2.r) {
//			for (int i = from1.c; i <= from2.c; i++) {
//				remove_list(&map[from1.r][i], toR, toC);
//			}
//		}
//		break;
//	}
//}
//
//// 인자로 받은 함수와 칸에 적힌 pos들 보고 실제 연산 수행해서 해당 칸의 val에 기록
//void calc(int toR, int toC) {
//	int ret = 0;
//	int toCmd = map[toR][toC].cmd;
//	if (toCmd == 0) return;
//	POS from1 = map[toR][toC].from1;
//	POS from2 = map[toR][toC].from2;
//	int m;
//	switch (toCmd) {
//	case 1:
//		ret = map[from1.r][from1.c].val + map[from2.r][from2.c].val;
//		break;
//	case 2:
//		ret = map[from1.r][from1.c].val - map[from2.r][from2.c].val;
//		break;
//	case 3:
//		ret = map[from1.r][from1.c].val * map[from2.r][from2.c].val;
//		break;
//	case 4:
//		ret = map[from1.r][from1.c].val / map[from2.r][from2.c].val;
//		break;
//	case 5:
//		m = MAX_VAL;
//		if (from1.c == from2.c) {
//			for (int i = from1.r; i <= from2.r; i++) {
//				m = MAX(m, map[i][from1.c].val);
//			}
//		}
//		else if (from1.r == from2.r) {
//			for (int i = from1.c; i <= from2.c; i++) {
//				m = MAX(m, map[from1.r][i].val);
//			}
//		}
//		ret = m;
//		break;
//	case 6:
//		m = MIN_VAL;
//		if (from1.c == from2.c) {
//			for (int i = from1.r; i <= from2.r; i++) {
//				m = MIN(m, map[i][from1.c].val);
//			}
//		}
//		else if (from1.r == from2.r) {
//			for (int i = from1.c; i <= from2.c; i++) {
//				m = MIN(m, map[from1.r][i].val);
//			}
//		}
//		ret = m;
//		break;
//	case 7:
//		int sum = 0;
//		if (from1.c == from2.c) {
//			for (int i = from1.r; i <= from2.r; i++) {
//				sum += map[i][from1.c].val;
//			}
//		}
//		else if (from1.r == from2.r) {
//			for (int i = from1.c; i <= from2.c; i++) {
//				sum += map[from1.r][i].val;
//			}
//		}
//		ret = sum;
//		break;
//	}
//	map[toR][toC].val = ret;
//}
//
//// 해당 칸에게 직접적, 간접적으로 영향을 받는 모든 칸에 대해 연산 수행
//void calc_ref(int fromR, int fromC) {
//
//	// 직접적으로 영향을 받는 칸에 대해 연산 수행하기 위해
//	// 해당 칸의 참조관계 리스트를 큐에 삽입
//	// 즉, 해당 칸을 피연산자로 사용하는 칸들을 큐에 삽입
//	rear = 0;
//	REF* to = map[fromR][fromC].head.next;
//	while (to != NULL) {
//		q[rear++] = to;
//		to = to->next;
//	}
//
//	// 큐에서 하나씩 빼서 실제 연산 수행하고
//	// 해당 칸에게서 영향을 받는 칸들도 다시 큐에 삽입
//	// 즉 입력된 fromR, fromC 칸을 피연산자로 직접 사용하지 않더라도
//	// 간접적으로 영향을 받는 모든 칸에 대해서도 연산을 수행하는 것
//	front = 0;
//	while (front != rear) {
//		REF* from = q[front++];
//		calc(from->r, from->c);
//
//		REF* to = map[from->r][from->c].head.next;
//		while (to != NULL) {
//			q[rear++] = to;
//			to = to->next;
//		}
//	}
//}
//
//void init(int C, int R) {
//	cmax = C; rmax = R;
//	for (int i = 0; i < rmax; i++) {
//		for (int j = 0; j < cmax; j++) {
//			map[i][j].cmd = 0;			// 함수 없는 것으로 초기화
//			map[i][j].val = 0;			// 값은 0으로 초기화
//			map[i][j].head.next = NULL;	// 참조관계 초기화 (head는 더미, head의 next를 널로 초기화)
//		}
//	}
//}
//
//void set(int col, int row, char input[]) {
//	// 실제 col과 row는 0부터 시작
//	col--; row--;
//	
//	// 기존 칸이 가졌던 참조관계를 삭제
//	remove_ref(row, col);
//
//	int newCmd;
//
//	// 함수 입력
//	if (input[0] >= 'A' && input[0] <= 'Z') {
//		POS from1, from2;
//		from1 = parse_pos(4, input);
//		if (input[6] == ',') from2 = parse_pos(7, input);
//		else from2 = parse_pos(8, input);
//
//		if (input[2] == 'D') newCmd = 1;		// ADD
//		else if (input[2] == 'B') newCmd = 2;	// SUB
//		else if (input[2] == 'L') newCmd = 3;	// MUL
//		else if (input[2] == 'V') newCmd = 4;	// DIV
//		else if (input[2] == 'X') newCmd = 5;	// MAX
//		else if (input[2] == 'N') newCmd = 6;	// MIN
//		else newCmd = 7;	// SUM
//
//		map[row][col].from1 = from1;
//		map[row][col].from2 = from2;
//	}
//
//	// 값 입력
//	else {
//		newCmd = 0;
//		map[row][col].val = parse_val(input);
//	}
//
//	// 새로운 참조관계 생성
//	map[row][col].cmd = newCmd;
//	add_ref(row, col);
//
//	// 해당 칸 연산
//	calc(row, col);
//
//	// 해당 칸에게서 영향을 받는 칸들 연쇄적으로 연산
//	calc_ref(row, col);
//}
//
//void update(int value[MAXR][MAXC]) {
//	for (int i = 0; i < rmax; i++) {
//		for (int j = 0; j < cmax; j++) {
//			value[i][j] = map[i][j].val;
//		}
//	}
//}
