//#include <stdio.h>
//#define NODE_MAX 1000001
//#define HASH_MAX 10007
//#define MAX_LEN 6
//
//struct NODE {
//	bool isEnter;
//	char name[MAX_LEN];
//	NODE* next;
//};
//
//NODE nodePool[NODE_MAX];
//int nodePoolIdx= 0;
//
//NODE hashTable[HASH_MAX];
//
//NODE* allNode[NODE_MAX];
//int allNodeIdx = 0;
//
//NODE* ret[NODE_MAX];
//int retIdx = 0;
//
//NODE* bufferNode[NODE_MAX];
//
//void mstrcpy(char* dest, char* src) {
//	while (*src)* dest++ = *src++;
//	*dest = 0;
//}
//
//int mstrcmp(const char* a, const char* b) {
//	while (*a && *a == *b) {
//		++a; ++b;
//	}
//	return *a - *b;
//}
//
//unsigned long getHkey(const char* str) {
//	unsigned long hash = 5381;
//	int c;
//	while (c = *str++)  hash = ((hash << 5) + hash) + c;
//	return hash % HASH_MAX;
//}
//
//// 사전 순으로 정렬, 출력 시 거꾸로 출력할 것임
//void mergeSort(int start, int end) {
//	if (start < end) {
//		int mid = (start + end) / 2;
//		mergeSort(start, mid);
//		mergeSort(mid + 1, end);
//
//		int i = start;
//		int j = mid + 1;
//		int k = start;
//		while (i <= mid && j <= end) {
//			if (mstrcmp(ret[i]->name, ret[j]->name) <= 0) {
//				bufferNode[k++] = ret[i++];
//			}
//			else {
//				bufferNode[k++] = ret[j++];
//			}
//		}
//		while (i <= mid) bufferNode[k++] = ret[i++];
//		while (j <= end) bufferNode[k++] = ret[j++];
//
//		for (int l = start; l <= end; ++l) {
//			ret[l] = bufferNode[l];
//		}
//	}
//}
//
//void swapRet(int l, int r) {
//	NODE* tmp = ret[l];
//	ret[l] = ret[r];
//	ret[r] = tmp;
//}
//
//// 사전 순으로 정렬, 출력 시 거꾸로 출력할 것임
//void quickSort(int leftEnd, int rightEnd) {
//	int l = leftEnd;
//	int r = rightEnd;
//
//	NODE* pivot = ret[(l + r) / 2];
//
//	while (l <= r) {
//		while (mstrcmp(ret[l]->name, pivot->name) < 0) l++;
//		while (mstrcmp(pivot->name, ret[r]->name) < 0) r--;
//		if (l <= r) {
//			swapRet(l, r);
//			l++;
//			r--;
//		}
//		if (leftEnd < r) quickSort(leftEnd, r);
//		if (l < rightEnd) quickSort(l, rightEnd);
//	}
//}
//
//// TC가 1개라서 필요없음
//void init() {
//	nodePoolIdx = 0;
//	for (int i = 0; i < HASH_MAX; ++i) {
//		hashTable[i].next = 0;
//	}
//}
//
//int main() {
//
//	//init();	// TC가 1개라서 필요없음
//	int N;
//	scanf("%d", &N);
//	for (int n = 0; n < N; ++n) {
//		char name[MAX_LEN], isEnter[6];
//		scanf("%s %s", name, isEnter);
//
//		unsigned long hkey = getHkey(name);
//
//		// enter
//		if (isEnter[0] == 'e') {
//			bool hasNode = false;
//			// 동일한 이름의 사람이 hashTable에 있다면 isEnter를 true로 바꿈
//			for (NODE* node = hashTable[hkey].next; node; node = node->next) {
//				if (mstrcmp(node->name, name) == 0) {
//					hasNode = true;
//					node->isEnter = true;
//					break;
//				}
//			}
//			// 처음 enter하는 사람이라면 새로운 노드 생성
//			if (!hasNode) {
//				NODE* newNode = &nodePool[nodePoolIdx++];
//				mstrcpy(newNode->name, name);
//				newNode->isEnter = true;
//				newNode->next = hashTable[hkey].next;
//				hashTable[hkey].next = newNode;
//				// 한번이라도 언급된 이름은 모두 allNode에 추가
//				allNode[allNodeIdx++] = newNode;
//			}
//		}
//		// leave
//		// hashTable에서 이름 찾아서 isEnter를 false로 바꿈
//		else {	// if (isEnter[0] == 'l') {
//			for (NODE* node = hashTable[hkey].next; node; node = node->next) {
//				if (mstrcmp(node->name, name) == 0) {
//					node->isEnter = false;
//					break;
//				}
//			}
//		}
//	}
//	// 모든 노드 담겨있는 배열에서 isEnter상태의 노드만 enterNode에 추가
//	for (int i = 0; i < allNodeIdx; ++i) {
//		if (allNode[i]->isEnter) ret[retIdx++] = allNode[i];
//	}
//	// enterNode에 대해 정렬 수행
//	mergeSort(0, retIdx - 1);
//	//quickSort(0, retIdx - 1);
//
//	// 출력
//	for (int i = retIdx - 1; i >= 0; i--) {
//		printf("%s\n", ret[i]->name);
//	}
//}