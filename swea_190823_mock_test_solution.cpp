//#include <malloc.h>
//
//typedef enum
//{
//	NAME,
//	NUMBER,
//	BIRTHDAY,
//	EMAIL,
//	MEMO
//} FIELD;
//
//typedef struct
//{
//	int count;
//	char str[20];
//} RESULT;
//
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//
//// 테이블에 들어올 수 있는 최대 레코드 개수는 5만이지만
//// 레코드에게 메모리 할당할 레코드의 Pool인 Record 배열의 크기는 10만으로 잡았다
//// 삽입 삭제가 빈번히 발생하는 경우, 중간에 삭제되어 빵꾸난 슬롯을 회수하지 않으므로, 인덱스가 5만을 벗어날 수 있기 때문이다.
//#define MAX_N 100000
//#define FIELD_MAX 5
//
//// 해쉬테이블의 크기는 통상 나올 수 있는 값의 가짓수의 3배 정도가 적절
//// 3배까지 충돌확률과 접근 소요 시간이 급격히 떨어짐
//#define HASH_MAX (MAX_N + 7)
//
//struct RECORD {
//	char str[FIELD_MAX][20];
//};
//
//struct NODE {
//	NODE* next[FIELD_MAX];	// 각 필드별 next와 prev에 위치한 노드
//	NODE* prev[FIELD_MAX];	// 각 필드별 next와 prev에 위치한 노드
//	RECORD* record;
//};
//
//NODE HashTable[HASH_MAX];
//
//unsigned long getHashCode(const char* str)
//{
//	unsigned long hash = 5381;
//	int c;
//
//	while (c = *str++) {
//		hash = (((hash << 5) + hash) + c) % HASH_MAX;	// 33진법으로 변환
//	}
//
//	return hash % HASH_MAX;
//}
//
//////////////////////////////////////////////////////////////////////////////////
//
//void mstrcpy(char *dest, const char *src)
//{
//	int i = 0;
//	while (src[i] != 0) { dest[i] = src[i]; i++; }
//	dest[i] = src[i];
//}
//
//int mstrcmp(const char *a, const char *b)
//{
//	int i;
//	for (i = 0; a[i] != 0; i++) { if (a[i] != b[i]) return a[i] - b[i]; }
//	return a[i] - b[i];
//}
//
//////////////////////////////////////////////////////////////////////////////////
//
//RECORD Record[MAX_N];
//int RCnt;
//
//NODE Node[MAX_N];
//int NCnt;
//
//RECORD* newRecord()
//{
//	RECORD* record = &Record[RCnt++];
//	// 모든 필드값을 널문자로 초기화
//	for (int i = 0; i < FIELD_MAX; ++i) { record->str[i][0] = 0; }
//	return record;
//}
//
//NODE* newNode()
//{
//	NODE* node = &Node[NCnt++];
//	// 모든 필드에 대해 next와 prev 포인터를 널로 초기화
//	for (int i = 0; i < FIELD_MAX; ++i) {
//		node->next[i] = 0;
//		node->prev[i] = 0;
//	}
//	node->record = 0;
//	return node;
//}
//
//void deleteRecord(RECORD* record)
//{
//}
//
//void deleteNode(NODE* node)
//{
//}
//
//////////////////////////////////////////////////////////////////////////////////
//
//void InitDB()
//{
//	// 새로운 TC 시작 전 메모리 해제 & 재할당 대신 Record, Node 카운터 0으로 초기화
//	// 실제 메모리 해제 & 재할당보다 빠름
//	RCnt = NCnt = 0;
//
//	for (int i = 0; i < HASH_MAX; ++i)			// 0~100007
//		for (int k = 0; k < FIELD_MAX; ++k) {	// 0~4 (필드 5개)
//			HashTable[i].next[k] = 0;			// 필드값을 널포인터로 초기화
//			HashTable[i].prev[k] = 0;
//		}
//}
//
//void addlinkedlist(NODE* node, int field)
//{
//	int hkey = getHashCode(node->record->str[field]);
//	// HashTable[hkey]는 더미노드, 실질적 첫 노드는 그 다음 노드
//	NODE* firstNode = HashTable[hkey].next[field];
//	// 더미가 아닌 실질적 첫 노드(더미의 next)를 newNode의 next로 지정
//	// 맨 앞 더미노드(HashTable[hkey])를 newNode의 prev로 지정
//	node->next[field] = firstNode;
//	node->prev[field] = &HashTable[hkey];
//	// 맨 앞 더미노드의 next인 head가 존재한다면 (0이 아니라면, 즉 널이 아니라면)
//	// head의 prev를 새로운 노드로 지정
//	if (firstNode != 0) firstNode->prev[field] = node;
//	// 맨앞 더미노드의 next를 새로운 노드로 지정
//	HashTable[hkey].next[field] = node;
//}
//
//void deletelinkedlist(NODE* node, int field)
//{
//	if (node->prev[field] != 0) node->prev[field]->next[field] = node->next[field];
//	if (node->next[field] != 0) node->next[field]->prev[field] = node->prev[field];
//}
//
//void Add(char* name, char* number, char* birthday, char* email, char* memo)
//{
//	// 새로운 레코드 생성
//	//RECORD* record = newRecord();
//	RECORD* newRecord = &Record[RCnt++];
//	mstrcpy(newRecord->str[0], name);
//	mstrcpy(newRecord->str[1], number);
//	mstrcpy(newRecord->str[2], birthday);
//	mstrcpy(newRecord->str[3], email);
//	mstrcpy(newRecord->str[4], memo);
//
//	// 새로운 노드 생성 후 새로 입력값 담은 레코드를 담음
//	//NODE* node = newNode();
//	NODE* newNode = &Node[NCnt++];
//	for (int i = 0; i < FIELD_MAX; ++i) {
//		newNode->next[i] = 0;
//		newNode->prev[i] = 0;
//	}
//	newNode->record = newRecord;
//
//	for (int field_idx = 0; field_idx < FIELD_MAX; ++field_idx) {
//		int hkey = getHashCode(newNode->record->str[field_idx]);
//		// HashTable[hkey]는 더미노드, 실질적 첫 노드는 그 다음 노드
//		NODE* firstNode = HashTable[hkey].next[field_idx];
//		// 더미가 아닌 실질적 첫 노드(더미의 next)를 newNode의 next로 지정
//		// 맨 앞 더미노드(HashTable[hkey])를 newNode의 prev로 지정
//		newNode->next[field_idx] = firstNode;
//		newNode->prev[field_idx] = &HashTable[hkey];
//		// 맨 앞 더미노드의 next인 head가 존재한다면 (0이 아니라면, 즉 널이 아니라면)
//		// head의 prev를 새로운 노드로 지정
//		if (firstNode != 0) { firstNode->prev[field_idx] = newNode; }
//		// 맨앞 더미노드의 next를 새로운 노드로 지정
//		HashTable[hkey].next[field_idx] = newNode;
//	}
//}
//
//int Delete(FIELD field, char* str)
//{
//	int ret = 0;
//	int hkey = getHashCode(str);
//	// 실질적 첫 노드(맨 앞 더미노드의 next)부터 next로 이동하면서 노드가 존재하는 동안 반복
//	NODE* node = HashTable[hkey].next[field];
//	while (node) {
//		NODE* next = node->next[field];
//		// 삭제할 노드 발견
//		if (!mstrcmp(str, node->record->str[field])) {
//			ret++;
//			// 삭제할 노드의 모든 필드에 대해 노드 앞뒤(prev, next) 포인터값 조정
//			for (int field_idx = 0; field_idx < FIELD_MAX; ++field_idx) {
//				if (node->prev[field_idx] != 0) node->prev[field_idx]->next[field_idx] = node->next[field_idx];
//				if (node->next[field_idx] != 0) node->next[field_idx]->prev[field_idx] = node->prev[field_idx];
//			}
//			
//			// 실제로는 레코드와 노드에 할당된 메모리를 해제하지 않는다
//			// 애초에 malloc이 아니라 고정된 크기의 배열을 한칸씩 떼다 썼기 때문
//			// 즉 NODE 타입 배열인 Node[]와 RECORD 타입 배열인 Record[]를 사용했기 때문
//			//deleteRecord(node->record);
//			//deleteNode(node);
//		}
//		node = next;
//	}
//	return ret;
//}
//
//// 실제로는 변경이 아니라 삭제 후 추가하는 형식
//// 즉, 변경할 노드의 값을 수정해서 복제해놓고
//// 기존 변경 전 노드를 삭제하고 변경 후 노드를 추가하는 방식
//int Change(FIELD field, char* str, FIELD changefield, char* changestr)
//{
//	int ret = 0;
//	int hkey = getHashCode(str);
//
//	NODE* node = HashTable[hkey].next[field];
//	while (node) {
//		NODE* next = node->next[field];
//		// 변경할 노드 발견
//		if (!mstrcmp(str, node->record->str[field])) {
//			ret++;
//			// 변경할 노드 좌우의 포인터 연결 끊기
//			if (node->prev[changefield] != 0)
//				node->prev[changefield]->next[changefield] = node->next[changefield];
//			if (node->next[changefield] != 0)
//				node->next[changefield]->prev[changefield] = node->prev[changefield];
//			// 필드값 변경 - mstrcpy(char *dest, const char *src)
//			mstrcpy(node->record->str[changefield], changestr);
//			// 변경한 노드를 새롭게 추가
//			addlinkedlist(node, changefield);
//		}
//		node = next;
//	}
//	return ret;
//}
//
//RESULT Search(FIELD field, char* str, FIELD ret_field)
//{
//	// RESULT의 멤버 : int count, char str[20];
//	RESULT result;
//	result.count = 0;
//
//	int hkey = getHashCode(str);
//	// 실질적 첫 노드(맨 앞 더미노드의 next)부터 next로 이동하면서 노드가 존재하는 동안 반복
//	NODE* node = HashTable[hkey].next[field];
//	while (node) {
//		NODE* next = node->next[field];
//		// str과 일치하는 필드값을 가지는 노드 발견
//		if (!mstrcmp(str, node->record->str[field])) {
//			result.count++;
//			// 해당 노드의 ret_field의 필드값을 result에 복제
//			mstrcpy(result.str, node->record->str[ret_field]);
//		}
//		node = next;
//	}
//	return result;
//}