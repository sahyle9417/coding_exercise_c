//#include <stdio.h>
//#define NODEMAX 10001
//int parent[NODEMAX];
//int rc[NODEMAX];
//int lc[NODEMAX];
//int nodeCount;
//
//// start1과 start2의 공통 조상 찾기
//int getNodeNum(int start1, int start2) {
//	int lCount = 0;
//	int rCount = 0;
//	int leftArr[NODEMAX];
//	int rightArr[NODEMAX];
//	// 가까운 부모부터 위로 올라가며 조상들 배열에 기록
//	while (parent[start1]) {
//		leftArr[lCount++] = parent[start1];
//		start1 = parent[start1];
//	}
//	while (parent[start2]) {
//		rightArr[rCount++] = parent[start2];
//		start2 = parent[start2];
//	}
//	// 가까운 부모부터 위로 올라가며 공통조상 발견하자마자 stop
//	for (int i = 0; i < lCount; i++) {
//		for (int j = 0; j < rCount; j++) {
//			if (leftArr[i] == rightArr[j]) {
//				return leftArr[i];
//			}
//		}
//	}
//	return 0;
//}
//
//void getCount(int root) {
//	if (root == 0) {
//		return;
//	}
//	getCount(lc[root]);
//	nodeCount++;
//	getCount(rc[root]);
//}
//
//void dfs(int root) {
//	int stack[NODEMAX];
//	int top = 0;
//	stack[top++] = root;
//	int u;
//	while (top) {		// stack이 빌때까지
//		u = stack[--top];
//		nodeCount++;
//		if (lc[u]) { stack[top++] = lc[u]; }
//		if (rc[u]) { stack[top++] = rc[u]; }
//	}
//}
//
//
//
//int main() {
//	freopen("input.txt", "r", stdin);
//	int V, E;
//	int tc;
//	int pTemp, cTemp;
//	int start1, start2, nodeNum;
//	scanf("%d", &tc);
//	for (int testCase = 1; testCase <= tc; testCase++) {
//		nodeCount = 0;
//		scanf("%d%d", &V, &E);
//		scanf("%d%d", &start1, &start2);
//
//		for (int i = 1; i <= V; i++) {
//			parent[i] = 0;	// i번째 노드의 부모
//			rc[i] = 0;			// i번째 노드의 우측 자식
//			lc[i] = 0;			// i번째 노드의 좌측 자식
//		}
//
//		for (int i = 0; i < E; i++) {
//			scanf("%d%d", &pTemp, &cTemp);
//			if (lc[pTemp]) { rc[pTemp] = cTemp; }	// 좌측 자식 있다면 우측 자식으로 추가
//			else { lc[pTemp] = cTemp; }					// 좌측 자식 없다면 우측 자식으로 추가
//			parent[cTemp] = pTemp;					// 자식의 부모를 기록
//		}
//		nodeNum = getNodeNum(start1, start2);	// 공통부모 찾기
//		//getCount(nodeNum);
//		dfs(nodeNum);
//
//		printf("#%d %d %d\n", testCase, nodeNum, nodeCount);
//	}
//
//	return 0;
//}
