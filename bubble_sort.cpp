//#include <stdio.h>
//
//// void main 사용 금지 (오류 발생), int main 사용
//// 로컬(MS)에서는 gets_s 사용, 서버 제출용으로는 gets 사용, 표준 코드 (scanf?) 사용 시에는 문제 없음
//// pro 시험에서는 merge 정렬, quick 정렬만 사용
//
//int src[] = { 2,3,1,6,4,5,8,7,9 };
//int n = 9;
//
////void selectSort(int size) {
////	for (int end = size - 1; end > 0; end--) {
////		for (int idx = 0; idx < end; idx++) {
////			if (src[idx] > src[end]) {
////				int tmp = src[idx];
////				src[idx] = src[end];
////				src[end] = tmp;
////			}
////		}
////	}
////	for (int i = 0; i < n; i++) {
////		printf("%d ", src[i]);
////	}
////}
//
//void selectSort(int size) {
//	int iLen = size - 1;
//	for (int i = 0; i < iLen; i++) {
//		int maxIdx = 0;
//		int jLen = size - i;
//		for (int j = 1; j < jLen; j++) {
//			if (src[j] > src[maxIdx]) {
//				maxIdx = j;
//			}
//		}
//		int tmp = src[maxIdx];
//		src[maxIdx] = src[jLen-1];
//		src[jLen-1] = tmp;
//	}
//	for (int i = 0; i < n; i++) {
//		printf("%d ", src[i]);
//	}
//}
//
//void bubbleSort(int size) {
//	int iLen = size - 1;
//	for (int i = 0; i < iLen; i++) {
//		int jLen = size - i - 1;
//		for (int j = 0; j < jLen; j++) {
//			if (src[j] > src[j+1]) {
//				int tmp = src[j];
//				src[j] = src[j+1];
//				src[j+1] = tmp;
//			}
//		}
//	}
//	for (int i = 0; i < n; i++) {
//		printf("%d ", src[i]);
//	}
//}
//
//int main() {
//	printf("%d\n", sizeof(src) / sizeof(src[0]));
//	//selectSort(sizeof(src) / sizeof(src[0]));
//	bubbleSort(sizeof(src) / sizeof(src[0]));
//	return 0;
//}
//
