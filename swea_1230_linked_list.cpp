//#include <stdio.h>
//#include <malloc.h>
//
//typedef struct node {
//	struct node* prev;
//	struct node* next;
//	int data;
//} Node;
//
//typedef struct {
//	Node* head;
//	Node* tail;
//	Node* cur;
//	int num;
//} List;
//
//void ListInit(List* list) {
//
//	list->head = (Node*)malloc(sizeof(Node));
//	list->tail = (Node*)malloc(sizeof(Node));
//
//	list->head->prev = NULL;
//	list->head->next = list->tail;
//
//	list->tail->prev = list->head;
//	list->tail->next = NULL;
//
//	list->num = 0;
//}
//
//// 끝에 노드 추가, 새로운 노드가 cur로 지정됨
//void ListAppend(List* list, int newData) {
//	Node* newNode = (Node*)malloc(sizeof(Node));
//	newNode->data = newData;
//
//	newNode->prev = list->tail->prev;
//	newNode->next = list->tail;
//	list->tail->prev->next = newNode;
//	list->tail->prev = newNode;
//
//	list->cur = newNode;
//	list->num++;
//}
//
//// cur노드 뒤에 노드 추가, 새로운 노드가 cur로 지정됨
//void ListInsert(List* list, int data) {
//	Node * newNode = (Node*)malloc(sizeof(Node));
//	newNode->data = data;
//
//	newNode->prev = list->cur;
//	newNode->next = list->cur->next;
//	list->cur->next->prev = newNode;
//	list->cur->next = newNode;
//
//	list->cur = newNode;
//	list->num++;
//}
//
//void ListFirst(List* list) {
//	list->cur = list->head->next;
//}
//
//void ListLast(List* list) {
//	list->cur = list->tail->prev;
//}
//
//// 다음 노드에 값이 있다면 다음 노드를 cur로 지정
//// 다음 노드가 tail이면 처음 노드를 cur로 지정
//void ListForward(List* list) {
//	if (list->cur->next == list->tail) list->cur = list->head->next;
//	else list->cur = list->cur->next;
//}
//
//// 이전 노드에 값이 있다면 이전 노드를 cur로 지정
//// 이전 노드가 head면 마지막 노드를 cur로 지정
//bool ListBackward(List* list) {
//	if (list->cur->prev == list->head) list->cur = list->tail->prev;
//	else list->cur = list->cur->prev;
//	return true;
//}
//
//// 현재 cur이 가리키는 노드 삭제
//int ListRemove(List* list) {
//	Node* rnode = list->cur;
//	int rdata = rnode->data;
//	rnode->prev->next = rnode->next;
//	rnode->next->prev = rnode->prev;
//	// 삭제 후 삭제된 노드 다음꺼를 가르켜야 순차적으로 삭제되는데
//	// 실수로 삭제된 노드 앞에꺼를 가리켜서 이상하게 삭제되었다.
//	// 특히 삭제되다가 앞에 head를 건드려서 실행 중 뻗어버리는 현상 있었다.
//	// 게다가 원인도 뜨지 않고 콘솔이 죽어버려서 찾는게 애를 먹었다.
//	list->cur = rnode->next;
//	free(rnode);
//	list->num--;
//	return rdata;
//}
//
//void freememory(List* list) {
//	Node* node = list->head->next;
//	while (node != list->tail) {
//		free(node->prev);
//		node = node->next;
//	}
//	free(list->tail);
//	free(list);
//}
//
//int main() {
//
//	freopen("input.txt", "r", stdin);
//
//	for (int tc = 1; tc <= 10; tc++) {
//
//		int length;
//		scanf("%d ", &length);
//
//		List* mylist = (List*)malloc(sizeof(List));
//		ListInit(mylist);
//
//		int tmp;
//		for (int i = 0; i < length; i++) {
//			scanf("%d ", &tmp);
//			ListAppend(mylist, tmp);
//		}
//
//		int command_num;
//		scanf("%d ", &command_num);
//		char command;
//		for (int cn = 1; cn <= command_num; cn++) {
//			scanf("%c ", &command);
//			if (command == 'I') {
//				int pos;
//				scanf("%d ", &pos);
//				// pos번째 노드를 찾기 위해 순차적으로 탐색하는 과정이 쉽지 않았음
//				// 특히 시작 노드 설정이 쉽지 않았다.
//				mylist->cur = mylist->head;
//				for (int p = 0; p < pos; p++) {
//					mylist->cur = mylist->cur->next;
//				}
//				int i_count;
//				scanf("%d ", &i_count);
//				for (int ic = 1; ic <= i_count; ic++) {
//					scanf("%d ", &tmp);
//					ListInsert(mylist, tmp);
//				}
//			}
//			else if (command == 'D') {
//				int pos;
//				scanf("%d ", &pos);
//				mylist->cur = mylist->head;
//				for (int p = 0; p <= pos; p++) {
//					mylist->cur = mylist->cur->next;
//				}
//				int d_count;
//				scanf("%d ", &d_count);
//				for (int dc = 0; dc < d_count; dc++) {
//					ListRemove(mylist);
//				}
//			}
//			else if (command == 'A') {
//				int a_count;
//				scanf("%d ", &a_count);
//				for (int ac = 0; ac < a_count; ac++) {
//					scanf("%d ", &tmp);
//					ListAppend(mylist, tmp);
//				}
//			}
//			//printf("finish\n");
//		}
//
//		printf("#%d ", tc);
//		mylist->cur = mylist->head;
//		for (int i = 1; i <= 10; i++) {
//			mylist->cur = mylist->cur->next;
//			printf("%d ", mylist->cur->data);
//		}
//		printf("\n");
//		freememory(mylist);
//	}
//}