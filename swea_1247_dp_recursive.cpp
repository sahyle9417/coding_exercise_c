//#include <iostream>
//#include <algorithm>
//#include <memory.h>
//using namespace std;
//
//struct po {
//	int x, y;
//};
//
//int t, n, full_bitmask;
//int d[11][1 << 11];		// d 배열은 현재 집위치와 현재 비트마스크 상태에서 남은 집을 모두 순회하고 도착지까지 도달하는데 필요한 최단 거리 기록
//int dist[12][12];			// 시작지점(i)에서 도착지점(j)까지의 거리 (경유없이 직행하는 경우의 거리)
//po a[12];
//
//// cur은 현재 위치 (즉 현재 bitmask에 1로 되어있는 집들 중 마지막으로 방문한 집의 인덱스)
//// bitmask는 현재까지 방문한 집들을 비트로 표현 (1이면 방문), visited 배열과 동일한 역할 수행
//// bitmask는 상위비트(msb)가 n번째집 하위비트(lsb)가 1번째집
//// 재귀적으로 dp 함수 호출해서 새로운 집을 방문하면서 bitmask 채워나감
//// bitmask가 다 채워지면 dp함수를 하나씩 빠져나가면서 누적된 거리를 d 배열에 기록(메모이제이션)
//// d 배열은 현재 집위치와 현재 비트마스크 상태에서 남은 집을 모두 순회하고 도착지까지 도달하는데 필요한 최단 거리 기록
//int dp(int cur, int bitmask) {
//	int &ret = d[cur][bitmask];	// 가독성을 위해 d[cur][bitmask]에 ret라는 새로운 이름(reference) 부여
//	if (ret != -1) return ret;		// 이미 계산했던 것은 재계산X (메모이제이션), 이 밑으로는 실제 값 계산
//	if (bitmask == full_bitmask) {			// full_bitmask : 1이 연속 n개, 모든 집 방문했음, 현재 집이 마지막(n번째 방문) 집
//		return dist[cur][n + 1];	// 마지막으로 방문한 집(현재위치, cur)에서 도착지(n+1)까지의 거리 반환
//	}									// 이제부터 재귀 빠져나가면서 dist 더해서 d에 메모이제이션 수행
//	// 이 밑으로는 메모이제이션도 안 되어 있고 마지막 집도 아니므로 재귀호출 수행
//	ret = 2e9;						// -1이 씌여있던 d[cur][bitmask]에 최단거리 기록하기 위해 매우 큰 값을 임시로 부여
//	for (int i = 1; i <= n; ++i) {
//		if (bitmask&(1 << i)) continue;			// bitmask에 i번째 bit가 0인 집(방문하지 않았던 집)만 방문
//		// dp(i, bitmask | (1 << i)) : 현재위치 i와 bitmask의 i번째 비트를 1로 바꾼 것을 dp로 넘김
//		// dp(i, bitmask | (1 << i)) + dist[cur][i]) : dp 결과에 현재 위치에서 i번째 집까지의 거리(dist[cur][i]) 더함
//		// dp(i, bitmask | (1 << i)) + dist[cur][i])와 ret 중 작은 값을 ret에 기록
//		ret = min(ret, dp(i, bitmask | (1 << i)) + dist[cur][i]);	// i번째 집에서 남은 비트마스크 채우고 도착점까지 도달하는데 드는 최단 거리 반환 받음
//	}
//
//	return ret;
//}
//
//void print() {
//	for (int i = 0; i < 11; i++) {
//		for (int j = 0; j < 1024; j++) {
//			if (d[i][j] != -1) {
//				printf("d[%d][", i);
//				for (int i = n; i >= 0; i--) {
//					int mask_tmp = 1 << i;
//					printf("%d", j & mask_tmp ? 1 : 0);
//				}
//				printf("] = %d\n", d[i][j]);
//			}
//		}
//	}
//}
//
//int main() {
//	ios::sync_with_stdio(false); cin.tie(0);
//	freopen("input.txt", "r", stdin);
//	cin >> t;
//	for (int tc = 1; tc <= t; ++tc) {
//		memset(d, -1, sizeof(d));
//		cin >> n;
//		int ans = 2e9;
//		full_bitmask = (1 << (n + 1)) - 1;	// full_bitmask : 1이 연속 n개 (시작과 끝은 고정이니 비트 사용X), n=5일때 e=63
//
//		// 시작점은 0번 인덱스에 저장, 도착점은 n+1번째 인덱스의 저장
//		// 나머지 방문해야할 n개의 집은 그 사이 1~n번 인덱스에 저장
//		cin >> a[0].x >> a[0].y >> a[n + 1].x >> a[n + 1].y;
//		for (int i = 1; i <= n; ++i)
//			cin >> a[i].x >> a[i].y;
//		for (int i = 0; i < n + 2; ++i) for (int j = 0; j < n + 2; ++j)
//			dist[i][j] = abs(a[i].x - a[j].x) + abs(a[i].y - a[j].y);	// 시작지점(i)에서 도착지점(j)까지의 거리 (경유없이 직행하는 경우의 거리)
//
//		/*printf("=======dist=======\n");
//		for (int i = 0; i < 12; i++) {
//			for (int j = 0; j < 12; j++) {
//				printf("%3d ", dist[i][j]);
//			}
//			puts("");
//		}*/
//		cout << "#" << tc << " " << dp(0, 1) << '\n';
//
//		//print();
//	}
//	return 0;
//}
