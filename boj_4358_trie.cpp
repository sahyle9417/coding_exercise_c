//#include <stdio.h>
//#define MAX_LEN 31
//#define MAX_DIC 10001		// 10만종
//#define MAX_TRIE 300001	// 30글자 * 10만종
//#define CHILD_NUM ('z' - ' ')
//
//typedef struct _dic_node {
//	char name[MAX_LEN];
//	int num;
//} DIC_NODE;
//
//typedef struct _trie_node {
//	_trie_node* child[CHILD_NUM];
//	_dic_node* dicNode;
//} TRIE_NODE;
//
//TRIE_NODE trieNodePool[MAX_TRIE];
//int trieNodePoolIdx = 1;
//
//DIC_NODE dicNodePool[MAX_DIC];
//int dicNodePoolIdx = 0;
//
//DIC_NODE* ret[MAX_DIC];
//int retIdx = 0;
//
//void init() {
//	for (int i = 0; i < trieNodePoolIdx; i++) {
//		for (int j = 0; j < CHILD_NUM; j++) {
//			trieNodePool[i].child[j] = 0;
//		}
//	}
//	trieNodePoolIdx = 1;
//	dicNodePoolIdx = 0;
//	retIdx = 0;
//}
//
//void mstrcpy(char* dst, char* src) {
//	while (*dst++ = *src++);
//}
//
//void insert(char name[MAX_LEN]) {
//	TRIE_NODE* trieNode = &trieNodePool[0];
//	for (int i = 0; name[i]; i++) {
//		int childIdx = name[i] - ' ';
//		if (!trieNode->child[childIdx]) {
//			trieNode->child[childIdx] = &trieNodePool[trieNodePoolIdx++];
//		}
//		trieNode = trieNode->child[childIdx];
//	}
//	
//	if (!trieNode->dicNode) {
//		DIC_NODE* dicNode = &dicNodePool[dicNodePoolIdx++];
//		mstrcpy(dicNode->name, name);
//		dicNode->num = 0;
//		trieNode->dicNode = dicNode;
//		ret[retIdx++] = dicNode;
//	}
//	trieNode->dicNode->num++;
//}
//
//
//int mstrcmp(char* a, char* b) {
//	int i = 0;
//	while (a[i]) {
//		if (a[i] != b[i]) return a[i] - b[i];
//		i++;
//	}
//	return a[i] - b[i];
//}
//
//void swap(int i, int j) {
//	DIC_NODE* tmp = ret[i];
//	ret[i] = ret[j];
//	ret[j] = tmp;
//}
//
//void quickSort(int leftEnd, int rightEnd) {
//	int l = leftEnd;
//	int r = rightEnd;
//	int pivot = (l + r) / 2;
//	while (l <= r) {
//		while (mstrcmp(ret[l]->name, ret[pivot]->name) < 0) l++;
//		while (mstrcmp(ret[pivot]->name, ret[r]->name) < 0) r--;
//		if (l <= r) {
//			swap(l, r);
//			l++;
//			r--;
//		}
//		if (leftEnd < r) quickSort(leftEnd, r);
//		if (l < rightEnd) quickSort(l, rightEnd);
//	}
//}
//
//DIC_NODE* bufferNode[MAX_DIC];
//void mergeSort(int start, int end) {
//	if (start < end) {
//		int mid = (start + end) / 2;
//		mergeSort(start, mid);
//		mergeSort(mid + 1, end);
//
//		int i = start;
//		int j = mid + 1;
//		int k = start;
//		while (i <= mid && j <= end) {
//			if (mstrcmp(ret[i]->name, ret[j]->name) <= 0)
//				bufferNode[k++] = ret[i++];
//			else
//				bufferNode[k++] = ret[j++];
//		}
//		while (i <= mid) bufferNode[k++] = ret[i++];
//		while (j <= end) bufferNode[k++] = ret[j++];
//
//		for (int l = start; l <= end; ++l) 
//			ret[l] = bufferNode[l];
//	}
//}
//
//int main() {
//	
//	//init();
//	
//	char name[MAX_LEN];
//	int totalNum = 0;
//
//	while (scanf(" %[^\n]", name) != EOF) {
//		insert(name);
//		totalNum++;
//	}
//
//	//quickSort(0, retIdx - 1);
//	mergeSort(0, retIdx - 1);
//
//	for (int i = 0; i < retIdx; i++) {
//		printf("%s %.4lf\n", ret[i]->name, (double)ret[i]->num / totalNum * 100);
//	}
//}