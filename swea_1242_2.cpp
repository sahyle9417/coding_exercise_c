//#include <stdio.h>
//#include <malloc.h>
//
///*
// 역순으로 표기
//{1, 1, 2, 3},	//0
//{1, 2, 2, 2},	//1
//{2, 2, 1, 2},	//2
//{1, 1, 4, 1},	//3
//{2, 3, 1, 1}	//4
//{1, 3, 2, 1}	//5
//{4, 1, 1, 1}	//6
//{2, 1, 3, 1}	//7
//{3, 1, 2, 1}	//8
//{2, 1, 1, 3}	//9
//A : 65
//0 : 48
//*/
//
//int parse[10][4] = { {3, 2, 1, 1}, {2, 2, 2, 1}, {2, 1, 2, 2}, {1, 4, 1, 1}, {1, 1, 3, 2}, {1, 2, 3, 1}, {1, 1, 1, 4}, {1, 3, 1, 2}, {1, 2, 1, 3}, {3, 1, 1, 2} };
//
//bool make_32_to_8 (int* output32, int* output8, int length_multiplier) {
//	if (length_multiplier > 1) {
//		for (int i = 0; i < 32; i++) {
//			output32[i] /= length_multiplier;
//		}
//	}
//	for (int i = 1; i < 8; i++) {			// 최종 output은 8자
//		for (int j = 0; j < 10; j++) {		// 하나의 digit은 10가지 경우의 수
//			bool match = true;
//			for (int k = 0; k < 4; k++) {	// 하나의 digit은 4가지 세부 숫자로 구성
//				if (output32[4 * i + k] != parse[j][k]) {
//					match = false;
//					break;
//				}
//			}
//			if (match) {
//				output8[i] = j;
//				break;
//			}
//		}
//		if (output8[i] == -1) {
//			return false;
//		}
//	}
//	for (int j = 0; j < 10; j++) {
//		bool match = true;
//		for (int k = 1; k < 4; k++) {
//			if (output32[k] != parse[j][k]) match = false;
//		}
//		if (match && output32[0] >= parse[j][0]) {
//			output8[0] = j;
//			break;
//		}
//	}
//	if (output8[0] != -1) return true;
//	else return false;
//}
//
//int verify(int* output8) {
//	int code = (output8[0] + output8[2] + output8[4] + output8[6]) * 3 + output8[1] + output8[3] + output8[5] + output8[7];
//	if (code % 10) return -1;
//	else return output8[0] + output8[1] + output8[2] + output8[3] + output8[4] + output8[5] + output8[6] + output8[7];
//}
//
//
//int main() {
//	freopen("sample_input.txt", "r", stdin);
//
//	int TC;
//	scanf("%d ", &TC);
//	for (int tc = 1; tc <= TC; tc++) {
//
//		int answer = 0;
//
//		int N, M;
//		scanf("%d %d", &N, &M);
//
//		char ** hex_arr = (char**)malloc(sizeof(char*)*N);
//		for (int i = 0; i < N; i++) {
//			hex_arr[i] = (char*)malloc(sizeof(char)*(M + 1));
//			scanf("%s", hex_arr[i]);
//		}
//
//		bool ** bin_arr = (bool**)malloc(sizeof(bool*)*N);
//		for (int i = 0; i < N; i++) {
//			bin_arr[i] = (bool*)malloc(sizeof(bool)*(4*M));
//			for (int j = 0; j < M; j++) {
//				char hex = hex_arr[i][j];
//				int dec;
//				// +10 빼먹어서 한참 고생
//				if (hex >= 'A') dec = hex - 'A' + 10;
//				else dec = hex - '0';
//				for (int k = 1; k <= 4; k++) {
//					bin_arr[i][(j + 1) * 4 - k] = (dec % 2 == 1);
//					dec /= 2;
//				}
//			}
//		}
//
//		/*for (int i = 0; i < N; i++) {
//			for (int j = 0; j < 4*M; j++) {
//				printf("%d", bin_arr[i][j]);
//			}
//			puts("");
//		}*/
//		int i = 0;
//		while (i < N) {
//
//			int j = 4 * M - 1;
//			while (j >= 0) {
//
//				// 비트 1 발견
//				if (bin_arr[i][j]) {
//
//					int start_i = i;
//					int start_j = j;
//
//					// 구간 32개의 길이 기록
//					int output32[32];
//					for (int oi = 31; oi >= 0; oi--) {
//						output32[oi] = 1;
//						// 0<->1 바뀌기 전까지가 하나의 구간의 길이
//						while (bin_arr[i][j] == bin_arr[i][j - 1]) {
//							output32[oi]++;
//							j--;
//						}
//						j--;
//					}
//					/*for (int oi = 0; oi < 32; oi++) {
//						printf("%d ", output32[oi]);
//					}
//					puts("");*/
//					// 암호코드 한글자의 길이가 7의 몇배인지 기록, M<=500이니까 이 값은 10이상이 될 수 없음
//					int length_multiplier = 10; 
//					for (int i = 31; i >= 28; i--) {
//						if (output32[i] < length_multiplier) length_multiplier = output32[i];
//					}
//
//					int output8[8] = { -1, -1, -1, -1, -1, -1, -1, -1};
//
//					if (make_32_to_8(output32, output8, length_multiplier)) {
//						/*for (int i = 0; i < 8; i++) {
//							printf("%d ", output8[i]);
//						}
//						puts(" ");*/
//						int verify_result = verify(output8);
//						//printf("%d\n", verify_result);
//						if (verify_result != -1) answer += verify_result;
//						int area_height = 5;
//						while (bin_arr[start_i + area_height - 1][start_j] == bin_arr[start_i + area_height][start_j]) area_height++;
//						//printf("start:[%d][%d], area_height:%d, mulitplier:%d\n", start_i, start_j, area_height, length_multiplier);
//						for (int tmp_i = 0; tmp_i < area_height; tmp_i++) {
//							for (int tmp_j = 0; tmp_j < length_multiplier * 7 * 8; tmp_j++) {
//								bin_arr[start_i + tmp_i][start_j - tmp_j] = false;
//							}
//						}
//					}
//				}
//				// 비트 0 스킵
//				j--;
//			}
//			i++;
//		}
//		printf("#%d %d\n", tc, answer);
//	}
//}