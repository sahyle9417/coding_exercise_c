//#include <stdio.h>
//#include <string.h>
//
//int main(void) {
//	char from[20]="1234567890";
//	char to1[20];
//	char to2[5];
//
//	strcpy(to1, from);
//	puts(to1);
//
//	// strncpy는 단순히 sizeof(to2)만큼만 복사할 뿐 널문자 첨가해주지 않는다
//	// 아래와 같이 널문자 처리를 직접 해주지 않으면 영역 너머까지 출력한다
//	strncpy(to2, from, sizeof(to2));
//	puts(to2);
//
//	// strncpy 사용 시 배열 실제 길이보다 한칸 적게 전달하고 널문자를 직접 추가한다
//	strncpy(to2, from, sizeof(to2)-1);
//	to2[sizeof(to2) - 1] = 0;
//	puts(to2);
//}