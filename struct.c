//#include <stdio.h>
//
//typedef struct {
//	char name[20];
//	int age;
//} Person;		// 'struct person 구조체변수명' 대신 'Person 구조체변수명' 사용 가능
//
//int main(void) {
//	Person p1 = { "이상현", 26 };
//	printf("name : %s, age : %d\n", p1.name, p1.age);
//
//	Person p_array[3] = { {"이상현", 26}, {"이상경", 24}, {"이상원", 19} };
//	for (int i = 0; i < 3; i++) {
//		printf("name : %s, age : %d\n", p_array[i].name, p_array[i].age);
//	}
//
//	strcpy(p_array[0].name, "이상윤");	// 문자열 멤버변수는 등호로 대입 불가
//	p_array[0].age = 2;
//	for (int i = 0; i < 3; i++) {
//		printf("name : %s, age : %d\n", p_array[i].name, p_array[i].age);
//	}
//}