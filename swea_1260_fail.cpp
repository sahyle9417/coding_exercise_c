//#include <stdio.h>
//#include <malloc.h>
//#define MAX_N 100
//#define MAX_HEAP 1
//
//bool visit[MAX_N + 1][MAX_N + 1];		// 우측과 하단에만 bound 필요
//int prev[MAX_N + 1];						// 인덱스 1부터 사용
//int next[MAX_N + 1];						// 인덱스 1부터 사용
//
//void print() {
//	printf("\nprev : ");
//	for (int i = 1; i <= MAX_N; i++) {
//		printf("%d ", prev[i]);
//	}
//	printf("\nnext : ");
//	for (int i = 1; i <= MAX_N; i++) {
//		printf("%d ", next[i]);
//	}
//	printf("\n");
//}
//
//int main() {
//	freopen("sample_input.txt", "r", stdin);
//
//	int TC;
//	scanf("%d", &TC);
//	for (int tc = 1; tc <= TC; tc++) {
//
//		int n;
//		scanf("%d", &n);
//
//		// visit 바운드 처리
//		// prev, next 초기화
//		for (int i = 0; i <= n; i++) {
//			visit[i][n] = visit[n][i] = true;
//			prev[i] = next[i] = 0;
//		}
//
//		int tmp;
//		for (int i = 0; i < n; i++) {
//			for (int j = 0; j < n; j++) {
//				scanf("%d", &tmp);
//				visit[i][j] = (tmp == 0);		// 입력값(tmp)가 0이면 더이상 방문할 필요 없으니 방문 처리(visit=true)
//			}
//		}
//
//		int row_num, col_num;
//		for (int i = 0; i < n; i++) {
//			for (int j = 0; j < n; j++) {
//				if (visit[i][j]) continue;
//				// 방문 안한 화학물질 칸 발견
//				row_num = 1;
//				col_num = 1;
//				while (!visit[i + row_num][j]) { row_num++; }
//				while (!visit[i][j + col_num]) { col_num++; }
//				for (int r = i; r < i + row_num; r++) {
//					for (int c = j; c < j + col_num; c++) {
//						visit[r][c] = true;
//					}
//				}
//				//printf("row:%d, col:%d\n", row_num, col_num);
//				prev[col_num] = row_num;
//				next[row_num] = col_num;
//			}
//		}
//
//
//		int min_cur, min_prevXnext;
//		int ans = 0;
//
//		while (true) {
//			min_prevXnext = MAX_N * MAX_N;
//			for (int i = 1; i <= n; i++) {
//				if (prev[i] == 0 || next[i] == 0) continue;
//				if (prev[i] * next[i] < min_prevXnext) {
//					min_prevXnext = prev[i] * next[i];
//					min_cur = i;
//				}
//			}
//			if (min_prevXnext == MAX_N * MAX_N) { break; }
//			ans += min_cur * min_prevXnext;
//			next[prev[min_cur]] = next[min_cur];
//			prev[next[min_cur]] = prev[min_cur];
//			next[min_cur] = 0;
//			prev[min_cur] = 0;
//		}
//		printf("#%d %d\n", tc, ans);
//	}
//}