//#include <stdio.h>
//
//int main(void) {
//	char str[7];	// 맨마지막에 널문자 들어가야하니 6자리까지만 입력 가능
//
//	puts("use gets()");
//	// gets : 개행만나기 전까지 입력받음, 공백 입력 가능, 개행 입력 불가
//	gets(str);
//	printf("길이 : %d\n", strlen(str));
//	puts(str);
//	
//	puts("use fgets()");
//	// fgets : 개행까지 입력받음(마지막 개행까지 입력됨), 공백 입력 가능, 개행 입력 가능
//	fgets(str, sizeof(str), stdin);
//	printf("길이 : %d\n", strlen(str)); 
//	puts(str);
//
//	puts("use scanf()");
//	// scanf : 개행 또는 공백 만나기 전까지 입력받음, 공백 입력 불가, 개행 입력 불가
//	scanf("%s", str);
//	printf("길이 : %d\n", strlen(str));
//	puts(str);
//}