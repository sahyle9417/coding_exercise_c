//// 중간에 스택의 top을 0으로 초기화하는 작업을 깜빡했다가 삽질했음, 주의 요망
//
//#include <stdio.h>
//#define MAX 8
//
//char input[MAX][MAX];
//
//int main() {
//	freopen("input.txt", "r", stdin);
//	for (int tc = 1; tc <= 10; tc++) {
//
//		int len;
//		scanf("%d ", &len);
//		for (int i = 0; i < MAX; i++) {
//			for (int j = 0; j < MAX; j++) {
//				scanf("%c ", &input[i][j]);
//			}
//		}
//
//		int answer = 0;
//		int front_end = (len / 2) - 1;
//		int rear_start;
//
//		if (len % 2 == 0) {
//			rear_start = len / 2;
//		}
//		else {
//			rear_start = (len / 2) + 1;
//		}
//
//		int stack[MAX / 2];
//		int top;
//
//		// 가로방향
//		for (int i = 0; i < MAX; i++) {
//			for (int j = 0; j <= MAX - len; j++) {
//				top = 0;
//				for (int k = 0; k <= front_end; k++) {
//					stack[top++] = input[i][j + k];
//				}
//				bool success = true;
//				for (int k = rear_start; k < len; k++) {
//					if (stack[--top] != input[i][j + k]) {
//						success = false;
//						break;
//					}
//				}
//				if (success) answer++;
//			}
//		}
//
//		// 세로방향
//		for (int j = 0; j < MAX; j++) {
//			for (int i = 0; i <= MAX - len; i++) {
//				top = 0;
//				for (int k = 0; k <= front_end; k++) {
//					stack[top++] = input[i + k][j];
//				}
//				bool success = true;
//				for (int k = rear_start; k < len; k++) {
//					if (stack[--top] != input[i + k][j]) {
//						success = false;
//						break;
//					}
//				}
//				if (success) answer++;
//			}
//		}
//		printf("#%d %d\n", tc, answer);
//	}
//}