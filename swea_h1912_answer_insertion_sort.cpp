//#define MAX_USER 1001		// 최대 사용자 수
//#define MAX_POST 100001	// 게시물 최대 개수
//#define MAX_RET 10		// 반환할 게시물 개수
//
//// 게시글에 대한 구조체만 만듦 (사용자 구조체X)
//typedef struct {
//	int timestamp;
//	int like;
//	int user;
//} POST;
//
//// pId(게시물id)를 인덱스로 사용해서 모든 게시물 저장 (사용자 구분X)
//// 1 <= timestamp, pId <= 100,000
//// 모든 게시물들의 timestamp는 서로 다르다.
//POST allPost[MAX_POST];
//
//// followNum[i] : i번째 사용자가 follow하는 사용자 수
//int followNum[MAX_USER];
//
//// followList[i] = {i, 1, 2, 3, ... };	//i번째 사용자가 follow하는 사용자들 리스트
//int followList[MAX_USER][MAX_USER];
//
//// 사용자 수 (2 <= globalN <= 1,000)
//int globalN;
//
//// uId번째 사용자 소유의 게시물의 pId를 myPost에 차례로 기록
//// myPost[uId] = {pId1, pId2, ... }
//// 분명 개별 사용자 소유의 게시물 개수의 상한이 주어지지 않았는데 해설에서 임의로 1만으로 지정했음 이상한 듯
//int myPost[MAX_USER][MAX_POST/10];
//
//// postNum[i] : i번재 사용자의 게시물(post) 개수
//int postNum[MAX_USER];
//
//int curTimestamp;
//
//// 비교 연산자 override (post 우선순위 비교)
//bool operator<(POST left, POST right) {
//	int leftPriority, rightPriority;
//	// left가 생성된지 1000초를 초과했다면, left의 우선순위인 leftPriority에 left의 timestamp 기록
//	// timestamp 값이 클수록, 즉 늦게 생성되었을수록 우선순위 높아진다.
//	if (left.timestamp + 1000 < curTimestamp)
//		leftPriority = left.timestamp;
//	// left가 생성된지 1000초 이내라면, like 개수가 우선순위에 먼저 반영된다.
//	// 같은 like값 내에서만 timestamp를 비교한다.
//	// timestamp의 최대값이 10만인데, like 개수에 20만을 곱하게 되면
//	// 자연스럽게 like값이 먼저 우선순위에 영향을 준다.
//	else
//		leftPriority = left.like * (MAX_POST * 2) + left.timestamp;
//
//	if (right.timestamp + 1000 < curTimestamp)
//		rightPriority = right.timestamp;
//	else
//		rightPriority = right.like * (MAX_POST * 2) + right.timestamp;
//
//
//	return leftPriority < rightPriority;
//}
//
//void init(int N) {
//	globalN = N;
//	curTimestamp = 0;
//
//	for (int i = 0; i <= globalN; i++) {
//		// i번째 사용자의 게시물 개수(postNum)와 i번째 사용자가 follow하는 사용자의 수(followNum) 초기화
//		postNum[i] = followNum[i] = 0;
//		// 자기 자신을 follow
//		followList[i][followNum[i]++] = i;
//	}
//}
//
//// uId1 사용자가 uId2 사용자를 follow
//// follow[i] = {1, 2, 3, ... }; -> i번째 사용자가 follow하는 사용자들 리스트
//void follow(int uId1, int uId2, int timestamp) {
//	followList[uId1][followNum[uId1]++] = uId2;
//}
//
//// uId 사용자가 timestamp 시점에 pId 게시물을 등록
//void makePost(int uId, int pId, int timestamp) {
//	allPost[pId].timestamp = timestamp;
//	allPost[pId].like = 0;
//	allPost[pId].user = uId;
//	// uId번재 사용자 소유의 게시물의 개수를 postNum에 기록
//	// uId번재 사용자 소유의 게시물의 pId를 myPost에 차례로 기록
//	// myPost[uId] = {pId1, pId2, ... }
//	myPost[uId][postNum[uId]++] = pId;
//}
//
//// 게시글 전체 담고 있는 배열(allPost)에서 해당 게시글 인덱스(pId)에 담긴 구조체에 like 멤버 증가
//void like(int pId, int timestamp) {
//	allPost[pId].like++;
//}
//
//
//void getFeed(int uId, int timestamp, int pIdList[]) {
//	
//	// 반환할 Feed
//	int retList[MAX_RET];
//	int retNum = 0;
//
//	curTimestamp = timestamp;
//
//	// 자신과 자신이 follow 하는 사용자 중 한명(chosenUser) 선택
//	for (int i = 0; i < followNum[uId]; i++) {
//		int chosenUser = followList[uId][i];
//
//		// chosenUser 사용자 소유의 게시물의 pId(chosenPost)를 인덱스 역순으로 하나씩 꺼내기
//		// 인덱스 역순으로 꺼내는 것은 최신 게시물(큰 timestamp)부터 꺼낸다는 의미
//		for (int j = postNum[chosenUser] - 1; j >= 0; j--) {
//			int chosenPost = myPost[chosenUser][j];
//			// 반환할 게시물 개수가 아직 10개 미만인 상태 지속적으로 추가될 예정
//			if (retNum < 10) {
//				// 반환할 pId 담을 리스트(retList)에 추가
//				retList[retNum++] = chosenPost;
//
//				// 이번에 추가된 게시물(chosenPost)의 우선순위(allPost[retList[k]])와
//				// 앞의 pId들의 우선순위(allPost[retList[k - 1]])를 비교해 적절한 위치에 도달할때까지 swap 반복
//				// 정렬된 게시물들 사이에 새로운 게시물을 삽입한다는 점에서 삽입정렬과 동일
//				int k = retNum - 1;
//				while (k > 0) {
//					if (allPost[retList[k - 1]] < allPost[retList[k]]) {
//						int t = retList[k - 1];
//						retList[k - 1] = retList[k];
//						retList[k] = t;
//						k--;
//					}
//					else { break; }
//				}
//			}
//			// retNum == 10 도달, 일단 10개 채워져있지만 지속적으로 교체될 예정
//			else {
//				// 현재 선택된 게시물의 우선순위(allPost[chosenPost])가
//				// 기존의 마지막 게시물의 우선순위(allPost[retList[9])보다 높아서 교체
//				if (allPost[retList[9]] < allPost[chosenPost]) {
//					retList[9] = chosenPost;
//					int k = 9;
//
//					// 기존의 마지막 게시물의 우선순위보다 높은 우선순위 가지는 게시물이
//					// 더 앞쪽의 게시물들보다 우선순위가 더 높은 경우 앞으로 swap 반복
//					while (k > 0) {
//						if (allPost[retList[k - 1]] < allPost[retList[k]]) {
//							int tmp = retList[k - 1];
//							retList[k - 1] = retList[k];
//							retList[k] = tmp;
//							k--;
//						}
//						else { break; }
//					}
//				}
//				// 해당 사용자(chosenUser)의 게시물 중 현재 선택된 게시물의 우선순위(allPost[chosenPost])가
//				// 정답 배열에서 가장 우선순위 낮은 게시물(allPost[retList[9]])보다 우선순위 더 낮음
//				else {
//					// 정답 배열이 10칸 모두 채워진 상태에서
//					// 현재 선택된 게시물(chosenPost)이 등록된지 1000초가 지났다면
//					// 어차피 like는 고려대상이 아니므로 더 이상 해당 사용자에게서 순위 역전의 여지 없음
//					if (allPost[chosenPost].timestamp + 1000 <= curTimestamp)
//						break;
//				}
//			}
//		}
//	}
//
//	// 우선순위 높은 게시물 반환 (유효한 게시물의 개수 = retNum)
//	for (int i = 0; i < retNum; i++)
//		pIdList[i] = retList[i];
//	// 게시물 전체 개수가 10개 안되는 경우, 나머지 칸은 0으로 채우기
//	for (int i = MAX_RET - 1; i >= retNum; i--)
//		pIdList[i] = 0;
//}
