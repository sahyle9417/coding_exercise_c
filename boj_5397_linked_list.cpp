//#include <stdio.h>
//#include <malloc.h>
//
//typedef struct node {
//	char data;
//	node* prev;
//	node* next;
//} Node;
//
//typedef struct list {
//	Node* head;
//	Node* tail;
//	Node* cur;
//} List;
//
//List* mylist;
//
//void init() {
//	mylist = (List*)malloc(sizeof(List));
//	mylist->head = (Node*)malloc(sizeof(Node));
//	mylist->tail = (Node*)malloc(sizeof(Node));
//
//	mylist->head->next = mylist->tail;
//	mylist->tail->prev = mylist->head;
//
//	mylist->cur = mylist->head;
//}
//
//void insert(char newData) {
//	Node* newNode = (Node*)malloc(sizeof(Node));
//	newNode->data = newData;
//	newNode->prev = mylist->cur;
//	newNode->next = mylist->cur->next;
//	mylist->cur->next->prev = newNode;
//	mylist->cur->next = newNode;
//	mylist->cur = newNode;
//}
//
//void remove() {
//	if (mylist->cur == mylist->head) return;
//	Node* delNode = mylist->cur;
//	delNode->prev->next = delNode->next;
//	delNode->next->prev = delNode->prev;
//	mylist->cur = mylist->cur->prev;
//	free(delNode);
//}
//
//void print() {
//	mylist->cur = mylist->head->next;
//	while (mylist->cur != mylist->tail) {
//		printf("%c", mylist->cur->data);
//		mylist->cur = mylist->cur->next;
//	}
//	puts("");
//}
//
//void finish() {
//	mylist->cur = mylist->head->next;
//	while (mylist->cur != mylist->tail) {
//		free(mylist->cur->prev);
//		mylist->cur = mylist->cur->next;
//	}
//	free(mylist->tail->prev);
//	free(mylist->tail);
//	free(mylist);
//}
//
//int main() {
//	int TC;
//	scanf("%d", &TC);
//	for (int tc = 1; tc <= TC; tc++) {
//		init();
//		char input[1000001];
//		scanf("%s", input);
//		
//		int i = 0;
//		while (input[i] != '\0') {
//			char tmp = input[i];
//			// 맨 밑에서 else 쓰기 위해 if문 조건을 일부러 나눠놓은 것임
//			if (tmp == '<') {
//				if (mylist->cur != mylist->head) {
//					mylist->cur = mylist->cur->prev;
//				}
//			}
//			else if (tmp == '>') {
//				if (mylist->cur != mylist->tail->prev) {
//					mylist->cur = mylist->cur->next;
//				}
//			}
//			else if (tmp == '-') {
//				if (mylist->cur != mylist->head) {
//					remove();
//				}
//			}
//			else {
//				insert(tmp);
//			}
//			i++;
//		}
//		print();
//		finish();
//	}
//}