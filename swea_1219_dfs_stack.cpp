////이 문제에서는 방문했던 노드를 다시 방문하는 경우는 없다고 명시했으니 visited 필요없음
//#include <stdio.h>
//
//int main() {
//	freopen("input.txt", "r", stdin);
//	for (int tc = 1; tc <= 10; tc++) {
//		int t;
//		scanf("%d ", &t);
//		int v;
//		scanf("%d ", &v);
//
//		int g[100][2];
//		for (int i = 0; i < 100; i++) {
//			for (int j = 0; j < 2; j++) {
//				g[i][j] = -1;
//			}
//		}
//
//		int from, to;
//		for (int i = 0; i < v; i++) {
//			scanf("%d ", &from);
//			scanf("%d ", &to);
//			if (g[from][0] == -1) {
//				g[from][0] = to;
//			}
//			else {
//				g[from][1] = to;
//			}
//		}
//
//		/*for (int i = 0; i < 100; i++) {
//			printf("%d->%d,%d\n", i, g[i][0], g[i][1]);
//		}*/
//
//		// c++에서는 boolean 배열이 true로 초기화되며, int 배열은 쓰레기값으로 초기화된다.
//		int visit[100];
//		for (int i = 0; i < 100; i++) {
//			visit[i] = 0;
//		}
//
//		int stack[100];
//		stack[0] = 0;	// 무조건 0번 노드에서 출발
//		int top = 1;
//		visit[0] = 1;
//
//		bool success = false;
//
//		while (top) {
//			int from = stack[--top];
//			for (int i = 0; i < 2; i++) {
//				int to = g[from][i];
//				if (to == -1) break;
//				if (visit[to] == 1) continue;
//				if (to == 99){
//					success = true;
//					break;
//				}
//				stack[top++] = to;
//				visit[to] = 1;
//			}
//			if (success) {
//				break;
//			}
//		}
//		if (success) printf("#%d 1\n", tc);
//		else printf("#%d 0\n", tc);
//	}
//}