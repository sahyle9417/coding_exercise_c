//// 함수가 풀려도 상수로 치환하면 안된다, 매번 함수를 재계산해서 업데이트해야함
//// 이거때매 엄청 오래 삽질했다, 문제에서 명확히 밝히지 않은 내용임
//#define MAXR		99
//#define MAXC		26
//#define MAXL		100
//#define MAX_CONST 10000
//#define MIN_CONST -10000
//
//int table[MAXR][MAXC];
//int status[MAXR][MAXC];			// 0:초기상태, 1:함수계산예정, 2:확정
//char function[MAXR][MAXC][MAXL];	// 함수의 내용 기록할 배열
//int globalC, globalR;
//
//void init(int C, int R) {
//	globalC = C;
//	globalR = R;
//	for (int c = 0; c < C; c++) {
//		for (int r = 0; r < R; r++) {
//			table[r][c] = 0;
//			status[r][c] = 0;
//			function[r][c][0] = '\0';
//		}
//	}
//}
//
////void print() {
////	puts("===Data===");
////	for (int r = 0; r < globalR; r++) {
////		for (int c = 0; c < globalC; c++) {
////			printf("%5d ", table[r][c]);
////		}
////		puts("");
////	}
////	puts("===Status===");
////	for (int r = 0; r < globalR; r++) {
////		for (int c = 0; c < globalC; c++) {
////			printf("%d ", status[r][c]);
////		}
////		puts("");
////	}
////	puts("===Function===");
////	for (int r = 0; r < globalR; r++) {
////		for (int c = 0; c < globalC; c++) {
////			if (function[r][c][0] != '\0') {
////				printf("%c%d : function : %s\n", (c + 'A'), (r + 1), function[r][c]);
////			}
////		}
////	}
////}
//
//void str_copy(char* from, char* to) {
//	int idx = 0;
//	while (from[idx] != '\0') {
//		to[idx] = from[idx];
//		idx++;
//	}
//	to[idx] = '\0';
//}
//
//// 함수 풀렸다면 true 반환, 아직이라면 false 반환
//bool solve(int col, int row, char func_str[]) {		// 함수
//	//printf("SOLVE : %c%d : function : %s\n", (col + 'A'), (row + 1), func_str);
//	str_copy(func_str, function[row][col]);
//	int col1 = func_str[4] - 'A';
//	int row1, col2, row2;
//	bool row1_over_9, row2_over_9;
//	if (func_str[6] == ',') {	// 첫번째 인수의 행번호가 1자리수
//		row1 = func_str[5] - '1';
//		col2 = func_str[7] - 'A';
//		if (func_str[9] == ')') row2 = func_str[8] - '1';
//		else row2 = (func_str[8] - '0') * 10 + (func_str[9] - '1');
//	}
//	else {
//		row1 = (func_str[5] - '0') * 10 + (func_str[6] - '1');
//		col2 = func_str[8] - 'A';
//		if (func_str[10] == ')') row2 = func_str[9] - '1';
//		else row2 = (func_str[9] - '0') * 10 + (func_str[10] - '1');
//	}
//	char c = func_str[0];
//	if (c == 'A') {		// ADD
//		if (status[row1][col1] == 2 && status[row2][col2] == 2) {
//			table[row][col] = table[row1][col1] + table[row2][col2];
//			return true;
//		}
//		else return false;
//	}
//	else if (c == 'D') {		// DIV
//		if (status[row1][col1] == 2 && status[row2][col2] == 2) {
//			table[row][col] = table[row1][col1] / table[row2][col2];
//			return true;
//		}
//		else return false;
//	}
//	else if (c == 'S') {
//		if (func_str[2] == 'B') {	// SUB
//			if (status[row1][col1] == 2 && status[row2][col2] == 2) {
//				table[row][col] = table[row1][col1] - table[row2][col2];
//				return true;
//			}
//			else return false;
//		}
//		if (func_str[2] == 'M') {		// SUM
//			bool possible = true;		// 연산 가능 여부 확인, 피연산자 모두 고정되었다면 수행 가능
//			int ret = 0;
//			if (row1 == row2) {	// 동일 row 내에서 col 방향 더하기
//				for (int col_tmp = col1; col_tmp <= col2; col_tmp++) {
//					if (status[row1][col_tmp] == 2) { ret += table[row1][col_tmp]; }
//					else {
//						possible = false;
//						break;
//					}
//				}
//				if (possible) {
//					table[row][col] = ret;
//					return true;
//				}
//				else return false;
//			}
//			else {						// 동일 col 내에서 row 방향 더하기
//				for (int row_tmp = row1; row_tmp <= row2; row_tmp++) {
//					if (status[row_tmp][col1] == 2) { ret += table[row_tmp][col1]; }
//					else {
//						possible = false;
//						break;
//					}
//				}
//				if (possible) {
//					table[row][col] = ret;
//					return true;
//				}
//				else return false;
//			}
//		}
//	}
//	else if (c == 'M') {
//		if (func_str[1] == 'U') {	// MUL
//			if (status[row1][col1] == 2 && status[row2][col2] == 2) {
//				table[row][col] = table[row1][col1] * table[row2][col2];
//				return true;
//			}
//			else return false;
//		}
//		if (func_str[1] == 'I') {
//			// MIN
//			bool possible = true;		// 연산 가능 여부 확인, 피연산자 모두 고정되었다면 수행 가능
//			int ret = MAX_CONST;
//			if (row1 == row2) {	// 동일 row 내에서 col 방향 탐색
//				for (int col_tmp = col1; col_tmp <= col2; col_tmp++) {
//					if (status[row1][col_tmp] == 2) {
//						if (table[row1][col_tmp] < ret) {
//							ret = table[row1][col_tmp];
//						}
//					}
//					else {
//						possible = false;
//						break;
//					}
//				}
//				if (possible) {
//					table[row][col] = ret;
//					return true;
//				}
//				else return false;
//			}
//			else {						// 동일 col 내에서 row 방향 탐색
//				for (int row_tmp = row1; row_tmp <= row2; row_tmp++) {
//					if (status[row_tmp][col1] == 2) {
//						if (table[row_tmp][col1] < ret) {
//							ret = table[row_tmp][col1];
//						}
//					}
//					else {
//						possible = false;
//						break;
//					}
//				}
//				if (possible) {
//					table[row][col] = ret;
//					return true;
//				}
//				else return false;
//			}
//		}
//		if (func_str[1] == 'A') {
//			// MAX
//			bool possible = true;		// 연산 가능 여부 확인, 피연산자 모두 고정되었다면 수행 가능
//			int ret = MIN_CONST;
//			if (row1 == row2) {	// 동일 row 내에서 col 방향 탐색
//				for (int col_tmp = col1; col_tmp <= col2; col_tmp++) {
//					if (status[row1][col_tmp] == 2) {
//						if (table[row1][col_tmp] > ret) {
//							ret = table[row1][col_tmp];
//						}
//					}
//					else {
//						possible = false;
//						break;
//					}
//				}
//				if (possible) {
//					table[row][col] = ret;
//					return true;
//				}
//				else return false;
//			}
//			else {						// 동일 col 내에서 row 방향 탐색
//				for (int row_tmp = row1; row_tmp <= row2; row_tmp++) {
//					if (status[row_tmp][col1] == 2) {
//						if (table[row_tmp][col1] > ret) {
//							ret = table[row_tmp][col1];
//						}
//					}
//					else {
//						possible = false;
//						break;
//					}
//				}
//				if (possible) {
//					table[row][col] = ret;
//					return true;
//				}
//				else return false;
//			}
//		}
//	}
//}
//
//void set(int col, int row, char input[]) {
//	col--; row--;		// 실제 인덱스는 0부터 시작
//	//printf("%c%d : ", (col + 'A'), (row+1));
//	char c = input[0];
//	if (c >= '0' && c <= '9') {
//		//printf("value : ");
//		int len = 0;
//		while (input[len] != '\0') { len++; }
//		int ret = 0;
//		int ten_multiplier = 1;
//		for (int i = len-1; i >=0; i--) {
//			ret += (input[i] - '0') * ten_multiplier;
//			ten_multiplier *= 10;
//		}
//		//printf("%d\n", ret);
//		table[row][col] = ret;
//		status[row][col] = 2;
//		function[row][col][0] = '\0';
//	}
//	else if (c == '-') {
//		//printf("value : ");
//		int len = 0;
//		while (input[len] != '\0') { len++; }
//		int ret = 0;
//		int ten_multiplier = -1;
//		for (int i = len - 1; i >= 1; i--) {
//			ret += (input[i] - '0') * ten_multiplier;
//			ten_multiplier *= 10;
//		}
//		//printf("%d\n", ret);
//		table[row][col] = ret;
//		status[row][col] = 2;
//		function[row][col][0] = '\0';
//	}
//	else { 
//		//printf("function : %s\n", input);
//		status[row][col] = 1;
//		str_copy(input, function[row][col]);
//	}
//	//print();
//}
//
//
//void update(int value[MAXR][MAXC]) {
//	//printf("UPDATE\n");
//	for (int r = 0; r < globalR; r++) {
//		for (int c = 0; c < globalC; c++) {
//			if (status[r][c] == 0) { status[r][c] = 2; }
//		}
//	}
//	//print();
//	// 한칸이라도 업데이트되면 계산할 거 남아있을 수 있으니 계속 반복
//	bool function_remain = true;
//	while(function_remain) {
//		function_remain = false;
//		for (int r = 0; r < globalR; r++) {
//			for (int c = 0; c < globalC; c++) {
//				// 함수가 풀려도 상수로 치환하면 안된다, 매번 함수를 재계산해서 업데이트해야함
//				// 이거때매 엄청 오래 삽질했다, 문제에서 명확히 밝히지 않은 내용임
//				if (status[r][c] == 1 && solve(c, r, function[r][c])) {
//					function_remain = true;
//					// 무한루프를 방지하기 위해 계산 완료된 함수는 상태를 2로 바꿔놓는다
//					// 하지만 함수는 계속 재계산해서 업데이트해야하므로 함수 내용을 지우면 안된다
//					status[r][c] = 2;
//				}
//			}
//		}
//	}
//	//print();
//	for (int r = 0; r < globalR; r++) {
//		for (int c = 0; c < globalC; c++) {
//			if (status[r][c] == 2) {
//				// 계산 완료되서 상태를 2로 바꿔놨던 함수도 계속 재계산해서 업데이트해야하므로 1로 바꿔놓는다.
//				if (function[r][c][0] == '\0') { status[r][c] = 0; }
//				else { status[r][c] = 1; }
//			}
//			value[r][c] = table[r][c];
//		}
//	}
//}
//
