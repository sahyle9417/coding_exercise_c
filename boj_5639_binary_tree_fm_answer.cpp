//#include <stdio.h>
//#define MAX_NODE 20000
//
//typedef struct _node {
//	int data;
//	struct _node* left;
//	struct _node* right;
//} NODE;
//
//NODE nodePool[MAX_NODE];
//int nodePoolIdx = 0;
//
//NODE* MakeNewNode(int newData) {
//	NODE* newNode = &nodePool[nodePoolIdx++];
//	newNode->data = newData;
//	//newNode->left = NULL;
//	//newNode->right = NULL;
//	return newNode;
//}
//
//// 루트 노드로부터 NULL을 만날 때까지 좌측 또는 우측 자식을 선택하면서 내려가다가
//// NULL 만나면 새로운 노드 생성 후 값 할당
//NODE* AssignNode(NODE* node, int newData) {
//	
//	// 노드가 존재하지 않으니 새로운 노드 생성하며 data 기록
//	if (node == NULL) {
//		node = MakeNewNode(newData);
//		return node;
//	}
//
//	// 새로운 data가 부모 노드의 data보다 작다면 좌측 노드에 대해 노드할당 함수를 재귀호출
//	else if (newData < node->data)
//		node->left = AssignNode(node->left, newData);
//
//	// 새로운 data가 부모 노드의 data보다 크다면 우측 노드에 대해 노드할당 함수를 재귀호출
//	else if (newData > node->data)
//		node->right = AssignNode(node->right, newData);
//
//	// 위에서 만든 자식 노드가 아닌 부모 노드를 반환
//	return node;
//}
//
//// 후위순회 출력
//void PostorderTraverse(NODE* node) {
//
//	// 비어있는 노드라면 출력X
//	if (node == NULL)
//		return;
//
//	// 좌측 자식 재귀호출
//	PostorderTraverse(node->left);
//	
//	// 우측 자식 재귀호출
//	PostorderTraverse(node->right);
//
//	// 자식들이 모두 출력되고 나서 부모(자기 자신) 출력
//	printf("%d\n", node->data);
//}
//
//int main(void) {
//	int data;
//	NODE* rootNode = NULL;
//	
//	while (scanf("%d", &data) == 1) { 
//		// 무조건 루트 노드로부터 시작, 반환받은 값으로 rootNode를 갱신하지 않으면 틀렸다고 나옴, 왜지?
//		rootNode = AssignNode(rootNode, data);
//	}
//
//	// 루트 노트로부터 후위순회 출력을 위한 재귀호출 시작
//	PostorderTraverse(rootNode);
//}