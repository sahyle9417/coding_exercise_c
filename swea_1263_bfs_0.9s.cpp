//#include<stdio.h>
//#include <time.h>
//#define MAX 1000
//#define INF 987654321
//typedef struct data {
//	int node_id, dist;
//} Data;
//Data queue[MAX];
//int connCnt[MAX];		// 노드 i와 연결된 노드의 개수
//int g[MAX][MAX];		// 노드 i와 연결된 노드 번호를 0번째 인덱스부터 차례로 기록
//bool visit[MAX];
//int N;
//
//void print() {
//	printf("\n============connCnt============\n");
//	for (int i = 0; i < N; i++) {
//		printf("%2d ", connCnt[i]);
//	}
//	puts("");
//
//	printf("============g============\n");
//	for (int i = 0; i < N; i++) {
//		for (int j = 0; j < N; j++) {
//			printf("%2d ", g[i][j]);
//		}
//		puts("");
//	}
//}
//
//int main() {
//	time_t start = clock();
//	freopen("input.txt", "r", stdin);
//	int tc;
//	int temp;
//	int CC, tempCC;
//	int front, rear;
//	int chk;
//	scanf("%d", &tc);
//
//	for (int testCase = 1; testCase <= 2; testCase++) {
//		chk = 1;
//		tempCC = 0;
//		CC = INF;
//
//		scanf("%d", &N);
//
//		// connCnt, g 초기화
//		for (int i = 0; i < N; i++) {
//			connCnt[i] = 0;
//			for (int j = 0; j < N; j++) { g[i][j] = 0; }
//		}
//
//		for (int i = 0; i < N; i++) {
//			for (int j = 0; j < N; j++) {
//				scanf("%d", &temp);
//				if (temp) {						// connCnt : 노드 i와 연결된 노드의 개수, 
//					g[i][connCnt[i]++] = j;	// g : 노드 i와 연결된 노드 번호를 0번째 인덱스부터 차례로 기록
//				}
//			}
//		}
//
//		// i는 CC값을 구할 최초 출발 노드
//		for (int i = 0; i < N; i++) {
//			// visited 초기화 (모든 노드들에 대해 방문하지 않은 것으로 처리)
//			for (int j = 0; j < N; j++) { visit[j] = false; }
//			// 현재 노드 i의 CC값 기록할 변수
//			tempCC = 0;
//			front = 0; rear = 0;
//			// 노드 i를 push하고 노드 i를 방문 처리
//			queue[rear].node_id = i;
//			queue[rear++].dist = 0;
//			visit[i] = true;
//			while (front != rear) {
//				// 큐에서 pop
//				Data pop = queue[front++];
//				printf("i:%d, u.node_id:%d, u.dist:%d\n", i, pop.node_id, pop.dist);
//				// pop된 노드(pop.node_id)에 연결된 노드의 개수(connCnt[pop.node_id])만큼 반복
//				for (int j = 0; j < connCnt[pop.node_id]; j++) {
//					// pop된 노드(pop.node_id)에 연결된 노드(node_id)를 하나씩 선택
//					int node_id = g[pop.node_id][j];
//					// pop된 노드(pop.node_id)에 연결된 노드(node_id)들 중에서 방문하지 않은 노드 발견
//					if (!visit[node_id]) {
//						// 처음 방문하는 노드의 id와 출발지(노드 i)에서 해당 노드까지의 거래(dist)를 담은 Data 구조체를 큐에 push하고 방문 처리
//						queue[rear].node_id = node_id;
//						queue[rear].dist = pop.dist + 1;	// pop된 노드와 이번에 새로 방문하는 노드는 인접해있으니 dist는 1만큼 추가
//						// 처음 방문하는 노드를 방문 처리
//						visit[node_id] = true;
//						tempCC += queue[rear++].dist;
//						// 이미 기존에 기록했던 최소 CC보다 커져버렸으니 더이상의 dist 연산은 무의미
//						// 다음 노드(i+1)번째 노드의 CC를 구하자
//						if (tempCC >= CC) { goto exit; }
//					}
//				}
//			}
//		exit:
//
//			// 최솟값 갱신
//			if (tempCC < CC) { CC = tempCC; }
//
//		}
//		//print();
//		printf("#%d %d\n", testCase, CC);
//	}
//	time_t end = clock();
//	printf("time : %f\n", (double)(end - start));
//
//	return 0;
//}
//
