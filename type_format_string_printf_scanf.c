//#include <stdio.h>
//
//int main(void) {
//
//	char c;			// %d(숫자), %c(문자)
//	short s;		// %d(숫자), %c(문자)
//	int i;			// %d(숫자), %c(문자)
//					// %o(8진수), %x(16진수 소문자), %X(16진수 대문자)
//					// %#o(앞에0붙인8진수), %#x(앞에0x붙인 16진수)
//	long l;			// %ld
//	long long ll;	// %lld
//	unsigned int u;	// %u(10진수), %o(8진수), %x(16진수 소문자), %X(16진수 대문자)
//					// %#o(앞에0붙인8진수), %#x(앞에0x붙인 16진수)
//
//	float f;		// %f(입력), %f(10진수출력), %e(e방식출력), %E(E방식출력)
//	double d;		// %lf(입력), %f(10진수출력), %e(e방식출력), %E(E방식출력)
//	long double ld;	// %Lf
//
//	char* str;		// %s
//	int* ptr;		// %p(주소값)
//
//	return 0;
//}