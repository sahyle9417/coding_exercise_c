//#include <stdio.h>
//#include <string.h>
//
//int main(void) {
//	char str1[] = "contents";
//	char str2[] = "contents";
//
//	if (str1 == str2) {
//		puts("address equal");
//	}
//	else {
//		puts("address not equal");
//	}
//
//	if (strcmp(str1, str2)==0) {	// string의 내용이 같다면 0 반환
//		puts("contents equal");		// 사전순서상 str1이 먼저 나오면 음수 반환
//	}								// 사전순서상 str1이 뒤에 나오면 양수 반환
//	else {							
//		puts("contents not equal");
//	}
//
//	char str3[] = "abcdef";
//	char str4[] = "abcabc";
//
//	if (strncmp(str3, str4, 3) == 0) {	// string 내용 앞 3글자만 비교
//		puts("first 3 char equal");
//	}
//	else {
//		puts("first 3 char not equal");
//	}
//}