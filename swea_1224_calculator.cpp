//// 조건문에 ii++ 달았다가 조건 성립하지 않아도 ii가 증가하는 현상 있어서 한참동안 삽질했음
//// 조건문에 포함된 전위연산, 후위연산은 조건 성립여부와 무관하게 항상 실행되는 것에 유의하기
//// 중위표현식(infix)->후위표현식(postfix) 변환 : https://ndb796.tistory.com/73
//// 중위표현식(infix)->후위표현식(postfix) 변환과정에서
//// 중위표현식(infix) 끝까지 다 보고 후위표현식 출력했다가 스택에 남아있는 연산자 포함시키지 않아서 삽질했음
//
//#include <stdio.h>
//#include <malloc.h>
//
//char * infix;
//char * postfix;
//char * op_stack;
//int * num_stack;
//
//int main() {
//	freopen("input.txt", "r", stdin);
//	//freopen("input2.txt", "r", stdin);
//	for (int tc = 1; tc <= 10; tc++) {
//		int length;
//		scanf("%d ", &length);
//		infix = (char *)malloc(sizeof(char)*(length+1));
//		scanf("%s ", infix);
//
//		postfix = (char *)malloc(sizeof(char)*(length + 1));
//		op_stack = (char *)malloc(sizeof(char)*(length + 1));
//		
//		int ii = 0;	// infix index
//		int pi = 0;		// postfix index
//		int top = 0;	// stack top
//
//
//		while (ii < length) {
//			if (infix[ii] >= '0' && infix[ii] <= '9') {
//				//printf("pi:%d, ii:%d\n", pi, ii);
//				postfix[pi++] = infix[ii];
//				//printf("put %c into postfix\n", postfix[pi - 1]);
//			}
//			// 조건문에 ii++ 달았다가 조건 성립하지 않아도 ii가 증가하는 현상 있어서 한참동안 삽질했음
//			else if (infix[ii]=='(') {
//				op_stack[top++] = '(';
//				//printf("put %c into op_stack\n", op_stack[top-1]);
//			}
//			else if (infix[ii] == '+') {
//				//printf("meet plus\n");
//				while (op_stack[top - 1] == '+' || op_stack[top - 1] == '*') {
//					postfix[pi++] = op_stack[--top];
//					//printf("move %c from op_stack to postfix\n", postfix[pi-1]);
//				}
//				op_stack[top++] = '+';
//			}
//			else if (infix[ii] == '*') {
//				while (op_stack[top - 1] == '*') {
//					postfix[pi++] = op_stack[--top];
//					//printf("move %c from op_stack to postfix\n", postfix[pi - 1]);
//				}
//				op_stack[top++] = '*';
//			}
//			else if (infix[ii] == ')') {
//				while (op_stack[top - 1] != '(') {
//					postfix[pi++] = op_stack[--top];
//				}
//				top--;	// 스택에서 '(' 제거
//			}
//			ii++;
//			/*printf("op_stack:");
//			for (int i = 0; i < top; i++) {
//				printf("%c", op_stack[i]);
//			}
//			printf("\npost:");
//			for (int i = 0; i < pi; i++) {
//				printf("%c", postfix[i]);
//			}
//			printf("\n");*/
//		}
//		for (int i = 0; i < top; i++) {
//			postfix[pi++] = op_stack[i];
//		}
//		/*printf("\n==========\n");
//		for (int i = 0; i < pi; i++) {
//			printf("%c", postfix[i]);
//		}*/
//
//		free(infix);
//		free(op_stack);
//
//		length = pi;
//		num_stack = (int*)malloc(sizeof(int)*length);
//		pi = 0;
//		top = 0;
//		int tmp1, tmp2;
//
//		while (pi < length) {
//			//printf("postfix[%d]:%c\n", pi, postfix[pi]);
//			if (postfix[pi] >= '0' && postfix[pi] <= '9') {
//				num_stack[top++] = postfix[pi] - '0';
//			}
//			else if (postfix[pi] == '+') {
//				tmp1 = num_stack[top - 1];
//				tmp2 = num_stack[top - 2];
//				top = top - 2;
//				num_stack[top++] = tmp1 + tmp2;
//				//printf("%d+%d=%d\n", tmp1, tmp2, (tmp1+tmp2));
//			}
//			else if (postfix[pi] == '*') {
//				tmp1 = num_stack[top - 1];
//				tmp2 = num_stack[top - 2];
//				top = top - 2;
//				num_stack[top++] = tmp1 * tmp2;
//				//printf("%d*%d=%d\n", tmp1, tmp2, (tmp1 * tmp2));
//			}
//			pi++;
//			/*for (int tt = 0; tt < top; tt++) printf("%d ", num_stack[tt]);
//			printf("\n");*/
//		}
//
//		printf("#%d %d\n", tc, num_stack[top - 1]);
//	}
//}